
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       pi.h
  * @author     baiyang
  * @date       2021-8-8
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/

/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    float integrator;
    float output_p;

    float _kp;
    float _ki;
    float _kimax;
} PI_ctrl;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void pi_ctrl_init(PI_ctrl * controler, float kp, float ki, float kimax);

// update controller
float pi_ctrl_update(PI_ctrl * controler, float target, float measurement, float dt);

static inline void pi_ctrl_set_kp(PI_ctrl * controler, float kp) { controler->_kp = kp; }
static inline void pi_ctrl_set_ki(PI_ctrl * controler, float ki) { controler->_ki = ki; }
static inline void pi_ctrl_set_kimax(PI_ctrl * controler, float kimax) { controler->_kimax = kimax; }

static inline float pi_ctrl_get_p(PI_ctrl * controler) { return controler->output_p; }
static inline float pi_ctrl_get_i(PI_ctrl * controler) { return controler->integrator; }

static inline void pi_ctrl_reset_i(PI_ctrl * controler) { controler->integrator = 0; }

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



