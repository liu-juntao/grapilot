
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       file_manager.c
  * @author     baiyang
  * @date       2021-7-12
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <string.h>

#include "file_manager.h"

#include <sys/types.h >
#include <sys/stat.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static uint8_t _mnt_init = 0;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
static gp_err _mk_rootfs(void)
{
    struct stat buf;

    if (stat("/sys", &buf) < 0) {
        mkdir("/sys", 0x777);
    }

    if (stat("/usr", &buf) < 0) {
        mkdir("/usr", 0x777);
    }

    if (stat("/log", &buf) < 0) {
        mkdir("/log", 0x777);
    }

    if (stat("/mission", &buf) < 0) {
        mkdir("/mission", 0x777);
    }

    return GP_EOK;
}

static void _deldir(char* path)
{
    DIR* dir = opendir(path);

    struct dirent* dr = readdir(dir);

    while (dr) {
        strcat(path, "/");
        strcat(path, dr->d_name);

        if (dr->d_type == FT_DIRECTORY) {
            _deldir(path);
        } else {
            unlink(path);
        }

        for (int i = strlen(path) - 1; i >= 0; i--) {
            if (path[i] == '/') {
                path[i] = '\0';
                break;
            }

            path[i] = 0;
        }

        dr = readdir(dir);
    }

    closedir(dir);
    unlink(path);
}

int fs_fprintf(int fd, const char* fmt, ...)
{
    va_list args;
    int length;
    char buffer[256];

    va_start(args, fmt);
    length = vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);

    return write(fd, buffer, length);
}

gp_err fs_deldir(const char* path)
{
    char temp_path[100];

    strcpy(temp_path, path);
    _deldir(temp_path);

    // TODO, check path
    return GP_EOK;
}

bool fs_file_exist(const char* path)
{
#if CONFIG_HAL_BOARD == HAL_BOARD_SITL_WIN
    int fd = open(path, O_WRONLY);
    if (fd < 0) {
        return false;
    } else {
        close(fd);
        return true;
    }
#else
    struct stat buf;

    if (stat(path, &buf) == 0) {
        return true;
    } else {
        return false;
    }
#endif
}

int fs_init(void)
{
    int res = 0;
    
#if CONFIG_HAL_BOARD == HAL_BOARD_SITL_WIN
    _mnt_init = 1;
    res = 1;
#else
    rt_device_t dev;
    int count = 10;

    while (count--)
    {
        dev = rt_device_find("sd0");
        if (dev != RT_NULL)
        {
            if (dfs_mount("sd0", "/", "elm", 0, 0) == RT_EOK)
            {
                res = 1;
                _mnt_init = 1;
                _mk_rootfs();
                rt_kprintf("dfs mount success\n");
                break;
            }
            else
            {
                res = 0;
                _mnt_init = 0;
                rt_kprintf("dfs mount failed\n");
            }
        }
        rt_thread_mdelay(100);
    }
#endif

    return res;
}

uint8_t fs_init_complete(void)
{
	return _mnt_init;
}

/*------------------------------------test------------------------------------*/


