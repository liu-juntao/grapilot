
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       mavproxy_monitor.h
  * @author     baiyang
  * @date       2021-7-22
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <common/gp_defines.h>

#include <c_library_v2/ardupilotmega/mavlink.h>

#include <rc_channel/rc_channel.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void mavproxy_rx_loop();

#if 0
void mavproxy_proc_command(mavlink_command_long_t* command, mavlink_message_t* msg);
#endif

// return MAV_TYPE corresponding to frame class
MAV_TYPE mavproxy_get_frame_mav_type();
MAV_MODE mavproxy_base_mode();
MAV_STATE mavproxy_vehicle_system_status();
uint32_t mavproxy_custom_mode();

void mavproxy_send_banner();

MAV_RESULT mavlink_handle_command_do_set_mode(const mavlink_command_long_t *packet);
void mavproxy_send_command_ack_sysid(uint16_t command, uint8_t result, mavlink_message_t* msg);

void mavproxy_get_sensor_status_flags(uint32_t *present, uint32_t *enabled, uint32_t *health);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



