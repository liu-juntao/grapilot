
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sim_flightgear.c
  * @author     baiyang
  * @date       2021-10-1
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#ifdef _MSC_VER
#include "sim_flightgear.h"

#include <platform/simulator/windows/drivers/gp_socket_udp.h>

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <Windows.h>

#include <rtthread.h>

#include <SitlMultiCopter/Multicopter.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static gp_socket_udp sock_flightgear;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
bool sim_flightgear_init()
{
    uint32_t udp_recv_port = 0;
    uint32_t udp_send_port = 5503;
    const char* local_ip = "127.0.0.1"; //本机通信

    if (!socket_udp_init(&sock_flightgear, local_ip, udp_recv_port, udp_send_port, NULL, 0)) {
        return false;
    }

    return true;
}
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void sim_flightgear_start_fg()
{
    if (!sim_flightgear_init()) {
        return;
    }

    char FGName[255];
    char curr_path[255];
    _getcwd(curr_path, sizeof(curr_path));
    snprintf(FGName, sizeof(FGName), "%s\\..\\..\\..\\tools\\autotest\\fg_quad_view.bat", curr_path);
    WinExec(FGName, SW_HIDE);
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void sim_flightgear_send_data()
{
    if (!socket_udp_is_connect(&sock_flightgear)) {
        return;
    }
    
    char fdm_buff[1024];
    int fdm_len = 0;

    Fdm_packed(fdm_buff, &fdm_len);
    
    socket_udp_send(&sock_flightgear, fdm_buff, fdm_len);
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void start_fg()
{
    sim_flightgear_start_fg();
}

MSH_CMD_EXPORT(start_fg, start flightgear);

/*------------------------------------test------------------------------------*/
#endif

