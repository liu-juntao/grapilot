
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_compass_lsm303d.c
  * @author     baiyang
  * @date       2021-12-2
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "sensor_compass_lsm303d.h"

#include <common/console/console.h>
/*-----------------------------------macro------------------------------------*/
#ifndef LSM303D_DRDY_M_PIN
#define LSM303D_DRDY_M_PIN -1
#endif

/* SPI protocol address bits */
#define DIR_READ                (1<<7)
#define DIR_WRITE               (0<<7)
#define ADDR_INCREMENT          (1<<6)

/* register addresses: A: accel, M: mag, T: temp */
#define ADDR_WHO_AM_I           0x0F
#define WHO_I_AM                     0x49

#define ADDR_OUT_TEMP_L         0x05
#define ADDR_OUT_TEMP_H         0x06
#define ADDR_STATUS_M           0x07
#define ADDR_OUT_X_L_M              0x08
#define ADDR_OUT_X_H_M              0x09
#define ADDR_OUT_Y_L_M              0x0A
#define ADDR_OUT_Y_H_M          0x0B
#define ADDR_OUT_Z_L_M          0x0C
#define ADDR_OUT_Z_H_M          0x0D

#define ADDR_INT_CTRL_M         0x12
#define ADDR_INT_SRC_M          0x13
#define ADDR_REFERENCE_X        0x1c
#define ADDR_REFERENCE_Y        0x1d
#define ADDR_REFERENCE_Z        0x1e

#define ADDR_STATUS_A           0x27
#define ADDR_OUT_X_L_A          0x28
#define ADDR_OUT_X_H_A          0x29
#define ADDR_OUT_Y_L_A          0x2A
#define ADDR_OUT_Y_H_A          0x2B
#define ADDR_OUT_Z_L_A          0x2C
#define ADDR_OUT_Z_H_A          0x2D

#define ADDR_CTRL_REG0          0x1F
#define ADDR_CTRL_REG1          0x20
#define ADDR_CTRL_REG2          0x21
#define ADDR_CTRL_REG3          0x22
#define ADDR_CTRL_REG4          0x23
#define ADDR_CTRL_REG5          0x24
#define ADDR_CTRL_REG6          0x25
#define ADDR_CTRL_REG7          0x26

#define ADDR_FIFO_CTRL          0x2e
#define ADDR_FIFO_SRC           0x2f

#define ADDR_IG_CFG1            0x30
#define ADDR_IG_SRC1            0x31
#define ADDR_IG_THS1            0x32
#define ADDR_IG_DUR1            0x33
#define ADDR_IG_CFG2            0x34
#define ADDR_IG_SRC2            0x35
#define ADDR_IG_THS2            0x36
#define ADDR_IG_DUR2            0x37
#define ADDR_CLICK_CFG          0x38
#define ADDR_CLICK_SRC          0x39
#define ADDR_CLICK_THS          0x3a
#define ADDR_TIME_LIMIT         0x3b
#define ADDR_TIME_LATENCY       0x3c
#define ADDR_TIME_WINDOW        0x3d
#define ADDR_ACT_THS            0x3e
#define ADDR_ACT_DUR            0x3f

#define REG1_RATE_BITS_A        ((1<<7) | (1<<6) | (1<<5) | (1<<4))
#define REG1_POWERDOWN_A        ((0<<7) | (0<<6) | (0<<5) | (0<<4))
#define REG1_RATE_3_125HZ_A     ((0<<7) | (0<<6) | (0<<5) | (1<<4))
#define REG1_RATE_6_25HZ_A      ((0<<7) | (0<<6) | (1<<5) | (0<<4))
#define REG1_RATE_12_5HZ_A      ((0<<7) | (0<<6) | (1<<5) | (1<<4))
#define REG1_RATE_25HZ_A        ((0<<7) | (1<<6) | (0<<5) | (0<<4))
#define REG1_RATE_50HZ_A        ((0<<7) | (1<<6) | (0<<5) | (1<<4))
#define REG1_RATE_100HZ_A       ((0<<7) | (1<<6) | (1<<5) | (0<<4))
#define REG1_RATE_200HZ_A       ((0<<7) | (1<<6) | (1<<5) | (1<<4))
#define REG1_RATE_400HZ_A       ((1<<7) | (0<<6) | (0<<5) | (0<<4))
#define REG1_RATE_800HZ_A       ((1<<7) | (0<<6) | (0<<5) | (1<<4))
#define REG1_RATE_1600HZ_A      ((1<<7) | (0<<6) | (1<<5) | (0<<4))

#define REG1_BDU_UPDATE         (1<<3)
#define REG1_Z_ENABLE_A         (1<<2)
#define REG1_Y_ENABLE_A         (1<<1)
#define REG1_X_ENABLE_A         (1<<0)

#define REG2_ANTIALIAS_FILTER_BW_BITS_A ((1<<7) | (1<<6))
#define REG2_AA_FILTER_BW_773HZ_A       ((0<<7) | (0<<6))
#define REG2_AA_FILTER_BW_194HZ_A       ((0<<7) | (1<<6))
#define REG2_AA_FILTER_BW_362HZ_A       ((1<<7) | (0<<6))
#define REG2_AA_FILTER_BW_50HZ_A        ((1<<7) | (1<<6))

#define REG2_FULL_SCALE_BITS_A  ((1<<5) | (1<<4) | (1<<3))
#define REG2_FULL_SCALE_2G_A    ((0<<5) | (0<<4) | (0<<3))
#define REG2_FULL_SCALE_4G_A    ((0<<5) | (0<<4) | (1<<3))
#define REG2_FULL_SCALE_6G_A    ((0<<5) | (1<<4) | (0<<3))
#define REG2_FULL_SCALE_8G_A    ((0<<5) | (1<<4) | (1<<3))
#define REG2_FULL_SCALE_16G_A   ((1<<5) | (0<<4) | (0<<3))

#define REG5_ENABLE_T           (1<<7)

#define REG5_RES_HIGH_M         ((1<<6) | (1<<5) | (1<<7))
#define REG5_RES_LOW_M          ((0<<6) | (0<<5))

#define REG5_RATE_BITS_M        ((1<<4) | (1<<3) | (1<<2))
#define REG5_RATE_3_125HZ_M     ((0<<4) | (0<<3) | (0<<2))
#define REG5_RATE_6_25HZ_M      ((0<<4) | (0<<3) | (1<<2))
#define REG5_RATE_12_5HZ_M      ((0<<4) | (1<<3) | (0<<2))
#define REG5_RATE_25HZ_M        ((0<<4) | (1<<3) | (1<<2))
#define REG5_RATE_50HZ_M        ((1<<4) | (0<<3) | (0<<2))
#define REG5_RATE_100HZ_M       ((1<<4) | (0<<3) | (1<<2))
#define REG5_RATE_DO_NOT_USE_M  ((1<<4) | (1<<3) | (0<<2))

#define REG6_FULL_SCALE_BITS_M  ((1<<6) | (1<<5))
#define REG6_FULL_SCALE_2GA_M   ((0<<6) | (0<<5))
#define REG6_FULL_SCALE_4GA_M   ((0<<6) | (1<<5))
#define REG6_FULL_SCALE_8GA_M   ((1<<6) | (0<<5))
#define REG6_FULL_SCALE_12GA_M  ((1<<6) | (1<<5))

#define REG7_CONT_MODE_M        ((0<<1) | (0<<0))

#define INT_CTRL_M              0x12
#define INT_SRC_M               0x13

#define LSM303D_MAG_DEFAULT_RANGE_GA          2
#define LSM303D_MAG_DEFAULT_RATE            100

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
static bool init(sensor_compass_lsm303d *lsm303d, enum RotationEnum rotation);
static void read(sensor_compass_backend *compass_backend);
static void _update(void *parameter);
static uint8_t _register_read(sensor_compass_lsm303d *lsm303d, uint8_t reg);
static bool _block_read(sensor_compass_lsm303d *lsm303d, uint8_t reg, uint8_t *buf, uint32_t size);
static void _register_write(sensor_compass_lsm303d *lsm303d, uint8_t reg, uint8_t val);
static void _register_modify(sensor_compass_lsm303d *lsm303d, uint8_t reg, uint8_t clearbits, uint8_t setbits);
static bool _data_ready(sensor_compass_lsm303d *lsm303d);
static bool _read_sample(sensor_compass_lsm303d *lsm303d);
static bool _hardware_init(sensor_compass_lsm303d *lsm303d);
static void _disable_i2c(sensor_compass_lsm303d *lsm303d);
static bool _mag_set_range(sensor_compass_lsm303d *lsm303d, uint8_t max_ga);
static bool _mag_set_samplerate(sensor_compass_lsm303d *lsm303d, uint16_t frequency);
/*----------------------------------variable----------------------------------*/
struct sensor_compass_backend_ops lsm303d_ops;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void sensor_compass_lsm303d_ctor(sensor_compass_lsm303d *lsm303d, gp_device_t dev)
{
    // 清空sensor_imu_backend结构体变量，因为sensor_imu_backend结构体有可能是申请的动态内存
    // 防止sensor_imu_backend中的变量初始为非零值。
    rt_memset(lsm303d, 0, sizeof(sensor_compass_lsm303d));

    sensor_compass_backend_ctor(&lsm303d->backend, "lsm303d");

    lsm303d_ops = compass_backend_ops;
    lsm303d_ops.read = read;

    lsm303d->backend.ops = &lsm303d_ops;
    lsm303d->_dev = dev;
}

void sensor_compass_lsm303d_destructor(sensor_compass_lsm303d *lsm303d)
{
    sensor_compass_backend_destructor(&lsm303d->backend);
}

sensor_compass_backend *sensor_compass_lsm303d_probe(gp_device_t dev, enum RotationEnum rotation)
{
    if (!dev) {
        return NULL;
    }

    sensor_compass_lsm303d *sensor = (sensor_compass_lsm303d *)rt_malloc(sizeof(sensor_compass_lsm303d));
    sensor_compass_lsm303d_ctor(sensor, dev);

    if (!sensor || !init(sensor, rotation)) {
        sensor_compass_lsm303d_destructor(sensor);
        rt_free(sensor);
        return NULL;
    }

    return (sensor_compass_backend *)sensor;
}

static bool init(sensor_compass_lsm303d *lsm303d, enum RotationEnum rotation)
{
    if (LSM303D_DRDY_M_PIN >= 0) {
        //lsm303d->_drdy_pin_m = hal.gpio->channel(LSM303D_DRDY_M_PIN);
        //lsm303d->_drdy_pin_m->mode(HAL_GPIO_INPUT);
    }

    bool success = _hardware_init(lsm303d);

    if (!success) {
        return false;
    }

    lsm303d->_initialised = true;

    /* register the compass instance in the frontend */
    devmgr_set_device_type(lsm303d->_dev, COMPASS_DEVTYPE_LSM303D);
    if (!sensor_compass_register_compass(devmgr_get_bus_id(lsm303d->_dev), &lsm303d->_compass_instance)) {
        return false;
    }
    sensor_compass_backend_set_dev_id(lsm303d->_compass_instance, devmgr_get_bus_id(lsm303d->_dev));

    sensor_compass_backend_set_rotation(lsm303d->_compass_instance, rotation);

    // read at 91Hz. We don't run at 100Hz as fetching data too fast can cause some very
    // odd periodic changes in the output data
    devmgr_register_periodic_callback(lsm303d->_dev, 13333, _update, lsm303d);

    console_printf("lsm303d(mag) found on bus 0x%x\n", (unsigned)devmgr_get_bus_id(lsm303d->_dev));

    return true;

}

static void read(sensor_compass_backend *compass_backend)
{
    sensor_compass_lsm303d *lsm303d = (sensor_compass_lsm303d *)compass_backend;

    if (!lsm303d->_initialised) {
        return;
    }

    sensor_compass_backend_drain_accumulated_samples(&lsm303d->backend, lsm303d->_compass_instance, NULL);
}

static void _update(void *parameter)
{
    sensor_compass_lsm303d *lsm303d = (sensor_compass_lsm303d *)parameter;

    if (!_read_sample(lsm303d)) {
        return;
    }

    Vector3f_t raw_field = {lsm303d->_mag_x * lsm303d->_mag_range_scale,
                            lsm303d->_mag_y * lsm303d->_mag_range_scale,
                            lsm303d->_mag_z * lsm303d->_mag_range_scale};

    sensor_compass_backend_accumulate_sample(&lsm303d->backend, &raw_field, lsm303d->_compass_instance, 10);
}

static uint8_t _register_read(sensor_compass_lsm303d *lsm303d, uint8_t reg)
{
    uint8_t val = 0;

    reg |= DIR_READ;
    devmgr_read_registers(lsm303d->_dev, reg, &val, 1);

    return val;
}

static bool _block_read(sensor_compass_lsm303d *lsm303d, uint8_t reg, uint8_t *buf, uint32_t size)
{
    reg |= DIR_READ | ADDR_INCREMENT;
    return devmgr_read_registers(lsm303d->_dev, reg, buf, size);
}

static void _register_write(sensor_compass_lsm303d *lsm303d, uint8_t reg, uint8_t val)
{
    devmgr_write_register(lsm303d->_dev, reg, val, false);
}

static void _register_modify(sensor_compass_lsm303d *lsm303d, uint8_t reg, uint8_t clearbits, uint8_t setbits)
{
    uint8_t val;

    val = _register_read(lsm303d, reg);
    val &= ~clearbits;
    val |= setbits;
    _register_write(lsm303d, reg, val);
}

/**
 * Return true if the LSM303D has new data available for both the mag and
 * the accels.
 */
static bool _data_ready(sensor_compass_lsm303d *lsm303d)
{
    if (lsm303d->_drdy_pin_m == NULL) {
        return true;
    }
    
    struct rt_device_pin_status status;
    rt_device_read(lsm303d->_drdy_pin_m, 0, &status, sizeof(struct rt_device_pin_status));

    return status.status;
}

// Read Sensor data
static bool _read_sample(sensor_compass_lsm303d *lsm303d)
{
    // 定义一字节对齐的匿名结构体
    struct PACKED {
        uint8_t status;
        int16_t x;
        int16_t y;
        int16_t z;
    } rx;

    if (_register_read(lsm303d, ADDR_CTRL_REG7) != lsm303d->_reg7_expected) {
        console_printf("LSM303D _read_data_transaction_accel: _reg7_expected unexpected\n");
        return false;
    }

    if (!_data_ready(lsm303d)) {
        return false;
    }

    if (!_block_read(lsm303d, ADDR_STATUS_M, (uint8_t *) &rx, sizeof(rx))) {
        return false;
    }

    /* check for overrun */
    if ((rx.status & 0x70) != 0) {
        return false;
    }

    if (rx.x == 0 && rx.y == 0 && rx.z == 0) {
        return false;
    }

    lsm303d->_mag_x = rx.x;
    lsm303d->_mag_y = rx.y;
    lsm303d->_mag_z = rx.z;

    return true;
}

static bool _hardware_init(sensor_compass_lsm303d *lsm303d)
{
    // initially run the bus at low speed
    devmgr_set_speed(lsm303d->_dev, DEV_SPEED_LOW);

    // Test WHOAMI
    uint8_t whoami = _register_read(lsm303d, ADDR_WHO_AM_I);
    if (whoami != WHO_I_AM) {
        goto fail_whoami;
    }

    uint8_t tries;
    for (tries = 0; tries < 5; tries++) {
        // ensure the chip doesn't interpret any other bus traffic as I2C
        _disable_i2c(lsm303d);

        /* enable mag */
        lsm303d->_reg7_expected = REG7_CONT_MODE_M;
        _register_write(lsm303d, ADDR_CTRL_REG7, lsm303d->_reg7_expected);
        _register_write(lsm303d, ADDR_CTRL_REG5, REG5_RES_HIGH_M);

        // DRDY on MAG on INT2
        _register_write(lsm303d, ADDR_CTRL_REG4, 0x04);

        _mag_set_range(lsm303d, LSM303D_MAG_DEFAULT_RANGE_GA);
        _mag_set_samplerate(lsm303d, LSM303D_MAG_DEFAULT_RATE);

        rt_thread_mdelay(10);
        if (_data_ready(lsm303d)) {
            break;
        }
    }
    if (tries == 5) {
        console_printf("Failed to boot LSM303D 5 times\n");
        goto fail_tries;
    }

    devmgr_set_speed(lsm303d->_dev, DEV_SPEED_HIGH);

    return true;

fail_tries:
fail_whoami:
    devmgr_set_speed(lsm303d->_dev, DEV_SPEED_HIGH);
    return false;
}

static void _disable_i2c(sensor_compass_lsm303d *lsm303d)
{
    // TODO: use the register names
    uint8_t a = _register_read(lsm303d, 0x02);
    _register_write(lsm303d, 0x02, (0x10 | a));
    a = _register_read(lsm303d, 0x02);
    _register_write(lsm303d, 0x02, (0xF7 & a));
    a = _register_read(lsm303d, 0x15);
    _register_write(lsm303d, 0x15, (0x80 | a));
    a = _register_read(lsm303d, 0x02);
    _register_write(lsm303d, 0x02, (0xE7 & a));
}

static bool _mag_set_range(sensor_compass_lsm303d *lsm303d, uint8_t max_ga)
{
    uint8_t setbits = 0;
    uint8_t clearbits = REG6_FULL_SCALE_BITS_M;
    float new_scale_ga_digit = 0.0f;

    if (max_ga == 0) {
        max_ga = 12;
    }

    if (max_ga <= 2) {
        lsm303d->_mag_range_ga = 2;
        setbits |= REG6_FULL_SCALE_2GA_M;
        new_scale_ga_digit = 0.080f;
    } else if (max_ga <= 4) {
        lsm303d->_mag_range_ga = 4;
        setbits |= REG6_FULL_SCALE_4GA_M;
        new_scale_ga_digit = 0.160f;
    } else if (max_ga <= 8) {
        lsm303d->_mag_range_ga = 8;
        setbits |= REG6_FULL_SCALE_8GA_M;
        new_scale_ga_digit = 0.320f;
    } else if (max_ga <= 12) {
        lsm303d->_mag_range_ga = 12;
        setbits |= REG6_FULL_SCALE_12GA_M;
        new_scale_ga_digit = 0.479f;
    } else {
        return false;
    }

    lsm303d->_mag_range_scale = new_scale_ga_digit;
    _register_modify(lsm303d, ADDR_CTRL_REG6, clearbits, setbits);

    return true;
}

static bool _mag_set_samplerate(sensor_compass_lsm303d *lsm303d, uint16_t frequency)
{
    uint8_t setbits = 0;
    uint8_t clearbits = REG5_RATE_BITS_M;

    if (frequency == 0) {
        frequency = 100;
    }

    if (frequency <= 25) {
        setbits |= REG5_RATE_25HZ_M;
        lsm303d->_mag_samplerate = 25;
    } else if (frequency <= 50) {
        setbits |= REG5_RATE_50HZ_M;
        lsm303d->_mag_samplerate = 50;
    } else if (frequency <= 100) {
        setbits |= REG5_RATE_100HZ_M;
        lsm303d->_mag_samplerate = 100;
    } else {
        return false;
    }

    _register_modify(lsm303d, ADDR_CTRL_REG5, clearbits, setbits);

    return true;
}

/*------------------------------------test------------------------------------*/


