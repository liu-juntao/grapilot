
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_compass_lsm303d.h
  * @author     baiyang
  * @date       2021-12-2
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "sensor_compass.h"
#include "sensor_compass_backend.h"

#include <common/time/gp_time.h>
#include <common/gp_rotations.h>
#include <device_manager/dev_mgr.h>
#include <common/gp_math/gp_mathlib.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct {
    sensor_compass_backend backend;

    rt_device_t _drdy_pin_m;
    gp_device_t _dev;

    float _mag_range_scale;
    int16_t _mag_x;
    int16_t _mag_y;
    int16_t _mag_z;

    uint8_t _compass_instance;
    bool _initialised;

    uint8_t _mag_range_ga;
    uint8_t _mag_samplerate;
    uint8_t _reg7_expected;
} sensor_compass_lsm303d;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
sensor_compass_backend *sensor_compass_lsm303d_probe(gp_device_t dev, enum RotationEnum rotation);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



