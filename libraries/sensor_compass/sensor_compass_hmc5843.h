
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_compass_hmc5843.h
  * @author     baiyang
  * @date       2021-12-1
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "sensor_compass.h"
#include "sensor_compass_backend.h"

#include <common/time/gp_time.h>
#include <common/gp_rotations.h>
#include <device_manager/dev_mgr.h>
#include <common/gp_math/gp_mathlib.h>
/*-----------------------------------macro------------------------------------*/
#ifndef HAL_COMPASS_HMC5843_I2C_ADDR
#define HAL_COMPASS_HMC5843_I2C_ADDR 0x1E
#endif
/*----------------------------------typedef-----------------------------------*/
typedef struct {
    sensor_compass_backend backend;

    gp_device_t _dev;

    Vector3f_t _scaling;
    float _gain_scale;

    int16_t _mag_x;
    int16_t _mag_y;
    int16_t _mag_z;

    uint8_t _compass_instance;

    enum RotationEnum _rotation;

    bool _initialised;
    bool _force_external;
} sensor_compass_hmc5843;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
sensor_compass_backend *sensor_compass_hmc5843_probe(gp_device_t dev, bool force_external, enum RotationEnum rotation);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



