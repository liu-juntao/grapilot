
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       param_cmd.c
  * @author     baiyang
  * @date       2021-7-12
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <string.h>

#include <rtthread.h>

#include "param.h"
#include <common/console/console.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void param_show_group_list(void)
{
    param_group_t* gp = (param_group_t*)&param_list;
    for(int j = 0 ; j < sizeof(param_list)/sizeof(param_group_t) ; j++){
        console_printf("%s:\n", gp->name);
        gp++;
    }
}

void param_dump(void)
{
    param_t* p;
    param_group_t* gp = (param_group_t*)&param_list;
    for(int j = 0 ; j < sizeof(param_list)/sizeof(param_group_t) ; j++){
        console_printf("%s:\n", gp->name);
        p = gp->content;
        for(int i= 0 ; i < gp->param_num ; i++){
            console_printf("%25s: ", p->name);
            if (p->type == PARAM_TYPE_INT8) {
                console_printf("%d\n", p->val.i8);
            }
        
            if (p->type == PARAM_TYPE_UINT8) {
                console_printf("%u\n", p->val.u8);
            }
        
            if (p->type == PARAM_TYPE_INT16) {
                console_printf("%d\n", p->val.i16);
            }
        
            if (p->type == PARAM_TYPE_UINT16) {
                console_printf("%u\n", p->val.u16);
            }
        
            if (p->type == PARAM_TYPE_INT32) {
                console_printf("%d\n", p->val.i32);
            }
        
            if (p->type == PARAM_TYPE_UINT32) {
                console_printf("%u\n", p->val.u32);
            }
        
            if (p->type == PARAM_TYPE_FLOAT) {
                console_printf("%f\n", p->val.f);
            }
        
            if (p->type == PARAM_TYPE_DOUBLE) {
                console_printf("%f\n", p->val.lf);
            }
            p++;
        }
        gp++;
    }
}

int param_dump_group(char* group_name)
{
    param_t* p;
    param_group_t* gp = (param_group_t*)&param_list;
    for(int j = 0 ; j < sizeof(param_list)/sizeof(param_group_t) ; j++){
        if( strcmp(group_name, gp->name)==0 ){
            console_printf("%s:\n", gp->name);
            p = gp->content;
            for(int i= 0 ; i < gp->param_num ; i++){
                console_printf("%25s: ", p->name);
            
                if (p->type == PARAM_TYPE_INT8) {
                    console_printf("%d\n", p->val.i8);
                }
            
                if (p->type == PARAM_TYPE_UINT8) {
                    console_printf("%u\n", p->val.u8);
                }
            
                if (p->type == PARAM_TYPE_INT16) {
                    console_printf("%d\n", p->val.i16);
                }
            
                if (p->type == PARAM_TYPE_UINT16) {
                    console_printf("%u\n", p->val.u16);
                }
            
                if (p->type == PARAM_TYPE_INT32) {
                    console_printf("%d\n", p->val.i32);
                }
            
                if (p->type == PARAM_TYPE_UINT32) {
                    console_printf("%u\n", p->val.u32);
                }
            
                if (p->type == PARAM_TYPE_FLOAT) {
                    console_printf("%f\n", p->val.f);
                }
            
                if (p->type == PARAM_TYPE_DOUBLE) {
                    console_printf("%f\n", p->val.lf);
                }
                p++;
            }
            
            return 1;
        }
        gp++;
    }
    
    return 0;
}

int param_dump_param(char* group_name, char* param_name)
{
    param_t* p = param_get_by_full_name(group_name, param_name);
    
    if(p != NULL){
        console_printf("%25s: ", p->name);
        if (p->type == PARAM_TYPE_INT8) {
            console_printf("%d\n", p->val.i8);
        }
    
        if (p->type == PARAM_TYPE_UINT8) {
            console_printf("%u\n", p->val.u8);
        }
    
        if (p->type == PARAM_TYPE_INT16) {
            console_printf("%d\n", p->val.i16);
        }
    
        if (p->type == PARAM_TYPE_UINT16) {
            console_printf("%u\n", p->val.u16);
        }
    
        if (p->type == PARAM_TYPE_INT32) {
            console_printf("%d\n", p->val.i32);
        }
    
        if (p->type == PARAM_TYPE_UINT32) {
            console_printf("%u\n", p->val.u32);
        }
    
        if (p->type == PARAM_TYPE_FLOAT) {
            console_printf("%f\n", p->val.f);
        }
    
        if (p->type == PARAM_TYPE_DOUBLE) {
            console_printf("%f\n", p->val.lf);
        }
        return 1;
    }
    
    return 0;
}

int param_cmd(int argc, char** argv)
{
    uint8_t group_flag = 0;
    int flag_cnt = 0;
    int param_num;
    
    if(argc > 1){
        for(int i = 0 ; i < argc-1 ; i++){
            if(strcmp(argv[i+1], "-g") == 0 || strcmp(argv[i+1], "--group") == 0){
                group_flag =1;
            }
            if(argv[i+1][0] == '-')
                flag_cnt++;
        }
        
        param_num = argc - flag_cnt;
        
        if(strcmp(argv[1], "load") == 0){
            param_load();
        }
        if(strcmp(argv[1], "store") == 0){
            param_save();
        }
        if(strcmp(argv[1], "get") == 0 && param_num == 2){
            if(group_flag)
                param_show_group_list();
            else
                param_dump();
        }
        if(strcmp(argv[1], "get") == 0 && param_num == 3){
            param_dump_group(argv[2]);
        }
        if(strcmp(argv[1], "get") == 0 && param_num == 4){
            param_dump_param(argv[2], argv[3]);
        }
        if(strcmp(argv[1], "set") == 0 && argc == 5){
            if(param_set_val_by_full_name(argv[2], argv[3], argv[4]))
                console_printf("fail, can not find %s in group %s\n", argv[3], argv[2]);
            else
                console_printf("success, %s in group %s is set to %s\n", argv[3], argv[2], argv[4]);
        }
    }
    
    return 0;
}
MSH_CMD_EXPORT(param_cmd,param_cmd);
/*------------------------------------test------------------------------------*/


