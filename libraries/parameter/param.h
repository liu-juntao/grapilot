
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       param.h
  * @author     baiyang
  * @date       2021-7-12
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <common/gp_config.h>
/*-----------------------------------macro------------------------------------*/
#define PARAM_MAX_NAME_SIZE    16

#define PARAM_DECLARE(_name) param_t __param_##_name

#define PARAM_DEFINE_INT8(_name, _default) \
    {                                      \
        .name = #_name,                    \
        .type = PARAM_TYPE_INT8,           \
        .val.i8 = _default,                \
        .user_param = NULL                 \
    }

#define PARAM_DEFINE_UINT8(_name, _default) \
    {                                       \
        .name = #_name,                     \
        .type = PARAM_TYPE_UINT8,           \
        .val.u8 = _default,                 \
        .user_param = NULL                  \
    }

#define PARAM_DEFINE_INT16(_name, _default) \
    {                                       \
        .name = #_name,                     \
        .type = PARAM_TYPE_INT16,           \
        .val.i16 = _default,                \
        .user_param = NULL                  \
    }

#define PARAM_DEFINE_UINT16(_name, _default) \
    {                                        \
        .name = #_name,                      \
        .type = PARAM_TYPE_UINT16,           \
        .val.u16 = _default,                 \
        .user_param = NULL                   \
    }

#define PARAM_DEFINE_INT32(_name, _default) \
    {                                       \
        .name = #_name,                     \
        .type = PARAM_TYPE_INT32,           \
        .val.i32 = _default,                \
        .user_param = NULL                  \
    }

#define PARAM_DEFINE_UINT32(_name, _default) \
    {                                        \
        .name = #_name,                      \
        .type = PARAM_TYPE_UINT32,           \
        .val.u32 = _default,                 \
        .user_param = NULL                   \
    }

#define PARAM_DEFINE_FLOAT(_name, _default) \
    {                                       \
        .name = #_name,                     \
        .type = PARAM_TYPE_FLOAT,           \
        .val.f = _default,                  \
        .user_param = NULL                  \
    }

#define PARAM_DEFINE_DOUBLE(_name, _default) \
    {                                        \
        .name = #_name,                      \
        .type = PARAM_TYPE_DOUBLE,           \
        .val.lf = _default,                  \
        .user_param = NULL                   \
    }

#define PARAM_GROUP(_group)         _param_##_group
#define PARAM_DECLARE_GROUP(_group) _param_##_group##_t
#define PARAM_DEFINE_GROUP(_group)                                  \
    {                                                               \
        .name = #_group,                                            \
        .param_num = sizeof(_param_##_group##_t) / sizeof(param_t), \
        .content = (param_t*)&_param_##_group##_t                   \
    }


#define PARAM_ID(_group, _name) &(((_param_##_group*)(param_list._param_##_group.content))->__param_##_name)
#define PARAM_GET(_group, _name) ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name

#define PARAM_GET_INT8(_group, _name)   ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.i8
#define PARAM_GET_UINT8(_group, _name)  ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.u8
#define PARAM_GET_INT16(_group, _name)  ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.i16
#define PARAM_GET_UINT16(_group, _name) ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.u16
#define PARAM_GET_INT32(_group, _name)  ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.i32
#define PARAM_GET_UINT32(_group, _name) ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.u32
#define PARAM_GET_FLOAT(_group, _name)  ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.f
#define PARAM_GET_DOUBLE(_group, _name) ((_param_##_group*)(param_list._param_##_group.content))->__param_##_name.val.lf
/*----------------------------------typedef-----------------------------------*/
/*定义参数/组包括4个步骤
     *步骤1：声明组
     *步骤2：在组中声明参数
     *步骤3：定义组
     *步骤4：在组中定义参数
*/

enum param_type_t {
    PARAM_TYPE_INT8 = 0,
    PARAM_TYPE_UINT8,
    PARAM_TYPE_INT16,
    PARAM_TYPE_UINT16,
    PARAM_TYPE_INT32,
    PARAM_TYPE_UINT32,
    PARAM_TYPE_FLOAT,
    PARAM_TYPE_DOUBLE,
    PARAM_TYPE_UNKNOWN = 0xFF
};

typedef enum {
    PARAM_PARSE_START = 0,
    PARAM_PARSE_LIST,
    PARAM_PARSE_GROUP_INFO,
    PARAM_PARSE_GROUP_NAME,
    PARAM_PARSE_GROUP,
    PARAM_PARSE_PARAM,
    PARAM_PARSE_PARAM_NAME,
    PARAM_PARSE_PARAM_VAL,
    PARAM_PARSE_PARAM_VAL_CONTENT,
} PARAM_PARSE_STATE;

typedef union {
    int8_t i8;
    uint8_t u8;
    int16_t i16;
    uint16_t u16;
    int32_t i32;
    uint32_t u32;
    float f;
    double lf;
} param_value_t;

typedef struct {
    const char* name;
    const uint8_t type;
    param_value_t val;
    void* user_param;
} param_t;

typedef struct {
    const char* name;
    const uint32_t param_num;
    param_t* content;
} param_group_t;

/*---------------------------------步骤二：在组中声明参数-------------------------------*/
typedef struct
{
    PARAM_DECLARE(PSC_POSZ_P);
    
    PARAM_DECLARE(PSC_VELZ_P);
    PARAM_DECLARE(PSC_VELZ_I);
    PARAM_DECLARE(PSC_VELZ_D);
    PARAM_DECLARE(PSC_VELZ_IMAX);
    PARAM_DECLARE(PSC_VELZ_FF);
    PARAM_DECLARE(PSC_VELZ_FLTD);
    PARAM_DECLARE(PSC_VELZ_FLTE);
    
    PARAM_DECLARE(PSC_ACCZ_P);
    PARAM_DECLARE(PSC_ACCZ_I);
    PARAM_DECLARE(PSC_ACCZ_D);
    PARAM_DECLARE(PSC_ACCZ_IMAX);
    PARAM_DECLARE(PSC_ACCZ_FF);
    PARAM_DECLARE(PSC_ACCZ_FLTD);
    PARAM_DECLARE(PSC_ACCZ_FLTE);
    PARAM_DECLARE(PSC_ACCZ_FLTT);
    PARAM_DECLARE(PSC_ACCZ_SMAX);
    
    PARAM_DECLARE(PSC_POSXY_P);
    
    PARAM_DECLARE(PSC_VELXY_P);
    PARAM_DECLARE(PSC_VELXY_I);
    PARAM_DECLARE(PSC_VELXY_D);
    PARAM_DECLARE(PSC_VELXY_IMAX);
    PARAM_DECLARE(PSC_VELXY_FF);
    PARAM_DECLARE(PSC_VELXY_FLTD);
    PARAM_DECLARE(PSC_VELXY_FLTE);

    PARAM_DECLARE(PSC_ANGLE_MAX);
    PARAM_DECLARE(PSC_JERK_XY);
    PARAM_DECLARE(PSC_JERK_Z);

}PARAM_GROUP(POS_CTRL);

typedef struct
{
    PARAM_DECLARE(INS_GYROFFS_X);
    PARAM_DECLARE(INS_GYROFFS_Y);
    PARAM_DECLARE(INS_GYROFFS_Z);
    PARAM_DECLARE(INS_GYR2OFFS_X);
    PARAM_DECLARE(INS_GYR2OFFS_Y);
    PARAM_DECLARE(INS_GYR2OFFS_Z);
    PARAM_DECLARE(INS_GYR3OFFS_X);
    PARAM_DECLARE(INS_GYR3OFFS_Y);
    PARAM_DECLARE(INS_GYR3OFFS_Z);
    PARAM_DECLARE(INS_ACCSCAL_X);
    PARAM_DECLARE(INS_ACCSCAL_Y);
    PARAM_DECLARE(INS_ACCSCAL_Z);
    PARAM_DECLARE(INS_ACCOFFS_X);
    PARAM_DECLARE(INS_ACCOFFS_Y);
    PARAM_DECLARE(INS_ACCOFFS_Z);
    PARAM_DECLARE(INS_ACC2SCAL_X);
    PARAM_DECLARE(INS_ACC2SCAL_Y);
    PARAM_DECLARE(INS_ACC2SCAL_Z);
    PARAM_DECLARE(INS_ACC2OFFS_X);
    PARAM_DECLARE(INS_ACC2OFFS_Y);
    PARAM_DECLARE(INS_ACC2OFFS_Z);
    PARAM_DECLARE(INS_ACC3SCAL_X);
    PARAM_DECLARE(INS_ACC3SCAL_Y);
    PARAM_DECLARE(INS_ACC3SCAL_Z);
    PARAM_DECLARE(INS_ACC3OFFS_X);
    PARAM_DECLARE(INS_ACC3OFFS_Y);
    PARAM_DECLARE(INS_ACC3OFFS_Z);
    PARAM_DECLARE(INS_GYRO_FILTER);
    PARAM_DECLARE(INS_ACCEL_FILTER);
    PARAM_DECLARE(INS_USE);
    PARAM_DECLARE(INS_USE2);
    PARAM_DECLARE(INS_USE3);
    PARAM_DECLARE(INS_STILL_THRESH);
    PARAM_DECLARE(INS_GYR_CAL);
    PARAM_DECLARE(INS_TRIM_OPTION);
    PARAM_DECLARE(INS_ACC_BODYFIX);
    PARAM_DECLARE(INS_POS1_X);
    PARAM_DECLARE(INS_POS1_Y);
    PARAM_DECLARE(INS_POS1_Z);
    PARAM_DECLARE(INS_POS2_X);
    PARAM_DECLARE(INS_POS2_Y);
    PARAM_DECLARE(INS_POS2_Z);
    PARAM_DECLARE(INS_POS3_X);
    PARAM_DECLARE(INS_POS3_Y);
    PARAM_DECLARE(INS_POS3_Z);
    PARAM_DECLARE(INS_GYR_ID);
    PARAM_DECLARE(INS_GYR2_ID);
    PARAM_DECLARE(INS_GYR3_ID);
    PARAM_DECLARE(INS_ACC_ID);
    PARAM_DECLARE(INS_ACC2_ID);
    PARAM_DECLARE(INS_ACC3_ID);
    PARAM_DECLARE(INS_FAST_SAMPLE);
    PARAM_DECLARE(INS_ENABLE_MASK);
    PARAM_DECLARE(INS_GYRO_RATE);
}PARAM_GROUP(INS);

typedef struct
{
    PARAM_DECLARE(BARO1_GND_PRESS);
    PARAM_DECLARE(BARO_GND_TEMP);
    PARAM_DECLARE(BARO_ALT_OFFSET);
    PARAM_DECLARE(BARO_PRIMARY);
    PARAM_DECLARE(BARO_EXT_BUS);
    PARAM_DECLARE(BARO_SPEC_GRAV);
    PARAM_DECLARE(BARO2_GND_PRESS);
    PARAM_DECLARE(BARO3_GND_PRESS);
    PARAM_DECLARE(BARO_FLTR_RNG);
    PARAM_DECLARE(BARO_PROBE_EXT);
    PARAM_DECLARE(BARO1_DEVID);
    PARAM_DECLARE(BARO2_DEVID);
    PARAM_DECLARE(BARO3_DEVID);
}PARAM_GROUP(BARO);

typedef struct
{
    PARAM_DECLARE(COMPASS_USE);
    PARAM_DECLARE(COMPASS_OFS_X);
    PARAM_DECLARE(COMPASS_OFS_Y);
    PARAM_DECLARE(COMPASS_OFS_Z);
    PARAM_DECLARE(COMPASS_DEC);
    PARAM_DECLARE(COMPASS_LEARN);
    PARAM_DECLARE(COMPASS_AUTODEC);
    PARAM_DECLARE(COMPASS_MOTCT);
    PARAM_DECLARE(COMPASS_MOT_X);
    PARAM_DECLARE(COMPASS_MOT_Y);
    PARAM_DECLARE(COMPASS_MOT_Z);
    PARAM_DECLARE(COMPASS_ORIENT);
    PARAM_DECLARE(COMPASS_EXTERNAL);

    PARAM_DECLARE(COMPASS_USE2);
    PARAM_DECLARE(COMPASS_OFS2_X);
    PARAM_DECLARE(COMPASS_OFS2_Y);
    PARAM_DECLARE(COMPASS_OFS2_Z);
    PARAM_DECLARE(COMPASS_MOT2_X);
    PARAM_DECLARE(COMPASS_MOT2_Y);
    PARAM_DECLARE(COMPASS_MOT2_Z);
    PARAM_DECLARE(COMPASS_ORIENT2);
    PARAM_DECLARE(COMPASS_EXTERN2);

    PARAM_DECLARE(COMPASS_USE3);
    PARAM_DECLARE(COMPASS_OFS3_X);
    PARAM_DECLARE(COMPASS_OFS3_Y);
    PARAM_DECLARE(COMPASS_OFS3_Z);
    PARAM_DECLARE(COMPASS_MOT3_X);
    PARAM_DECLARE(COMPASS_MOT3_Y);
    PARAM_DECLARE(COMPASS_MOT3_Z);
    PARAM_DECLARE(COMPASS_ORIENT3);
    PARAM_DECLARE(COMPASS_EXTERN3);

    PARAM_DECLARE(COMPASS_DEV_ID);
    PARAM_DECLARE(COMPASS_DEV_ID2);
    PARAM_DECLARE(COMPASS_DEV_ID3);
    PARAM_DECLARE(COMPASS_DEV_ID4);
    PARAM_DECLARE(COMPASS_DEV_ID5);
    PARAM_DECLARE(COMPASS_DEV_ID6);
    PARAM_DECLARE(COMPASS_DEV_ID7);
    PARAM_DECLARE(COMPASS_DEV_ID8);

    PARAM_DECLARE(COMPASS_DIA_X);
    PARAM_DECLARE(COMPASS_DIA_Y);
    PARAM_DECLARE(COMPASS_DIA_Z);
    PARAM_DECLARE(COMPASS_ODI_X);
    PARAM_DECLARE(COMPASS_ODI_Y);
    PARAM_DECLARE(COMPASS_ODI_Z);

    PARAM_DECLARE(COMPASS_DIA2_X);
    PARAM_DECLARE(COMPASS_DIA2_Y);
    PARAM_DECLARE(COMPASS_DIA2_Z);
    PARAM_DECLARE(COMPASS_ODI2_X);
    PARAM_DECLARE(COMPASS_ODI2_Y);
    PARAM_DECLARE(COMPASS_ODI2_Z);

    PARAM_DECLARE(COMPASS_DIA3_X);
    PARAM_DECLARE(COMPASS_DIA3_Y);
    PARAM_DECLARE(COMPASS_DIA3_Z);
    PARAM_DECLARE(COMPASS_ODI3_X);
    PARAM_DECLARE(COMPASS_ODI3_Y);
    PARAM_DECLARE(COMPASS_ODI3_Z);
    
    PARAM_DECLARE(COMPASS_CAL_FIT);
    PARAM_DECLARE(COMPASS_TYPEMASK);
    PARAM_DECLARE(COMPASS_OFFS_MAX);
    PARAM_DECLARE(COMPASS_FLTR_RNG);
    PARAM_DECLARE(COMPASS_AUTO_ROT);
    PARAM_DECLARE(COMPASS_PRIO1_ID);
    PARAM_DECLARE(COMPASS_PRIO2_ID);
    PARAM_DECLARE(COMPASS_PRIO3_ID);
    
    PARAM_DECLARE(COMPASS_ENABLE);

    PARAM_DECLARE(COMPASS_SCALE);
    PARAM_DECLARE(COMPASS_SCALE2);
    PARAM_DECLARE(COMPASS_SCALE3);

    PARAM_DECLARE(COMPASS_OPTIONS);

    PARAM_DECLARE(COMPASS_CUS_ROLL);
    PARAM_DECLARE(COMPASS_CUS_PIT);
    PARAM_DECLARE(COMPASS_CUS_YAW);
}PARAM_GROUP(COMPASS);

typedef struct
{
    PARAM_DECLARE(ATC_SLEW_YAW);
    PARAM_DECLARE(ATC_ANG_RLL_P);
    PARAM_DECLARE(ATC_RAT_RLL_P);
    PARAM_DECLARE(ATC_RAT_RLL_I);
    PARAM_DECLARE(ATC_RAT_RLL_D);
    PARAM_DECLARE(ATC_RAT_RLL_IMAX);
    PARAM_DECLARE(ATC_RAT_RLL_FLTT);
    PARAM_DECLARE(ATC_RAT_RLL_FLTE);
    PARAM_DECLARE(ATC_RAT_RLL_FLTD);
    PARAM_DECLARE(ATC_ANG_PIT_P);
    PARAM_DECLARE(ATC_RAT_PIT_P);
    PARAM_DECLARE(ATC_RAT_PIT_I);
    PARAM_DECLARE(ATC_RAT_PIT_D);
    PARAM_DECLARE(ATC_RAT_PIT_IMAX);
    PARAM_DECLARE(ATC_RAT_PIT_FLTT);
    PARAM_DECLARE(ATC_RAT_PIT_FLTE);
    PARAM_DECLARE(ATC_RAT_PIT_FLTD);
    PARAM_DECLARE(ATC_ANG_YAW_P);
    PARAM_DECLARE(ATC_RAT_YAW_P);
    PARAM_DECLARE(ATC_RAT_YAW_I);
    PARAM_DECLARE(ATC_RAT_YAW_D);
    PARAM_DECLARE(ATC_RAT_YAW_IMAX);
    PARAM_DECLARE(ATC_RAT_YAW_FLTT);
    PARAM_DECLARE(ATC_RAT_YAW_FLTE);
    PARAM_DECLARE(ATC_RAT_YAW_FLTD);
    PARAM_DECLARE(ATC_THR_MIX_MIN);
    PARAM_DECLARE(ATC_THR_MIX_MAN);
    PARAM_DECLARE(ATC_THR_MIX_MAX);
    PARAM_DECLARE(ATC_ACCEL_R_MAX);
    PARAM_DECLARE(ATC_ACCEL_P_MAX);
    PARAM_DECLARE(ATC_ACCEL_Y_MAX);
    PARAM_DECLARE(ATC_RATE_R_MAX);
    PARAM_DECLARE(ATC_RATE_P_MAX);
    PARAM_DECLARE(ATC_RATE_Y_MAX);
    PARAM_DECLARE(ATC_RATE_FF_EN);
    PARAM_DECLARE(ATC_ANGLE_BOOST);
    PARAM_DECLARE(ATC_INPUT_TC);
    PARAM_DECLARE(ATC_ANG_LIM_TC);
}PARAM_GROUP(ATT_CTRL);

typedef struct {
    PARAM_DECLARE(BLOG_MODE);
    PARAM_DECLARE(ANGLE_MAX);
    PARAM_DECLARE(FRAME_CLASS);
    PARAM_DECLARE(FRAME_TYPE);
    PARAM_DECLARE(FS_THR_ENABLE);
    PARAM_DECLARE(FS_THR_VALUE);
    PARAM_DECLARE(THR_DZ);
    PARAM_DECLARE(BLOG_IMU_SPEED);
    PARAM_DECLARE(PILOT_THR_FILT);
    PARAM_DECLARE(PILOT_Y_RATE);
    PARAM_DECLARE(ACRO_Y_EXPO);
    PARAM_DECLARE(PILOT_THR_BHV);
    PARAM_DECLARE(DISARM_DELAY);
    PARAM_DECLARE(PILOT_SPEED_UP);
    PARAM_DECLARE(PILOT_SPEED_DN);
    PARAM_DECLARE(PILOT_ACCEL_Z);
    PARAM_DECLARE(PILOT_TKOFF_ALT);
    PARAM_DECLARE(AHRS_ORIENTATION);
    PARAM_DECLARE(ESC_CALIBRATION);
    PARAM_DECLARE(GPS_HDOP_GOOD);
    PARAM_DECLARE(WP_YAW_BEHAVIOR);
    PARAM_DECLARE(LAND_ALT_LOW);
    PARAM_DECLARE(LAND_SPEED);
    PARAM_DECLARE(LAND_SPEED_HIGH);
    PARAM_DECLARE(LAND_REPOSITION);
    PARAM_DECLARE(WP_NAVALT_MIN);
    PARAM_DECLARE(FS_OPTIONS);
    PARAM_DECLARE(FS_GCS_ENABLE);
    PARAM_DECLARE(FS_GCS_TIMEOUT);
} PARAM_GROUP(VEHICLE);

typedef struct {
    PARAM_DECLARE(WPNAV_SPEED);
    PARAM_DECLARE(WPNAV_RADIUS);
    PARAM_DECLARE(WPNAV_SPEED_UP);
    PARAM_DECLARE(WPNAV_SPEED_DN);
    PARAM_DECLARE(WPNAV_ACCEL);
    PARAM_DECLARE(WPNAV_ACCEL_Z);
    PARAM_DECLARE(WPNAV_RFND_USE);
    PARAM_DECLARE(WPNAV_JERK);
    PARAM_DECLARE(WPNAV_TER_MARGIN);
} PARAM_GROUP(WPNAV);

typedef struct {
    PARAM_DECLARE(LOITER_ANG_MAX);
    PARAM_DECLARE(LOITER_SPEED);
    PARAM_DECLARE(LOITER_ACC_MAX);
    PARAM_DECLARE(LOITER_BK_ACC);
    PARAM_DECLARE(LOITER_BK_JERK);
    PARAM_DECLARE(LOITER_BK_DELAY);
} PARAM_GROUP(LOITER);

typedef struct {
    PARAM_DECLARE(RCMAP_ROLL);
    PARAM_DECLARE(RCMAP_PITCH);
    PARAM_DECLARE(RCMAP_YAW);
    PARAM_DECLARE(RCMAP_THROTTLE);
    PARAM_DECLARE(RC1_MIN);
    PARAM_DECLARE(RC1_MAX);
    PARAM_DECLARE(RC1_TRIM);
    PARAM_DECLARE(RC1_DZ);
    PARAM_DECLARE(RC1_OPTION);
    PARAM_DECLARE(RC2_MIN);
    PARAM_DECLARE(RC2_MAX);
    PARAM_DECLARE(RC2_TRIM);
    PARAM_DECLARE(RC2_DZ);
    PARAM_DECLARE(RC2_OPTION);
    PARAM_DECLARE(RC3_MIN);
    PARAM_DECLARE(RC3_MAX);
    PARAM_DECLARE(RC3_TRIM);
    PARAM_DECLARE(RC3_DZ);
    PARAM_DECLARE(RC3_OPTION);
    PARAM_DECLARE(RC4_MIN);
    PARAM_DECLARE(RC4_MAX);
    PARAM_DECLARE(RC4_TRIM);
    PARAM_DECLARE(RC4_DZ);
    PARAM_DECLARE(RC4_OPTION);
    PARAM_DECLARE(RC5_MIN);
    PARAM_DECLARE(RC5_MAX);
    PARAM_DECLARE(RC5_TRIM);
    PARAM_DECLARE(RC5_DZ);
    PARAM_DECLARE(RC5_OPTION);
    PARAM_DECLARE(RC6_MIN);
    PARAM_DECLARE(RC6_MAX);
    PARAM_DECLARE(RC6_TRIM);
    PARAM_DECLARE(RC6_DZ);
    PARAM_DECLARE(RC6_OPTION);
    PARAM_DECLARE(RC7_MIN);
    PARAM_DECLARE(RC7_MAX);
    PARAM_DECLARE(RC7_TRIM);
    PARAM_DECLARE(RC7_DZ);
    PARAM_DECLARE(RC7_OPTION);
    PARAM_DECLARE(RC8_MIN);
    PARAM_DECLARE(RC8_MAX);
    PARAM_DECLARE(RC8_TRIM);
    PARAM_DECLARE(RC8_DZ);
    PARAM_DECLARE(RC8_OPTION);
    PARAM_DECLARE(RC9_MIN);
    PARAM_DECLARE(RC9_MAX);
    PARAM_DECLARE(RC9_TRIM);
    PARAM_DECLARE(RC9_DZ);
    PARAM_DECLARE(RC9_OPTION);
    PARAM_DECLARE(RC10_MIN);
    PARAM_DECLARE(RC10_MAX);
    PARAM_DECLARE(RC10_TRIM);
    PARAM_DECLARE(RC10_DZ);
    PARAM_DECLARE(RC10_OPTION);
    PARAM_DECLARE(RC11_MIN);
    PARAM_DECLARE(RC11_MAX);
    PARAM_DECLARE(RC11_TRIM);
    PARAM_DECLARE(RC11_DZ);
    PARAM_DECLARE(RC11_OPTION);
    PARAM_DECLARE(RC12_MIN);
    PARAM_DECLARE(RC12_MAX);
    PARAM_DECLARE(RC12_TRIM);
    PARAM_DECLARE(RC12_DZ);
    PARAM_DECLARE(RC12_OPTION);
    PARAM_DECLARE(RC13_MIN);
    PARAM_DECLARE(RC13_MAX);
    PARAM_DECLARE(RC13_TRIM);
    PARAM_DECLARE(RC13_DZ);
    PARAM_DECLARE(RC13_OPTION);
    PARAM_DECLARE(RC14_MIN);
    PARAM_DECLARE(RC14_MAX);
    PARAM_DECLARE(RC14_TRIM);
    PARAM_DECLARE(RC14_DZ);
    PARAM_DECLARE(RC14_OPTION);
    PARAM_DECLARE(RC15_MIN);
    PARAM_DECLARE(RC15_MAX);
    PARAM_DECLARE(RC15_TRIM);
    PARAM_DECLARE(RC15_DZ);
    PARAM_DECLARE(RC15_OPTION);
    PARAM_DECLARE(RC16_MIN);
    PARAM_DECLARE(RC16_MAX);
    PARAM_DECLARE(RC16_TRIM);
    PARAM_DECLARE(RC16_DZ);
    PARAM_DECLARE(RC16_OPTION);
    PARAM_DECLARE(RC17_MIN);
    PARAM_DECLARE(RC17_MAX);
    PARAM_DECLARE(RC17_TRIM);
    PARAM_DECLARE(RC17_DZ);
    PARAM_DECLARE(RC17_OPTION);
    PARAM_DECLARE(RC18_MIN);
    PARAM_DECLARE(RC18_MAX);
    PARAM_DECLARE(RC18_TRIM);
    PARAM_DECLARE(RC18_DZ);
    PARAM_DECLARE(RC18_OPTION);
} PARAM_GROUP(RC);

typedef struct {
    PARAM_DECLARE(FLTMODE_CH);
    PARAM_DECLARE(FLTMODE1);
    PARAM_DECLARE(FLTMODE2);
    PARAM_DECLARE(FLTMODE3);
    PARAM_DECLARE(FLTMODE4);
    PARAM_DECLARE(FLTMODE5);
    PARAM_DECLARE(FLTMODE6);
} PARAM_GROUP(FLIGHTMODE);

typedef struct {
    PARAM_DECLARE(SERIAL1_PROTOCOL);
    PARAM_DECLARE(SERIAL1_BAUD);
    PARAM_DECLARE(SERIAL2_PROTOCOL);
    PARAM_DECLARE(SERIAL2_BAUD);
    PARAM_DECLARE(SERIAL3_PROTOCOL);
    PARAM_DECLARE(SERIAL3_BAUD);
    PARAM_DECLARE(SERIAL4_PROTOCOL);
    PARAM_DECLARE(SERIAL4_BAUD);
    PARAM_DECLARE(SERIAL5_PROTOCOL);
    PARAM_DECLARE(SERIAL5_BAUD);
    PARAM_DECLARE(SERIAL6_PROTOCOL);
    PARAM_DECLARE(SERIAL6_BAUD);
    PARAM_DECLARE(SERIAL7_PROTOCOL);
    PARAM_DECLARE(SERIAL7_BAUD);
    PARAM_DECLARE(SERIAL8_PROTOCOL);
    PARAM_DECLARE(SERIAL8_BAUD);
} PARAM_GROUP(SERIAL);

typedef struct {
    PARAM_DECLARE(MOT_YAW_HEADROOM);
    PARAM_DECLARE(MOT_THST_EXPO);
    PARAM_DECLARE(MOT_SPIN_MAX);
    PARAM_DECLARE(MOT_BAT_VOLT_MAX);
    PARAM_DECLARE(MOT_BAT_VOLT_MIN);
    PARAM_DECLARE(MOT_BAT_CURR_MAX);
    PARAM_DECLARE(MOT_PWM_TYPE);
    PARAM_DECLARE(MOT_PWM_MIN);
    PARAM_DECLARE(MOT_PWM_MAX);
    PARAM_DECLARE(MOT_SPIN_MIN);
    PARAM_DECLARE(MOT_SPIN_ARM);
    PARAM_DECLARE(MOT_BAT_CURR_TC);
    PARAM_DECLARE(MOT_THST_HOVER);
    PARAM_DECLARE(MOT_HOVER_LEARN);
    PARAM_DECLARE(MOT_SAFE_DISARM);
    PARAM_DECLARE(MOT_YAW_SV_ANGLE);
    PARAM_DECLARE(MOT_SPOOL_TIME);
    PARAM_DECLARE(MOT_BOOST_SCALE);
    PARAM_DECLARE(MOT_BAT_IDX);
    PARAM_DECLARE(MOT_SLEW_UP_TIME);
    PARAM_DECLARE(MOT_SLEW_DN_TIME);
    PARAM_DECLARE(MOT_SAFE_TIME);
} PARAM_GROUP(MOTOR);

typedef struct
{
    PARAM_DECLARE(ARMING_REQUIRE);
    PARAM_DECLARE(ARMING_ACCTHRESH);
    PARAM_DECLARE(ARMING_RUDDER);
    PARAM_DECLARE(ARMING_MIS_ITEMS);
    PARAM_DECLARE(ARMING_CHECK);
}PARAM_GROUP(ARMING);

typedef struct
{
    PARAM_DECLARE(NTF_LED_BRIGHT);
    PARAM_DECLARE(NTF_BUZZ_TYPES);
    PARAM_DECLARE(NTF_LED_OVERRIDE);
    PARAM_DECLARE(NTF_BUZZ_PIN);
    PARAM_DECLARE(NTF_LED_TYPES);
    PARAM_DECLARE(NTF_BUZZ_ON_LVL);
    PARAM_DECLARE(NTF_BUZZ_VOLUME);
    PARAM_DECLARE(NTF_LED_LEN);
}PARAM_GROUP(NTF);

typedef struct
{
    PARAM_DECLARE(GPS_TYPE);
    PARAM_DECLARE(GPS_TYPE2);
    PARAM_DECLARE(GPS_NAVFILTER);
    PARAM_DECLARE(GPS_AUTO_SWITCH);
    PARAM_DECLARE(GPS_MIN_DGPS);
    PARAM_DECLARE(GPS_SBAS_MODE);
    PARAM_DECLARE(GPS_MIN_ELEV);
    PARAM_DECLARE(GPS_INJECT_TO);
    PARAM_DECLARE(GPS_SBP_LOGMASK);
    PARAM_DECLARE(GPS_RAW_DATA);
    PARAM_DECLARE(GPS_GNSS_MODE);
    PARAM_DECLARE(GPS_SAVE_CFG);
    PARAM_DECLARE(GPS_GNSS_MODE2);
    PARAM_DECLARE(GPS_AUTO_CONFIG);
    PARAM_DECLARE(GPS_RATE_MS);
    PARAM_DECLARE(GPS_RATE_MS2);
    PARAM_DECLARE(GPS_POS1_X);
    PARAM_DECLARE(GPS_POS1_Y);
    PARAM_DECLARE(GPS_POS1_Z);
    PARAM_DECLARE(GPS_POS2_X);
    PARAM_DECLARE(GPS_POS2_Y);
    PARAM_DECLARE(GPS_POS2_Z);
    PARAM_DECLARE(GPS_DELAY_MS);
    PARAM_DECLARE(GPS_DELAY_MS2);
    PARAM_DECLARE(GPS_BLEND_MASK);
    PARAM_DECLARE(GPS_BLEND_TC);
    PARAM_DECLARE(GPS_DRV_OPTIONS);
    PARAM_DECLARE(GPS_COM_PORT);
    PARAM_DECLARE(GPS_COM_PORT2);
    PARAM_DECLARE(GPS_PRIMARY);
}PARAM_GROUP(GPS);

typedef struct
{
    PARAM_DECLARE(LOIT_ANG_MAX);
    PARAM_DECLARE(LOIT_SPEED);
    PARAM_DECLARE(LOIT_ACC_MAX);
    PARAM_DECLARE(LOIT_BRK_ACCEL);
    PARAM_DECLARE(LOIT_BRK_JERK);
    PARAM_DECLARE(LOIT_BRK_DELAY);
}PARAM_GROUP(LOIT);

typedef struct {
    PARAM_DECLARE(RTL_ALT);
    PARAM_DECLARE(RTL_CONE_SLOPE);
    PARAM_DECLARE(RTL_SPEED);
    PARAM_DECLARE(RTL_ALT_FINAL);
    PARAM_DECLARE(RTL_CLIMB_MIN);
    PARAM_DECLARE(RTL_LOIT_TIME);
    PARAM_DECLARE(RTL_ALT_TYPE);
    PARAM_DECLARE(RTL_OPTIONS);
} PARAM_GROUP(RTL);

typedef struct {
    PARAM_DECLARE(SYSID_THISMAV);
    PARAM_DECLARE(SYSID_MYGCS);
} PARAM_GROUP(GCS);
/*---------------------------------步骤一：声明组-----------------------------*/
typedef struct{
    param_group_t    PARAM_GROUP(INS);
    param_group_t    PARAM_GROUP(ATT_CTRL);
    param_group_t    PARAM_GROUP(POS_CTRL);
    param_group_t    PARAM_GROUP(VEHICLE);
    param_group_t    PARAM_GROUP(WPNAV);
    param_group_t    PARAM_GROUP(LOITER);
    param_group_t    PARAM_GROUP(RC);
    param_group_t    PARAM_GROUP(FLIGHTMODE);
    param_group_t    PARAM_GROUP(SERIAL);
    param_group_t    PARAM_GROUP(MOTOR);
    param_group_t    PARAM_GROUP(ARMING);
    param_group_t    PARAM_GROUP(BARO);
    param_group_t    PARAM_GROUP(COMPASS);
    param_group_t    PARAM_GROUP(NTF);
    param_group_t    PARAM_GROUP(GPS);
    param_group_t    PARAM_GROUP(LOIT);
    param_group_t    PARAM_GROUP(RTL);
    param_group_t    PARAM_GROUP(GCS);
}param_list_t;

/*----------------------------------variable----------------------------------*/
extern param_list_t param_list;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void param_init(void);
void param_interface_init(void);

void param_save(void);
void param_save_to_file(void);
void param_save_if_needed(void);

void param_load(void);

param_t* param_get_by_index(uint32_t index);
param_t* param_get_by_name(const char* param_name);
param_t* param_get_by_full_name(char* group_name, char* param_name);

int param_set_val(param_t* param, void *val);
int param_set_val_by_full_name(char* group_name, char* param_name, char* val);

uint32_t param_get_count(void);
int param_get_index(const param_t* param);
int param_get_index_by_name(char* param_name);

void param_traverse(void (*param_ops)(param_t* param));

void param_link_variable(param_t* param, void* variable);
void param_set_obj(param_t* param, double _value);
void param_set_obj_and_notify(param_t* param, double _value);
void param_save_obj(param_t* param);
void param_set_and_save(param_t* param, double _value);
void param_set_and_save_ifchanged(param_t* param, double _value);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



