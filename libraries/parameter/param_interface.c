
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       param_interface.c
  * @author     baiyang
  * @date       2021-7-12
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "param.h"

#include <rtthread.h>
/*-----------------------------------macro------------------------------------*/
#define EVENT_SAVE_PARAM (1 << 0)
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static struct rt_event param_event;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
void param_interface_init()
{
    rt_event_init(&param_event, "param", RT_IPC_FLAG_FIFO);
}

/**
  * @brief       
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
void param_save()
{
    rt_event_send(&param_event, EVENT_SAVE_PARAM);
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void param_save_if_needed()
{
    rt_uint32_t recv_set = 0;
    rt_uint32_t wait_set = EVENT_SAVE_PARAM;

    rt_err_t rt_err = rt_event_recv(&param_event, wait_set, RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
            0, &recv_set);

    if (recv_set & EVENT_SAVE_PARAM) {
        param_save_to_file();
    }
}

/*------------------------------------test------------------------------------*/


