
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_accel_cal.h
  * @author     baiyang
  * @date       2022-3-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#include "AccelCalibrator.h"
#include <mavproxy/mavproxy.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef  struct accel_cal* accel_cal_t;

/** @ 
  * @brief  
  */
struct accel_cal {
    bool _use_gcs_snoop;
    bool _waiting_for_mavlink_ack;
    uint32_t _last_position_request_ms;
    uint8_t _step;
    enum accel_cal_status_t _status;
    enum accel_cal_status_t _last_result;

    bool _started;
    bool _saving;

    uint8_t _num_active_calibrators;
};
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void accel_cal_ctor(accel_cal_t acc_cal);
bool accel_cal_running(accel_cal_t acc_cal);
void accel_cal_update(accel_cal_t acc_cal);
void accel_cal_start(accel_cal_t acc_cal);
void accel_cal_cancel(accel_cal_t acc_cal);
accelcal_t accel_cal_get_calibrator(uint8_t index);
void accel_cal_handleMessage(accel_cal_t acc_cal, const mavlink_message_t *msg);
bool accel_cal_gcs_vehicle_position(accel_cal_t acc_cal, float position);

// get the status of the calibrator server as a whole
static inline enum accel_cal_status_t accel_cal_get_status(accel_cal_t acc_cal) { return acc_cal->_status; }
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



