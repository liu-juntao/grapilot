
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_imu_invensense.c
  * @author     baiyang
  * @date       2021-10-21
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include  "sensor_imu_invensense.h"
#include  "sensor_imu_invensense_registers.h"

#include <common/console/console.h>
#include <common/time/gp_time.h>
/*-----------------------------------macro------------------------------------*/
#define MPU_SAMPLE_SIZE 14
#define MPU_FIFO_BUFFER_LEN 8

/*
  EXT_SYNC allows for frame synchronisation with an external device
  such as a camera. When enabled the LSB of AccelZ holds the FSYNC bit
 */
#ifndef INVENSENSE_EXT_SYNC_ENABLE
#define INVENSENSE_EXT_SYNC_ENABLE 0
#endif

// code to debug unexpected register changes
#define INVENSENSE_DEBUG_REG_CHANGE 0

#define int16_val(v, idx) ((int16_t)(((uint16_t)v[2*idx] << 8) | v[2*idx+1]))
#define uint16_val(v, idx)(((uint16_t)v[2*idx] << 8) | v[2*idx+1])
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
static bool _init(sensor_imu_invensense *invensense);
static bool _hardware_init(sensor_imu_invensense *invensense);
static bool _block_read(sensor_imu_invensense *invensense, uint8_t reg, uint8_t *buf, uint32_t size);
static uint8_t _register_read(sensor_imu_invensense *invensense, uint8_t reg);
static void _register_write(sensor_imu_invensense *invensense, uint8_t reg, uint8_t val, bool checked);
static bool _check_whoami(sensor_imu_invensense *invensense);
static bool _data_ready(sensor_imu_invensense *invensense);
static void _fifo_reset(sensor_imu_invensense *invensense, bool log_error);
static void _set_filter_register(sensor_imu_invensense *invensense);
static void _poll_data(void *parameter);
#if INVENSENSE_DEBUG_REG_CHANGE
static void _check_register_change(sensor_imu_invensense *invensense);
#endif
static void _read_fifo(sensor_imu_invensense *invensense);
static bool _accumulate(sensor_imu_invensense *invensense, uint8_t *samples, uint8_t n_samples);
static bool _accumulate_sensor_rate_sampling(sensor_imu_invensense *invensense, uint8_t *samples, uint8_t n_samples);
static bool _check_raw_temp(sensor_imu_invensense *invensense, int16_t t2);

void sensor_imu_invensense_start(sensor_imu_backend *backend);
bool sensor_imu_invensense_update(sensor_imu_backend *backend);
void sensor_imu_invensense_destructor(sensor_imu_backend *backend);

/*----------------------------------variable----------------------------------*/
static struct sensor_imu_backend_ops invensense_ops;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void sensor_imu_invensense_ctor(sensor_imu_invensense *invensense, sensor_imu *imu, gp_device_t dev, enum RotationEnum rotation)
{
    // 清空sensor_imu_backend结构体变量，因为sensor_imu_backend结构体有可能是申请的动态内存
    // 防止sensor_imu_backend中的变量初始为非零值。
    rt_memset(invensense, 0, sizeof(sensor_imu_invensense));

    sensor_imu_backend_ctor(&invensense->backend, "invensense");

    invensense_ops = imu_backend_ops;

    invensense_ops.start = sensor_imu_invensense_start;
    invensense_ops.update = sensor_imu_invensense_update;

    invensense->backend.ops = &invensense_ops;

    invensense->_rotation = rotation;
    invensense->_dev = dev;

    invensense->multiplier_accel = INT16_MAX/(26*GRAVITY_MSS);
    invensense->temp_sensitivity = 1.0f/340; // degC/LSB
    invensense->temp_zero = 36.53f;          // degC
    lpf_set_cutoff2(&invensense->_temp_filter, 1, 1.0f/1000);

    lpf_set_cutoff2_vec3f(&invensense->_accum.accel_filter, 188, 1.0f/4000);
}

void sensor_imu_invensense_destructor(sensor_imu_backend *backend)
{
    sensor_imu_invensense *invensense = (sensor_imu_invensense *)backend;

    if (invensense->_fifo_buffer != NULL) {
        rt_free(invensense->_fifo_buffer);
    }

    sensor_imu_backend_destructor(backend);
}

sensor_imu_backend *sensor_imu_invensense_probe(sensor_imu *imu, gp_device_t dev, enum RotationEnum rotation)
{
    if (!dev) {
        return NULL;
    }

    devmgr_set_read_flag(dev, MPU_DIR_READ);

    sensor_imu_invensense *sensor = (sensor_imu_invensense *)rt_malloc(sizeof(sensor_imu_invensense));
    sensor_imu_invensense_ctor(sensor, imu, dev, rotation);

    if (!sensor || !_init(sensor)) {
        sensor_imu_invensense_destructor((sensor_imu_backend *)sensor);
        rt_free(sensor);
        return NULL;
    }

    sensor_imu_backend *backend = &sensor->backend;

    if (sensor->_mpu_type == Invensense_MPU9250) {
        backend->_id = HAL_INS_MPU9250_SPI;
    } else if (sensor->_mpu_type == Invensense_MPU6500) {
        backend->_id = HAL_INS_MPU6500;
    } else {
        backend->_id = HAL_INS_MPU60XX_SPI;
    }

    return (sensor_imu_backend *)sensor;
}

void sensor_imu_invensense_start(sensor_imu_backend *backend)
{
    sensor_imu_invensense *invensense = (sensor_imu_invensense *)backend;

    // initially run the bus at low speed
    devmgr_set_speed(invensense->_dev, DEV_SPEED_LOW);

    // only used for wake-up in accelerometer only low power mode
    _register_write(invensense, MPUREG_PWR_MGMT_2, 0x00, false);

    rt_thread_mdelay(1);

    // always use FIFO
    _fifo_reset(invensense, false);

    // grab the used instances
    enum ImuDevTypes gdev, adev;
    switch (invensense->_mpu_type) {
    case Invensense_MPU9250:
        gdev = DEVTYPE_GYR_MPU9250;
        adev = DEVTYPE_ACC_MPU9250;
        invensense->_enable_offset_checking = true;
        break;
    case Invensense_ICM20602:
        gdev = DEVTYPE_INS_ICM20602;
        adev = DEVTYPE_INS_ICM20602;
        invensense->_enable_offset_checking = true;
        break;
    case Invensense_ICM20601:
        gdev = DEVTYPE_INS_ICM20601;
        adev = DEVTYPE_INS_ICM20601;
        invensense->_enable_offset_checking = true;
        break;
    case Invensense_ICM20608:
        // unfortunately we don't have a separate ID for 20608, and we
        // can't change this without breaking existing calibrations
        gdev = DEVTYPE_GYR_MPU6000;
        adev = DEVTYPE_ACC_MPU6000;
        invensense->_enable_offset_checking = true;
        break;
    case Invensense_ICM20789:
        gdev = DEVTYPE_INS_ICM20789;
        adev = DEVTYPE_INS_ICM20789;
        invensense->_enable_offset_checking = true;
        break;
    case Invensense_ICM20689:
        gdev = DEVTYPE_INS_ICM20689;
        adev = DEVTYPE_INS_ICM20689;
        invensense->_enable_offset_checking = true;
        break;
    case Invensense_MPU6000:
    case Invensense_MPU6500:
    default:
        gdev = DEVTYPE_GYR_MPU6000;
        adev = DEVTYPE_ACC_MPU6000;
        break;
    }

    /*
      setup temperature sensitivity and offset. This varies
      considerably between parts
     */
    switch (invensense->_mpu_type) {
    case Invensense_MPU9250:
        invensense->temp_zero = 21.0f;
        invensense->temp_sensitivity = 1.0f/340;
        break;

    case Invensense_MPU6000:
    case Invensense_MPU6500:
        invensense->temp_zero = 36.53f;
        invensense->temp_sensitivity = 1.0f/340;
        break;

    case Invensense_ICM20608:
    case Invensense_ICM20602:
    case Invensense_ICM20601:
        invensense->temp_zero = 25.0f;
        invensense->temp_sensitivity = 1.0f/326.8f;
        break;

    case Invensense_ICM20789:
        invensense->temp_zero = 25.0f;
        invensense->temp_sensitivity = 0.003f;
        break;
    case Invensense_ICM20689:
        invensense->temp_zero = 25.0f;
        invensense->temp_sensitivity = 0.003f;
        break;
    }

    if (!sensor_imu_register_gyro(&invensense->_gyro_instance, 1000, devmgr_get_bus_id_devtype(invensense->_dev, gdev)) ||
        !sensor_imu_register_accel(&invensense->_accel_instance, 1000, devmgr_get_bus_id_devtype(invensense->_dev, adev))) {
        return;
    }

    // setup ODR and on-sensor filtering
    _set_filter_register(invensense);

    // update backend sample rate
    sensor_imu_backend_set_accel_raw_sample_rate(invensense->_accel_instance, invensense->_accel_backend_rate_hz);
    sensor_imu_backend_set_gyro_raw_sample_rate(invensense->_gyro_instance, invensense->_gyro_backend_rate_hz);

    // indicate what multiplier is appropriate for the sensors'
    // readings to fit them into an int16_t:
    sensor_imu_backend_set_raw_sample_accel_multiplier(invensense->_accel_instance, invensense->multiplier_accel);

    // set sample rate to 1000Hz and apply a software filter
    // In this configuration, the gyro sample rate is 8kHz
    _register_write(invensense, MPUREG_SMPLRT_DIV, 0, true);
    rt_thread_mdelay(1);

    // Gyro scale 2000潞/s
    _register_write(invensense, MPUREG_GYRO_CONFIG, BITS_GYRO_FS_2000DPS, true);
    rt_thread_mdelay(1);

    // read the product ID rev c has 1/2 the sensitivity of rev d
    uint8_t product_id = _register_read(invensense, MPUREG_PRODUCT_ID);

    if (invensense->_mpu_type == Invensense_MPU6000 &&
        ((product_id == MPU6000ES_REV_C4) ||
         (product_id == MPU6000ES_REV_C5) ||
         (product_id == MPU6000_REV_C4)   ||
         (product_id == MPU6000_REV_C5))) {
        // Accel scale 8g (4096 LSB/g)
        // Rev C has different scaling than rev D
        _register_write(invensense, MPUREG_ACCEL_CONFIG,1<<3, true);
        invensense->_accel_scale = GRAVITY_MSS / 4096.0f;
        invensense->_gyro_scale = (radians(1) / 16.4f);
    } else if (invensense->_mpu_type == Invensense_ICM20601) {
        // Accel scale 32g (4096 LSB/g)
        _register_write(invensense, MPUREG_ACCEL_CONFIG,1<<3, true);
        invensense->_accel_scale = GRAVITY_MSS / 4096.0f;
        invensense->_gyro_scale = (radians(1) / 8.2f);
        backend->_clip_limit = 29.5f * GRAVITY_MSS;
    } else {
        // Accel scale 16g (2048 LSB/g)
        _register_write(invensense, MPUREG_ACCEL_CONFIG,3<<3, true);
        invensense->_accel_scale = GRAVITY_MSS / 2048.0f;
        invensense->_gyro_scale = (radians(1) / 16.4f);
    }
    rt_thread_mdelay(1);

    if (invensense->_mpu_type == Invensense_ICM20608 ||
        invensense->_mpu_type == Invensense_ICM20602 ||
        invensense->_mpu_type == Invensense_ICM20601) {
        // this avoids a sensor bug, see description above
        _register_write(invensense, MPUREG_ICM_UNDOC1, MPUREG_ICM_UNDOC1_VALUE, true);
    }
    
    // configure interrupt to fire when new data arrives
    _register_write(invensense, MPUREG_INT_ENABLE, BIT_RAW_RDY_EN, false);
    rt_thread_mdelay(1);

    // clear interrupt on any read, and hold the data ready pin high
    // until we clear the interrupt. We don't do this for the 20789 as
    // that sensor has already setup the appropriate config inside the
    // baro driver.
    if (invensense->_mpu_type != Invensense_ICM20789) {    
        uint8_t v = _register_read(invensense, MPUREG_INT_PIN_CFG) | BIT_INT_RD_CLEAR | BIT_LATCH_INT_EN;
        v &= BIT_BYPASS_EN;
        _register_write(invensense, MPUREG_INT_PIN_CFG, v, false);
    }

    if (invensense->_enable_offset_checking) {
        /*
          there is a bug in at least the ICM-20602 where the
          MPUREG_ACC_OFF_Y_H changes at runtime, which adds an offset
          on the Y accelerometer. To prevent this we read the factory
          cal values of the sensor at startup and write them back as
          checked register values. Then we rely on the register
          checking code to detect the change and fix it
         */
        uint8_t regs[] = { MPUREG_ACC_OFF_X_H, MPUREG_ACC_OFF_X_L,
                           MPUREG_ACC_OFF_Y_H, MPUREG_ACC_OFF_Y_L,
                           MPUREG_ACC_OFF_Z_H, MPUREG_ACC_OFF_Z_L };
        for (uint8_t i=0; i<ARRAY_SIZE(regs); i++) {
            _register_write(invensense, regs[i], _register_read(invensense, regs[i]), true);
        }
    }


    if (invensense->_mpu_type == Invensense_ICM20602) {
        /*
          save y offset high separately for ICM20602 to quickly
          recover from a change in this register. The ICM20602 has a
          bug where every few hours it can change the factory Y offset
          register, which leads to a sudden change in Y accelerometer
          output
         */
        invensense->_saved_y_ofs_high = _register_read(invensense, MPUREG_ACC_OFF_Y_H);
    }

    // now that we have initialised, we set the bus speed to high
    devmgr_set_speed(invensense->_dev, DEV_SPEED_HIGH);

    // setup sensor rotations from probe()
    sensor_imu_backend_set_gyro_orientation(invensense->_gyro_instance, invensense->_rotation);
    sensor_imu_backend_set_accel_orientation(invensense->_accel_instance, invensense->_rotation);

    // setup scale factors for fifo data after downsampling
    invensense->_fifo_accel_scale = invensense->_accel_scale / invensense->_accel_fifo_downsample_rate;
    invensense->_fifo_gyro_scale = invensense->_gyro_scale / invensense->_gyro_fifo_downsample_rate;
    
    // allocate fifo buffer
    invensense->_fifo_buffer = (uint8_t *)rt_malloc(MPU_FIFO_BUFFER_LEN * MPU_SAMPLE_SIZE);
    if (invensense->_fifo_buffer == NULL) {
        console_panic("Invensense: Unable to allocate FIFO buffer");
    }

    // start the timer process to read samples, using the fastest rate avilable
    //_dev->register_periodic_callback(1000000UL / invensense->_gyro_backend_rate_hz, FUNCTOR_BIND_MEMBER(&AP_InertialSensor_Invensense::_poll_data, void));
    devmgr_register_periodic_callback(invensense->_dev, 1000000UL / invensense->_gyro_backend_rate_hz, _poll_data, invensense);
}

/*
  publish any pending data
 */
bool sensor_imu_invensense_update(sensor_imu_backend *backend)
{
    sensor_imu_invensense *invensense = (sensor_imu_invensense *)backend;

    sensor_imu_backend_update_accel(backend, invensense->_accel_instance);
    sensor_imu_backend_update_gyro(backend, invensense->_gyro_instance);

    sensor_imu_backend_publish_temperature(backend, invensense->_accel_instance, invensense->_temp_filtered);

    return true;
}

static bool _init(sensor_imu_invensense *invensense)
{
    bool success = _hardware_init(invensense);

    return success;
}

static bool _hardware_init(sensor_imu_invensense *invensense)
{
    sensor_imu_backend *imu_backend = &invensense->backend;

    rt_mutex_take(imu_backend->_mutex, RT_WAITING_FOREVER);

    // setup for register checking. We check much less often on I2C
    // where the cost of the checks is higher
    devmgr_setup_checked_registers(invensense->_dev, 13, devmgr_get_bus_type(invensense->_dev) == BUS_TYPE_I2C?200:20);

    // initially run the bus at low speed
    devmgr_set_speed(invensense->_dev, DEV_SPEED_LOW);

    if (!_check_whoami(invensense)) {
        rt_mutex_release(imu_backend->_mutex);
        return false;
    }

    // Chip reset
    uint8_t tries;
    for (tries = 0; tries < 5; tries++) {
        invensense->_last_stat_user_ctrl = _register_read(invensense, MPUREG_USER_CTRL);

        /* First disable the master I2C to avoid hanging the slaves on the
         * aulixiliar I2C bus - it will be enabled again if the AuxiliaryBus
         * is used */
        if (invensense->_last_stat_user_ctrl & BIT_USER_CTRL_I2C_MST_EN) {
            invensense->_last_stat_user_ctrl &= ~BIT_USER_CTRL_I2C_MST_EN;
            _register_write(invensense, MPUREG_USER_CTRL, invensense->_last_stat_user_ctrl, false);
            rt_thread_mdelay(10);
        }

        /* reset device */
        _register_write(invensense, MPUREG_PWR_MGMT_1, BIT_PWR_MGMT_1_DEVICE_RESET, false);
        rt_thread_mdelay(100);

        /* bus-dependent initialization */
        if (devmgr_get_bus_type(invensense->_dev) == BUS_TYPE_SPI) {
            /* Disable I2C bus if SPI selected (Recommended in Datasheet to be
             * done just after the device is reset) */
            invensense->_last_stat_user_ctrl |= BIT_USER_CTRL_I2C_IF_DIS;
            _register_write(invensense, MPUREG_USER_CTRL, invensense->_last_stat_user_ctrl, false);
        }

        /* bus-dependent initialization */
        if ((devmgr_get_bus_type(invensense->_dev) == BUS_TYPE_I2C)
            && (invensense->_mpu_type == Invensense_MPU9250 || invensense->_mpu_type == Invensense_ICM20789)) {
            /* Enable I2C bypass to access internal device */
            _register_write(invensense, MPUREG_INT_PIN_CFG, BIT_BYPASS_EN, false);
        }


        // Wake up device and select GyroZ clock. Note that the
        // Invensense starts up in sleep mode, and it can take some time
        // for it to come out of sleep
        _register_write(invensense, MPUREG_PWR_MGMT_1, BIT_PWR_MGMT_1_CLK_ZGYRO, false);
        rt_thread_mdelay(5);

        // check it has woken up
        if (_register_read(invensense, MPUREG_PWR_MGMT_1) == BIT_PWR_MGMT_1_CLK_ZGYRO) {
            break;
        }

        rt_thread_mdelay(10);
        if (_data_ready(invensense)) {
            break;
        }
    }

    devmgr_set_speed(invensense->_dev, DEV_SPEED_HIGH);

    if (tries == 5) {
        console_printf("Failed to boot Invensense 5 times\n");
        rt_mutex_release(imu_backend->_mutex);
        return false;
    }

    if (invensense->_mpu_type == Invensense_ICM20608 ||
        invensense->_mpu_type == Invensense_ICM20602 ||
        invensense->_mpu_type == Invensense_ICM20601) {
        // this avoids a sensor bug, see description above
        _register_write(invensense, MPUREG_ICM_UNDOC1, MPUREG_ICM_UNDOC1_VALUE, true);
    }

    rt_mutex_release(imu_backend->_mutex);

    return true;
}

static bool _block_read(sensor_imu_invensense *invensense, uint8_t reg, uint8_t *buf, uint32_t size)
{
    return devmgr_read_registers(invensense->_dev, reg, buf, size);
}

static uint8_t _register_read(sensor_imu_invensense *invensense, uint8_t reg)
{
    uint8_t val = 0;
    devmgr_read_registers(invensense->_dev, reg, &val, 1);
    return val;
}

static void _register_write(sensor_imu_invensense *invensense, uint8_t reg, uint8_t val, bool checked)
{
    devmgr_write_register(invensense->_dev, reg, val, checked);
}

/*
  check whoami for sensor type
 */
static bool _check_whoami(sensor_imu_invensense *invensense)
{
    uint8_t whoami = _register_read(invensense, MPUREG_WHOAMI);
    switch (whoami) {
    case MPU_WHOAMI_6000:
        invensense->_mpu_type = Invensense_MPU6000;
        return true;
    case MPU_WHOAMI_6500:
        invensense->_mpu_type = Invensense_MPU6500;
        return true;
    case MPU_WHOAMI_MPU9250:
    case MPU_WHOAMI_MPU9255:
        invensense->_mpu_type = Invensense_MPU9250;
        return true;
    case MPU_WHOAMI_20608:
        invensense->_mpu_type = Invensense_ICM20608;
        return true;
    case MPU_WHOAMI_20602:
        invensense->_mpu_type = Invensense_ICM20602;
        return true;
    case MPU_WHOAMI_20601:
        invensense->_mpu_type = Invensense_ICM20601;
        return true;
    case MPU_WHOAMI_ICM20789:
    case MPU_WHOAMI_ICM20789_R1:
        invensense->_mpu_type = Invensense_ICM20789;
        return true;
    case MPU_WHOAMI_ICM20689:
        invensense->_mpu_type = Invensense_ICM20689;
        return true;
    }
    // not a value WHOAMI result
    return false;
}

/*
 * Return true if the Invensense has new data available for reading.
 *
 * We use the data ready pin if it is available.  Otherwise, read the
 * status register.
 */
static bool _data_ready(sensor_imu_invensense *invensense)
{
    uint8_t status = _register_read(invensense, MPUREG_INT_STATUS);
    return (status & BIT_RAW_RDY_INT) != 0;
}

static void _fifo_reset(sensor_imu_invensense *invensense, bool log_error)
{
    uint32_t now = time_millis();
    if (log_error &&
        now - invensense->last_reset_ms < 10000) {
        invensense->reset_count++;
        if (invensense->reset_count == 10) {
            // 10 resets, each happening within 10s, triggers an internal error
            //INTERNAL_ERROR(AP_InternalError::error_t::imu_reset);
            invensense->reset_count = 0;
        }
    } else if (log_error &&
        now - invensense->last_reset_ms > 10000) {
        //if last reset was more than 10s ago consider this the first reset
        invensense->reset_count = 1;
    }
    invensense->last_reset_ms = now;

    uint8_t user_ctrl = invensense->_last_stat_user_ctrl;
    user_ctrl &= ~(BIT_USER_CTRL_FIFO_RESET | BIT_USER_CTRL_FIFO_EN);

    devmgr_set_speed(invensense->_dev, DEV_SPEED_LOW);

    _register_write(invensense, MPUREG_FIFO_EN, 0, false);
    _register_write(invensense, MPUREG_USER_CTRL, user_ctrl, false);
    _register_write(invensense, MPUREG_USER_CTRL, user_ctrl | BIT_USER_CTRL_FIFO_RESET, false);
    _register_write(invensense, MPUREG_USER_CTRL, user_ctrl | BIT_USER_CTRL_FIFO_EN, false);
    _register_write(invensense, MPUREG_FIFO_EN, BIT_XG_FIFO_EN | BIT_YG_FIFO_EN |
                    BIT_ZG_FIFO_EN | BIT_ACCEL_FIFO_EN | BIT_TEMP_FIFO_EN, true);

    time_delay_us(1);
    devmgr_set_speed(invensense->_dev, DEV_SPEED_HIGH);
    invensense->_last_stat_user_ctrl = user_ctrl | BIT_USER_CTRL_FIFO_EN;

    sensor_imu_backend_notify_accel_fifo_reset(invensense->_accel_instance);
    sensor_imu_backend_notify_gyro_fifo_reset(invensense->_gyro_instance);
}

/*
  set the DLPF filter frequency. Assumes caller has taken semaphore
 */
static void _set_filter_register(sensor_imu_invensense *invensense)
{
    sensor_imu_backend *backend = &invensense->backend;

    uint8_t config;

#if INVENSENSE_EXT_SYNC_ENABLE
    // add in EXT_SYNC bit if enabled
    config = (MPUREG_CONFIG_EXT_SYNC_AZ << MPUREG_CONFIG_EXT_SYNC_SHIFT);
#else
    config = 0;
#endif

    // assume 1kHz sampling to start
    invensense->_gyro_fifo_downsample_rate = invensense->_accel_fifo_downsample_rate = 1;
    invensense->_gyro_to_accel_sample_ratio = 2;
    invensense->_gyro_backend_rate_hz = invensense->_accel_backend_rate_hz =  1000;
    
    if (sensor_imu_backend_enable_fast_sampling(invensense->_accel_instance)) {
        invensense->_fast_sampling = (devmgr_get_bus_type(invensense->_dev) == BUS_TYPE_SPI);
        if (invensense->_fast_sampling) {
            // constrain the gyro rate to be at least the loop rate
            uint8_t loop_limit = 1;
            if (sensor_imu_backend_get_loop_rate_hz(backend) > 1000) {
                loop_limit = 2;
            }
            if (sensor_imu_backend_get_loop_rate_hz(backend) > 2000) {
                loop_limit = 4;
            }
            // constrain the gyro rate to be a 2^N multiple
            uint8_t fast_sampling_rate = math_constrain_int16(sensor_imu_backend_get_fast_sampling_rate(backend), loop_limit, 8);

            // calculate rate we will be giving gyro samples to the backend
            invensense->_gyro_fifo_downsample_rate = 8 / fast_sampling_rate;
            invensense->_gyro_backend_rate_hz *= fast_sampling_rate;

            // calculate rate we will be giving accel samples to the backend
            if (invensense->_mpu_type >= Invensense_MPU9250) {
                invensense->_accel_fifo_downsample_rate = MAX(4 / fast_sampling_rate, 1);
                invensense->_accel_backend_rate_hz *= MIN(fast_sampling_rate, 4);
            } else {
                invensense->_gyro_to_accel_sample_ratio = 8;
                invensense->_accel_fifo_downsample_rate = 1;
                lpf_set_cutoff2_vec3f(&invensense->_accum.accel_filter, 188, 1.0f/1000);
            }

            // for logging purposes set the oversamping rate
            sensor_imu_backend_set_accel_oversampling(backend, invensense->_accel_instance, invensense->_accel_fifo_downsample_rate);
            sensor_imu_backend_set_gyro_oversampling(backend, invensense->_gyro_instance, invensense->_gyro_fifo_downsample_rate);

            sensor_imu_backend_set_accel_sensor_rate_sampling_enabled(backend, invensense->_accel_instance, true);
            sensor_imu_backend_set_gyro_sensor_rate_sampling_enabled(backend, invensense->_gyro_instance, true);

            /* set divider for internal sample rate to 0x1F when fast
             sampling enabled. This reduces the impact of the slave
             sensor on the sample rate. It ends up with around 75Hz
             slave rate, and reduces the impact on the gyro and accel
             sample rate, ending up with around 7760Hz gyro rate and
             3880Hz accel rate
             */
            _register_write(invensense, MPUREG_I2C_SLV4_CTRL, 0x1F, false);
        }
    }
    
    if (invensense->_fast_sampling) {
        // this gives us 8kHz sampling on gyros and 4kHz on accels
        config |= BITS_DLPF_CFG_256HZ_NOLPF2;
    } else {
        // limit to 1kHz if not on SPI
        config |= BITS_DLPF_CFG_188HZ;
    }

    config |= MPUREG_CONFIG_FIFO_MODE_STOP;
    _register_write(invensense, MPUREG_CONFIG, config, true);

    if (invensense->_mpu_type != Invensense_MPU6000) {
        if (invensense->_fast_sampling) {
            // setup for 4kHz accels
            _register_write(invensense, ICMREG_ACCEL_CONFIG2, ICM_ACC_FCHOICE_B, true);
        } else {
            uint8_t fifo_size = (invensense->_mpu_type == Invensense_ICM20789 || invensense->_mpu_type == Invensense_ICM20689) ? 1:0;
            _register_write(invensense, ICMREG_ACCEL_CONFIG2, ICM_ACC_DLPF_CFG_218HZ | (fifo_size<<6), true);
        }
    }
}

/*
 * Timer process to poll for new data from the Invensense. Called from bus thread with semaphore held
 */
static void _poll_data(void *parameter)
{
    sensor_imu_invensense *invensense = (sensor_imu_invensense *)parameter;

    _read_fifo(invensense);

#if INVENSENSE_DEBUG_REG_CHANGE
    _check_register_change(invensense);
#endif // INVENSENSE_DEBUG_REG_CHANGE
}

#if INVENSENSE_DEBUG_REG_CHANGE
/*
  catch unexpected register changes on an IMU. This was used to
  find the bug in the ICM-20602 that causes the Y accel offset
  trim register to change value in flight
*/
static void _check_register_change(sensor_imu_invensense *invensense)
{
    if (invensense->_mpu_type != Invensense_ICM20602) {
        return;
    }
    static uint16_t counter;
    if (++counter < 100) {
        return;
    }
    counter = 0;
    static bool reg_init;
    static uint8_t reg_value[0x80];
    static uint8_t next_reg;
    if (!reg_init) {
        reg_init = true;
        for (uint8_t i=0; i<ARRAY_SIZE(reg_value); i++) {
            reg_value[i] = _register_read(invensense, i);
        }
    }
    bool skip = false;
    if ((next_reg >= MPUREG_ACCEL_XOUT_H && next_reg <= MPUREG_GYRO_ZOUT_L) ||
        next_reg == MPUREG_MEM_R_W || next_reg == MPUREG_FIFO_R_W ||
        next_reg == MPUREG_INT_STATUS ||
        next_reg == MPUREG_FIFO_COUNTH || next_reg == MPUREG_FIFO_COUNTL) {
        skip = true;
    }
    if (!skip) {
        uint8_t v = _register_read(invensense, next_reg);
        if (v != reg_value[next_reg]) {
            //mavproxy_send_statustext(MAV_SEVERITY_WARNING, "change[%02x] 0x%02x -> 0x%02x",
            //              next_reg, reg_value[next_reg], v);
            reg_value[next_reg] = v;
        }
    }
    next_reg = (next_reg+1) % ARRAY_SIZE(reg_value);
}
#endif // INVENSENSE_DEBUG_REG_CHANGE

static void _read_fifo(sensor_imu_invensense *invensense)
{
    uint8_t n_samples;
    uint16_t bytes_read;
    uint8_t *rx = invensense->_fifo_buffer;
    bool need_reset = false;

    if (!_block_read(invensense, MPUREG_FIFO_COUNTH, rx, 2)) {
        goto check_registers;
    }

    bytes_read = uint16_val(rx, 0);
    n_samples = bytes_read / MPU_SAMPLE_SIZE;

    if (n_samples == 0) {
        /* Not enough data in FIFO */
        goto check_registers;
    }

    /*
      testing has shown that if we have more than 32 samples in the
      FIFO then some of those samples will be corrupt. It always is
      the ones at the end of the FIFO, so clear those with a reset
      once we've read the first 24. Reading 24 gives us the normal
      number of samples for fast sampling at 400Hz

      On I2C with the much lower clock rates we need a lower threshold
      or we may never catch up
     */
    if (devmgr_get_bus_type(invensense->_dev) == BUS_TYPE_I2C) {
        if (n_samples > 4) {
            need_reset = true;
            n_samples = 4;
        }
    } else {
        if (n_samples > 32) {
            need_reset = true;
            n_samples = 24;
        }
    }
    
    while (n_samples > 0) {
        uint8_t n = MIN(n_samples, MPU_FIFO_BUFFER_LEN);

        if (!_block_read(invensense, MPUREG_FIFO_R_W, rx, n * MPU_SAMPLE_SIZE)) {
                goto check_registers;
        }

        if (invensense->_fast_sampling) {
            if (!_accumulate_sensor_rate_sampling(invensense, rx, n)) {
            //    //if (!hal.scheduler->in_expected_delay()) {
            //    //    debug("IMU[%u] stop at %u of %u", _accel_instance, n_samples, bytes_read/MPU_SAMPLE_SIZE);
            //    //}
                break;
            }
        } else {
            if (!_accumulate(invensense, rx, n)) {
                break;
            }
        }
        n_samples -= n;
    }

    if (need_reset) {
        //debug("fifo reset n_samples %u", bytes_read/MPU_SAMPLE_SIZE);
        _fifo_reset(invensense, false);
    }
    
check_registers:
    // check next register value for correctness

    if (invensense->_mpu_type == Invensense_ICM20602) {
        const uint8_t y_ofs = _register_read(invensense, MPUREG_ACC_OFF_Y_H);
        if (y_ofs != invensense->_saved_y_ofs_high) {
            /*
              we check and restore the ICM20602 Y offset high register
              on every update. We don't mark the IMU unhealthy when we
              do this. This is a workaround for a bug in the ICM-20602
              where this register can change in flight. We log these
              events to help with log analysis, but don't shout at the
              GCS to prevent possible flood
            */
            //AP::logger().Write_MessageF("ICM20602 yofs fix: %x %x", y_ofs, invensense->_saved_y_ofs_high);
            _register_write(invensense, MPUREG_ACC_OFF_Y_H, invensense->_saved_y_ofs_high, false);
        }
    }

    devmgr_set_speed(invensense->_dev, DEV_SPEED_LOW);

    struct checkreg reg;
    if (!devmgr_check_next_register2(invensense->_dev, &reg)) {
        //log_register_change(_dev->get_bus_id(), reg);
        sensor_imu_backend_inc_gyro_error_count(invensense->_gyro_instance);
        sensor_imu_backend_inc_accel_error_count(invensense->_accel_instance);
    }

    devmgr_set_speed(invensense->_dev, DEV_SPEED_HIGH);
}

static bool _accumulate(sensor_imu_invensense *invensense, uint8_t *samples, uint8_t n_samples)
{
    for (uint8_t i = 0; i < n_samples; i++) {
        const uint8_t *data = samples + MPU_SAMPLE_SIZE * i;
        bool fsync_set = false;

#if INVENSENSE_EXT_SYNC_ENABLE
        fsync_set = (int16_val(data, 2) & 1U) != 0;
#endif
        
        Vector3f_t accel = {int16_val(data, 1),
                         int16_val(data, 0),
                         -int16_val(data, 2)};
        vec3_mult(&accel, &accel, invensense->_accel_scale);

        int16_t t2 = int16_val(data, 3);
        if (!_check_raw_temp(invensense, t2)) {
            //if (!hal.scheduler->in_expected_delay()) {
            //    debug("temp reset IMU[%u] %d %d", _accel_instance, _raw_temp, t2);
            //}
            _fifo_reset(invensense, true);
            return false;
        }
        float temp = t2 * invensense->temp_sensitivity + invensense->temp_zero;
        
        Vector3f_t gyro = {int16_val(data, 5),
                        int16_val(data, 4),
                        -int16_val(data, 6)};
        //gyro *= _gyro_scale;
        vec3_mult(&gyro, &gyro, invensense->_gyro_scale);

        sensor_imu_backend_rotate_and_correct_accel(&invensense->backend, invensense->_accel_instance, &accel);
        sensor_imu_backend_rotate_and_correct_gyro(&invensense->backend, invensense->_gyro_instance, &gyro);

        sensor_imu_backend_notify_new_accel_raw_sample(&invensense->backend, invensense->_accel_instance, &accel, 0, fsync_set);
        sensor_imu_backend_notify_new_gyro_raw_sample(&invensense->backend, invensense->_gyro_instance, &gyro, 0);

        invensense->_temp_filtered = lpf_apply1(&invensense->_temp_filter, temp);
    }
    return true;
}

/*
  when doing fast sampling the sensor gives us 8k samples/second. Every 2nd accel sample is a duplicate.

  To filter this we first apply a 1p low pass filter at 188Hz, then we
  average over 8 samples to bring the data rate down to 1kHz. This
  gives very good aliasing rejection at frequencies well above what
  can be handled with 1kHz sample rates.
 */
static bool _accumulate_sensor_rate_sampling(sensor_imu_invensense *invensense, uint8_t *samples, uint8_t n_samples)
{
    int32_t tsum = 0;
    const int32_t unscaled_clip_limit = invensense->backend._clip_limit / invensense->_accel_scale;
    bool clipped = false;
    bool ret = true;
    
    for (uint8_t i = 0; i < n_samples; i++) {
        const uint8_t *data = samples + MPU_SAMPLE_SIZE * i;

        // use temperatue to detect FIFO corruption
        int16_t t2 = int16_val(data, 3);
        if (!_check_raw_temp(invensense, t2)) {
            //if (!hal.scheduler->in_expected_delay()) {
            //    debug("temp reset IMU[%u] %d %d", _accel_instance, _raw_temp, t2);
            //}
            _fifo_reset(invensense, true);
            ret = false;
            break;
        }
        tsum += t2;

        if (invensense->_accum.gyro_count % invensense->_gyro_to_accel_sample_ratio == 0) {
            // accel data is at 4kHz or 1kHz
            Vector3f_t a = {int16_val(data, 1), int16_val(data, 0), -int16_val(data, 2)};
            if (fabsf(a.x) > unscaled_clip_limit ||
                fabsf(a.y) > unscaled_clip_limit ||
                fabsf(a.z) > unscaled_clip_limit) {
                clipped = true;
            }

            Vector3f_t a_filter = lpf_apply1_vec3f(&invensense->_accum.accel_filter, &a);
            vec3_add(&invensense->_accum.accel, &invensense->_accum.accel, &a_filter);

            Vector3f_t a2 = vec3_mult2(&a, invensense->_accel_scale);
            sensor_imu_backend_notify_new_accel_sensor_rate_sample(&invensense->backend, invensense->_accel_instance, &a2);

            invensense->_accum.accel_count++;

            if (invensense->_accum.accel_count % invensense->_accel_fifo_downsample_rate == 0) {
                vec3_mult(&invensense->_accum.accel, &invensense->_accum.accel, invensense->_fifo_accel_scale);
                sensor_imu_backend_rotate_and_correct_accel(&invensense->backend, invensense->_accel_instance, &invensense->_accum.accel);
                sensor_imu_backend_notify_new_accel_raw_sample(&invensense->backend, invensense->_accel_instance, &invensense->_accum.accel, 0, false);
                vec3_zero(&invensense->_accum.accel);
                invensense->_accum.accel_count = 0;
                // we assume that the gyro rate is always >= and a multiple of the accel rate
                invensense->_accum.gyro_count = 0;
            }
        }

        invensense->_accum.gyro_count++;

        Vector3f_t g = {int16_val(data, 5),
                   int16_val(data, 4),
                   -int16_val(data, 6)};

        Vector3f_t g2 = vec3_mult2(&g, invensense->_gyro_scale);
        sensor_imu_backend_notify_new_gyro_sensor_rate_sample(&invensense->backend, invensense->_gyro_instance, &g2);

        vec3_add(&invensense->_accum.gyro, &invensense->_accum.gyro, &g);

        if (invensense->_accum.gyro_count % invensense->_gyro_fifo_downsample_rate == 0) {
            vec3_mult(&invensense->_accum.gyro, &invensense->_accum.gyro, invensense->_fifo_gyro_scale);
            sensor_imu_backend_rotate_and_correct_gyro(&invensense->backend, invensense->_gyro_instance, &invensense->_accum.gyro);
            sensor_imu_backend_notify_new_gyro_raw_sample(&invensense->backend, invensense->_gyro_instance, &invensense->_accum.gyro, 0);
            vec3_zero(&invensense->_accum.gyro);
        }
    }

    if (clipped) {
        sensor_imu_backend_increment_clip_count(invensense->_accel_instance);
    }

    if (ret) {
        float temp = ((float)(tsum)/(float)n_samples)*invensense->temp_sensitivity + invensense->temp_zero;
        invensense->_temp_filtered = lpf_apply1(&invensense->_temp_filter, temp);
    }
    
    return ret;
}

/*
  fetch temperature in order to detect FIFO sync errors
*/
static bool _check_raw_temp(sensor_imu_invensense *invensense, int16_t t2)
{
    if (abs(t2 - invensense->_raw_temp) < 400) {
        // cached copy OK
        return true;
    }
    uint8_t trx[2];
    if (_block_read(invensense, MPUREG_TEMP_OUT_H, trx, 2)) {
        invensense->_raw_temp = int16_val(trx, 0);
    }
    return (abs(t2 - invensense->_raw_temp) < 800);
}

/*------------------------------------test------------------------------------*/


