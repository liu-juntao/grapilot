
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_serial.h
  * @author     baiyang
  * @date       2021-7-15
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <rtthread.h>
/*-----------------------------------macro------------------------------------*/
#define GP_SERIAL_COUNT_MAX    9

#define GP_SERIAL_GPS_BUFSIZE_TX    1024
#define GP_SERIAL_GPS_BUFSIZE_RX    1024

#define GP_SERIAL_MAV_BUFSIZE_TX    1024
#define GP_SERIAL_MAV_BUFSIZE_RX    1024

#define GP_SERIAL_DEFAULT_BUFSIZE_TX    512
#define GP_SERIAL_DEFAULT_BUFSIZE_RX    512
/*----------------------------------typedef-----------------------------------*/
/** @ 
  * @brief  应用对应的功能号
  */
enum SerialProtocol {
    SerialProtocol_None = -1,
    SerialProtocol_Console = 0,                  //
    SerialProtocol_MAVLink = 1,
    SerialProtocol_MAVLink2 = 2,                 // do not use - use MAVLink and provide instance of 1
    SerialProtocol_FrSky_D = 3,                  // FrSky D protocol (D-receivers)
    SerialProtocol_FrSky_SPort = 4,              // FrSky SPort protocol (X-receivers)
    SerialProtocol_GPS = 5,
    SerialProtocol_GPS2 = 6,                     // do not use - use GPS and provide instance of 1
    SerialProtocol_AlexMos = 7,
    SerialProtocol_SToRM32 = 8,
    SerialProtocol_Rangefinder = 9,
    SerialProtocol_FrSky_SPort_Passthrough = 10, // FrSky SPort Passthrough (OpenTX) protocol (X-receivers)
    SerialProtocol_Lidar360 = 11,                // Lightware SF40C, TeraRanger Tower or RPLidarA2
    SerialProtocol_Aerotenna_uLanding      = 12, // Ulanding support - deprecated, users should use Rangefinder
    SerialProtocol_Beacon = 13,
    SerialProtocol_Volz = 14,                    // Volz servo protocol
    SerialProtocol_Sbus1 = 15,
    SerialProtocol_ESCTelemetry = 16,
    SerialProtocol_Devo_Telem = 17,
    SerialProtocol_OpticalFlow = 18,
    SerialProtocol_Robotis = 19,
    SerialProtocol_NMEAOutput = 20,
    SerialProtocol_WindVane = 21,
    SerialProtocol_SLCAN = 22,
    SerialProtocol_RCIN = 23,
    SerialProtocol_EFI_MS = 24,                   // MegaSquirt EFI serial protocol
    SerialProtocol_LTM_Telem = 25,
    SerialProtocol_RunCam = 26,
    SerialProtocol_Hott = 27,
    SerialProtocol_Scripting = 28,
    SerialProtocol_CRSF = 29,
    SerialProtocol_Generator = 30,
    SerialProtocol_Winch = 31,
    SerialProtocol_MSP = 32,
    SerialProtocol_DJI_FPV = 33,
    SerialProtocol_AirSpeed = 34,
    SerialProtocol_ADSB = 35,
    SerialProtocol_AHRS = 36,
    SerialProtocol_SmartAudio = 37,
    SerialProtocol_Torqeedo = 39,
    SerialProtocol_NumProtocols                    // must be the last value
};

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void gp_serial_device_init(void);
uint32_t SerialManager_find_baudrate(enum SerialProtocol protocol, uint8_t instance);
int32_t SerialManager_set_baudrate(rt_device_t pSerial, uint32_t baudrate);
int16_t SerialManager_rx_available(rt_device_t pSerial);
int16_t SerialManager_tx_space(rt_device_t pSerial);
uint32_t SerialManager_write(rt_device_t pSerial, const uint8_t *buffer, uint32_t size);
uint32_t SerialManager_write_str(rt_device_t pSerial, const char *str);
uint32_t SerialManager_write_byte(rt_device_t pSerial, uint8_t byte);
uint32_t SerialManager_read(rt_device_t pSerial, uint8_t *buffer, uint32_t size);
int16_t SerialManager_read_byte(rt_device_t pSerial);
uint64_t SerialManager_receive_time_constraint_us(rt_device_t pSerial, uint16_t nbytes);
const rt_device_t SerialManager_find_protocol_instance(enum SerialProtocol protocol, uint8_t instance);
int8_t SerialManager_find_portnum(enum SerialProtocol protocol, uint8_t instance);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



