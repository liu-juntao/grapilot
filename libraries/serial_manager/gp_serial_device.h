
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_serial_device.h
  * @author     baiyang
  * @date       2021-7-15
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <rthw.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <stdint.h>
#include <stdbool.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct gp_serial_device* gp_serial_device_t;

/** @ 
  * @brief  操作函数指针结构体
  */
struct gp_serial_ops
{
    rt_err_t (*gp_serial_init)(gp_serial_device_t serial);
    rt_err_t (*gp_serial_open)(gp_serial_device_t serial);
    rt_err_t (*gp_serial_close)(gp_serial_device_t serial);
    rt_size_t (*gp_serial_read)(gp_serial_device_t serial, rt_off_t pos, void* buffer, rt_size_t size);
    rt_size_t (*gp_serial_write)(gp_serial_device_t serial, rt_off_t pos, const void* buffer, rt_size_t size);
    rt_err_t (*gp_serial_control)(gp_serial_device_t serial, int cmd, void* arg);
};

/** @ 
  * @brief  serial结构体
  */
struct gp_serial_device
{
    struct rt_device parent;        //设备
    const char *pname;              //设备名
    rt_device_t puart;              //对应串口
    const struct gp_serial_ops *ops;//操作
    int8_t protocol;                //功能号
    uint32_t baud;                  //波特率
    rt_uint16_t oflag;              //设备使用模式标志位
    struct rt_ringbuffer *rb_rx;    //接受缓冲区
    struct rt_ringbuffer *rb_tx;    //发送缓冲区

    uint64_t receive_timestamp[2];
    uint8_t receive_timestamp_idx;

    bool is_usb;
};

/*----------------------------------variable----------------------------------*/
extern const struct gp_serial_ops _serial_ops;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
rt_err_t gp_serial_device_register(gp_serial_device_t serial, const char* name, rt_uint32_t flag);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



