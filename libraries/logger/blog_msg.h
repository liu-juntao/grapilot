
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       blog_msg.h
  * @author     baiyang
  * @date       2021-8-6
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
/*-----------------------------------macro------------------------------------*/
#define BLOG_VERSION 1

// 帧头和帧尾
#define BLOG_BEGIN_MSG1 0x92
#define BLOG_BEGIN_MSG2 0x05
#define BLOG_END_MSG    0x26

// 相关描述符长度限制
#define BLOG_MAX_NAME_LEN     20
#define BLOG_DESCRIPTION_SIZE 128
#define BLOG_MODEL_INFO_SIZE  256

// buffer长度和Sector大小定义
#define BLOG_BUFFER_SIZE         12 * 1024U
#define BLOG_SECTOR_SIZE         4096 /* larger block can increase wrte bandwidth */
#define BLOG_MAX_SECTOR_TO_WRITE (BLOG_BUFFER_SIZE/BLOG_SECTOR_SIZE)

#define LOG_PACKET_HEADER        uint8_t head1, head2, msgid
#define LOG_PACKET_END           uint8_t end_msg

// 头尾宏
#define LOG_PACKET_HEADER_INIT(id) \
    .head1 = BLOG_BEGIN_MSG1,      \
    .head2 = BLOG_BEGIN_MSG2,      \
    .msgid = id

#define LOG_PACKET_END_INIT \
    .end_msg = BLOG_END_MSG
/*----------------------------------typedef-----------------------------------*/
/* BLog Msg ID */
enum {
    // should start from 1
    BLOG_IMU_ID = 1,
    BLOG_MAG_ID,
    BLOG_BARO_ID,
    BLOG_GPS_ID,
    BLOG_ATT_ID,
    BLOG_ATC_ID,
    BLOG_POS_XY_ID,
    BLOG_POS_Z_ID,
    BLOG_PSC_XY_ID,
    BLOG_PSC_Z_ID,
    BLOG_RC_IN_ID,
    BLOG_SRV_OUT_ID,
    BLOG_VER_CTRL_ID,
    BLOG_MODE_ID,
    BLOG_EVENT_ID,
    BLOG_PID_ID,
    BLOG_MISSION_CMD_ID,
    BLOG_LOITER_ID,
};

enum {
    BLOG_CB_START,
    BLOG_CB_STOP,
};

// 数据类型定义（解析用）
enum {
    BLOG_INT8 = 0,
    BLOG_UINT8,
    BLOG_INT16,
    BLOG_UINT16,
    BLOG_INT32,
    BLOG_UINT32,
    BLOG_FLOAT,
    BLOG_DOUBLE,
    BLOG_BOOLEAN,
};

enum {
    BLOG_STATUS_IDLE = 0,
    BLOG_STATUS_WRITE_HEAD,
    BLOG_STATUS_LOGGING,
    BLOG_STATUS_STOPPING,
};

typedef enum {
    ARM = 0,
} LogEvent;

typedef enum {
    //飞机是否解锁事件
    ARMED = 1,
    DISARMED = 0,
} LogEventCode;

#pragma pack(1)
typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       gyr_x;
    float       gyr_y;
    float       gyr_z;
    float       acc_x;
    float       acc_y;
    float       acc_z;
    float       fil_gyr_x;
    float       fil_gyr_y;
    float       fil_gyr_z;
    float       fil_acc_x;
    float       fil_acc_y;
    float       fil_acc_z;
    LOG_PACKET_END;
} blog_imu;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       mag_x_mea;
    float       mag_y_mea;
    float       mag_z_mea;
    float       mag_x_cor;
    float       mag_y_cor;
    float       mag_z_cor;
    LOG_PACKET_END;
} blog_mag;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    uint32_t    timegpsms;
    uint8_t     fixType;
    uint8_t     numSV;
    int32_t     lon;
    int32_t     lat;
    int32_t     height;
    float       hAcc;
    float       vAcc;
    float       sAcc;
    float       velN;
    float       velE;
    float       velD;
    float       speed;
    float       headMot;
    float       headAcc;
    LOG_PACKET_END;
} blog_gps;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       roll;
    float       pitch;
    float       yaw;
    float       RollRate;
    float       PitchRate;
    float       YawRate;
    LOG_PACKET_END;
} blog_att;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       DesRoll;
    float       DesPitch;
    float       DesYaw;
    float       DesRollRate;
    float       DesPitchRate;
    float       DesYawRate;
    LOG_PACKET_END;
} blog_atc;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       Px;
    float       Py;
    float       Vx;
    float       Vy;
    float       Ax;
    float       Ay;
    LOG_PACKET_END;
} blog_pos_xy;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       DesPx;
    float       DesPy;
    float       DesVx;
    float       DesVy;
    float       DesAx;
    float       DesAy;
    LOG_PACKET_END;
} blog_psc_xy;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       Pz;
    float       Vz;
    float       Az;
    LOG_PACKET_END;
} blog_pos_z;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       DesPz;
    float       DesVz;
    float       DesAz;
    LOG_PACKET_END;
} blog_psc_z;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    uint16_t    chan1_raw;
    uint16_t    chan2_raw;
    uint16_t    chan3_raw;
    uint16_t    chan4_raw;
    uint16_t    chan5_raw;
    uint16_t    chan6_raw;
    uint16_t    chan7_raw;
    uint16_t    chan8_raw;
    uint16_t    chan9_raw;
    uint16_t    chan10_raw;
    uint16_t    chan11_raw;
    uint16_t    chan12_raw;
    uint16_t    chan13_raw;
    uint16_t    chan14_raw;
    uint16_t    chan15_raw;
    uint16_t    chan16_raw;
    uint16_t    chan17_raw;
    uint16_t    chan18_raw;
    LOG_PACKET_END;
} blog_rc_in;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    uint16_t    servo1_raw;
    uint16_t    servo2_raw;
    uint16_t    servo3_raw;
    uint16_t    servo4_raw;
    uint16_t    servo5_raw;
    uint16_t    servo6_raw;
    uint16_t    servo7_raw;
    uint16_t    servo8_raw;
    LOG_PACKET_END;
} blog_srv_out;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       ThrIn;
    float       DesAlt;
    float       InavAlt;
    float       BaroAlt;
    float       TargetClimbRate;
    float       ClimbRate;
    LOG_PACKET_END;
} blog_ver_ctrl;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    uint8_t     mode;
    uint8_t     ModeReason;
    LOG_PACKET_END;
} blog_flight_mode;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    uint8_t     event;
    int8_t      EventCode;
    LOG_PACKET_END;
} blog_event;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       RollRatTar;
    float       RollRatErr;
    float       PitchRatTar;
    float       PitchRatErr;
    float       YawRatTar;
    float       YawRatErr;
    LOG_PACKET_END;
} blog_pid;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float       RollTorque;
    float       PitchTorque;
    float       YawTorque;
    float       ThrustIn;
    LOG_PACKET_END;
} blog_torque;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    uint16_t    command_total;
    uint16_t    sequence;
    uint16_t    command;
    float       param1;
    float       param2;
    float       param3;
    float       param4;
    int32_t     latitude;
    int32_t     longitude;
    float       altitude;
    uint8_t     frame;
    LOG_PACKET_END;
} blog_mission_cmd;

typedef struct {
    LOG_PACKET_HEADER;
    uint32_t    time_ms;
    float desired_accel_x;
    float desired_accel_y;
    float predicted_accel_x;
    float predicted_accel_y;
    float brake_accel;
    float loiter_accel_brake_x;
    float loiter_accel_brake_y;
    float desired_vel_x;
    float desired_vel_y;
    float desired_speed;
    float drag_decel;
    float des_vel_norm_x;
    float des_vel_norm_y;
    LOG_PACKET_END;
} blog_loiter;
#pragma pack()

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void blog_write_imu(const float gyr[3],const float acc[3],const float filter_gyr[3],const float filter_acc[3]);
void blog_write_imu2();
void blog_write_mag();
void blog_write_gps();
void blog_write_att();
void blog_write_att_ctrl();
void blog_write_pos_xy();
void blog_write_pos_ctrl_xy();
void blog_write_pos_z();
void blog_write_pos_ctrl_z();
void blog_write_rc_in();
void blog_write_srv_out();
void blog_write_vertical_ctrl();
void blog_write_mode(uint8_t mode, uint8_t mode_reason);
void blog_write_event(uint8_t event, int8_t event_code);
void blog_write_att_pid();
void blog_write_loiter();
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



