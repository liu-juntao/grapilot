
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       blog.h
  * @author     baiyang
  * @date       2021-8-6
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>

#include "blog_msg.h"

#include <common/gp_defines.h>
#include <parameter/param.h>
/*-----------------------------------macro------------------------------------*/
#define BLOG_MAX_CALLBACK_NUM 10
/*----------------------------------typedef-----------------------------------*/
#pragma pack(1)

typedef struct {
    char name[BLOG_MAX_NAME_LEN];
    uint16_t type;
    uint16_t number;
} blog_elem_t;


typedef struct {
    char name[BLOG_MAX_NAME_LEN];
    uint8_t msg_id;
    uint8_t num_elem;
    blog_elem_t* elem_list;
} blog_bus_t;


typedef struct {
    //log info
    uint16_t version;
    uint32_t timestamp;
    uint16_t max_name_len;
    uint16_t max_desc_len;
    uint16_t max_model_info_len;
    char description[BLOG_DESCRIPTION_SIZE];
    char model_info[BLOG_MODEL_INFO_SIZE];
    //bus info
    uint8_t num_bus;
    blog_bus_t* bus_list;
    //parameter info
    uint8_t num_param_group;
    param_group_t* param_group_list;
} blog_header_t;
#pragma pack()

typedef struct {
    uint8_t* data;
    uint32_t head; // head point for sector
    uint32_t tail; // tail point for sector
    uint32_t num_sector;
    uint32_t index; // index in sector
} blog_buffer_t;

#define BLOG_ELEMENT(_name, _type) \
    {                              \
            #_name,                \
            _type,                 \
            1                      \
    }

#define BLOG_ELEMENT_VEC(_name, _type, _num) \
    {                                        \
            #_name,                          \
            _type,                           \
            _num                             \
    }

#define BLOG_BUS(_name, _id, _elem_list)              \
    {                                                 \
            #_name,                                   \
            _id,                                      \
            sizeof(_elem_list) / sizeof(blog_elem_t), \
            _elem_list                                \
    }

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
gp_err blog_add_desc(char* desc);
gp_err blog_start(char* file_name);
void blog_stop(void);
gp_err blog_push_msg_data(const void* payload, uint16_t len);

uint8_t blog_get_status(void);
char* blog_get_logging_file_name(void);
void blog_show_status(void);

gp_err blog_register_callback(uint8_t cb_type, void (*cb)(void));

void blog_init(void);
int32_t blog_get_log_mode();
void blog_async_output(void);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



