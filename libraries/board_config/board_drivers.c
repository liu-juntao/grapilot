
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       board_drivers.c
  * @author     baiyang
  * @date       2021-10-4
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "borad_config.h"

#include <rtthread.h>

#include <common/time/gp_time.h>
#include <common/console/console.h>
#include <file_manager/file_manager.h>
#include <parameter/param.h>
#include <uITC/uITC_msg.h>
#include <serial_manager/task_serial.h>
#include <rc_channel/rc.h>

#if HAL_WITH_IO_MCU
#include <iomcu/gp_iomcu.h>
#endif

#include <rc_channel/rc_channel.h>
#include <srv_channel/srv_channel.h>
#include <mavproxy/mavproxy.h>
#include <notify/notify.h>
#include <rtc/gp_rtc.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
static inline void brd_board_setup_time()
{
    time_init();
}

static inline void brd_board_setup_console()
{
    console_init();
}

static inline void brd_board_setup_fs_and_param()
{
    fs_init();
    param_init();
}

static inline void brd_board_setup_uitc()
{
    uitc_msg_init();
}

static inline void brd_board_setup_uart()
{
    task_serial_init();
}

static inline void brd_board_setup_mavproxy()
{
    mavproxy_init();
}

#if HAL_WITH_IO_MCU
static inline void brd_board_setup_iomcu()
{
    iomcu_ctor();
    iomcu_init();
}
#endif

static inline void brd_board_setup_rcin()
{
    rc_hal_ctor();
    rc_hal_init();
}

static inline void brd_board_setup_srv()
{
    srv_channels_ctor();
    srv_channels_init();
}

/*
  init safety state
 */
static inline void brd_board_init_safety(board_config *brd)
{
#if HAL_HAVE_SAFETY_SWITCH
    bool force_safety_off = (brd->state.safety_enable == 0);

    if (force_safety_off) {
        srv_hal_force_safety_off();
        // wait until safety has been turned off
        uint8_t count = 20;
        while (srv_hal_safety_switch_state() != SAFETY_ARMED && count--) {
            rt_thread_mdelay(20);
        }
    }
#endif
}

/*
  init safety state
 */
static inline void brd_board_setup_notify()
{
    notify_ctor();
    notify_init();
}

/*
  init safety state
 */
static inline void brd_board_setup_rtc()
{
    rtc_ctor();
}

static void brd_board_autodetect(board_config *brd)
{
#if defined(HAL_RTTHREAD_ARCH_FMUV2)
    // only one choice
    brd->state.board_type = PX4_BOARD_PIXHAWK;
    console_printf("Detected FMUv2\n");
#elif defined(HAL_RTTHREAD_ARCH_FMUV3)
    // only one choice
    brd->state.board_type = PX4_BOARD_PIXHAWK2;
    console_printf("Detected FMUv3\n");
#elif defined(HAL_RTTHREAD_ARCH_FMUV4)
    // only one choice
    brd->state.board_type = PX4_BOARD_PIXRACER;
    console_printf("Detected Pixracer\n");
#elif defined(HAL_RTTHREAD_ARCH_FMUV5)
    // only one choice
    brd->state.board_type = PX4_BOARD_FMUV5;
    console_printf("Detected FMUv5\n");
#elif defined(HAL_RTTHREAD_ARCH_FMUV6)
    // only one choice
    brd->state.board_type = PX4_BOARD_FMUV6;
    console_printf("Detected FMUv6\n");
#endif
}

static void brd_board_setup_drivers(board_config *brd)
{
    // run board auto-detection
    brd_board_autodetect(brd);

    brd->px4_configured_board = (px4_board_type)brd->state.board_type;

    switch (brd->px4_configured_board) {
    case PX4_BOARD_PX4V1:
    case PX4_BOARD_PIXHAWK:
    case PX4_BOARD_PIXHAWK2:
    case PX4_BOARD_FMUV5:
        brd->state.io_enable = 1;
        break;
    case PX4_BOARD_FMUV6:
    case PX4_BOARD_SP01:
    case PX4_BOARD_PIXRACER:
    case PX4_BOARD_PHMINI:
    case PX4_BOARD_AUAV21:
    case PX4_BOARD_PH2SLIM:
    case VRX_BOARD_BRAIN51:
    case VRX_BOARD_BRAIN52:
    case VRX_BOARD_BRAIN52E:
    case VRX_BOARD_UBRAIN51:
    case VRX_BOARD_UBRAIN52:
    case VRX_BOARD_CORE10:
    case VRX_BOARD_BRAIN54:
    case PX4_BOARD_AEROFC:
    case PX4_BOARD_PIXHAWK_PRO:
    case PX4_BOARD_PCNC1:
    case PX4_BOARD_MINDPXV2:
        break;
    default:
        console_printf("Unknown board type");
        break;
    }
}

/**
  * @brief       
  * @param[in]   brd  
  * @param[out]  
  * @retval      
  * @note        
  */
void brd_board_setup(board_config *brd)
{
    brd_board_setup_time();
    brd_board_setup_console();
    brd_board_setup_fs_and_param();
    brd_board_setup_uitc();
    brd_board_setup_uart();

    // 不要此函数之前调用mavproxy_send_statustext（向地面站发送文本消息）
    brd_board_setup_mavproxy();

#if GP_FEATURE_BOARD_DETECT
    brd_board_setup_drivers(brd);
#endif

#if HAL_WITH_IO_MCU
    brd_board_setup_iomcu();
#endif

    brd_board_setup_rcin();
    brd_board_setup_srv();

    brd_board_init_safety(brd);
    brd_board_setup_notify();
    brd_board_setup_rtc();
}
/*------------------------------------test------------------------------------*/


