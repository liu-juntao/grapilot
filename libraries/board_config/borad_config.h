
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       borad_config.h
  * @author     baiyang
  * @date       2021-10-4
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
#include <rtconfig.h>
#include <common/gp_defines.h>
/*-----------------------------------macro------------------------------------*/
#ifndef GP_FEATURE_BOARD_DETECT
#if defined(HAL_RTTHREAD_ARCH_FMUV2) || defined(HAL_RTTHREAD_ARCH_FMUV3) || defined(HAL_RTTHREAD_ARCH_FMUV4) || defined(HAL_RTTHREAD_ARCH_FMUV5) || defined(HAL_RTTHREAD_ARCH_FMUV6)
#define GP_FEATURE_BOARD_DETECT 1
#else
#define GP_FEATURE_BOARD_DETECT 0
#endif
#endif
/*----------------------------------typedef-----------------------------------*/
// valid types for BRD_TYPE: these values need to be in sync with the
// values from the param description
typedef enum {
    BOARD_TYPE_UNKNOWN = -1,
    PX4_BOARD_AUTO     = 0,
    PX4_BOARD_PX4V1    = 1,
    PX4_BOARD_PIXHAWK  = 2,
    PX4_BOARD_PIXHAWK2 = 3,
    PX4_BOARD_PIXRACER = 4,
    PX4_BOARD_PHMINI   = 5,
    PX4_BOARD_PH2SLIM  = 6,
    PX4_BOARD_AEROFC   = 13,
    PX4_BOARD_PIXHAWK_PRO = 14,
    PX4_BOARD_AUAV21   = 20,
    PX4_BOARD_PCNC1    = 21,
    PX4_BOARD_MINDPXV2 = 22,
    PX4_BOARD_SP01     = 23,
    PX4_BOARD_FMUV5    = 24,
    VRX_BOARD_BRAIN51  = 30,
    VRX_BOARD_BRAIN52  = 32,
    VRX_BOARD_BRAIN52E = 33,
    VRX_BOARD_UBRAIN51 = 34,
    VRX_BOARD_UBRAIN52 = 35,
    VRX_BOARD_CORE10   = 36,
    VRX_BOARD_BRAIN54  = 38,
    PX4_BOARD_FMUV6    = 39,
    PX4_BOARD_OLDDRIVERS = 100,
} px4_board_type;

enum board_safety_button_option {
    BOARD_SAFETY_OPTION_BUTTON_ACTIVE_SAFETY_OFF= (1 << 0),
    BOARD_SAFETY_OPTION_BUTTON_ACTIVE_SAFETY_ON=  (1 << 1),
    BOARD_SAFETY_OPTION_BUTTON_ACTIVE_ARMED=      (1 << 2),
    BOARD_SAFETY_OPTION_SAFETY_ON_DISARM=         (1 << 3),
};

enum safety_state {
    SAFETY_NONE,
    SAFETY_DISARMED,
    SAFETY_ARMED
};

/** @ 
  * @brief  每个阶段初始化完成标志
  */
enum init_stage {
    INIT_STAGE_HARDWARE  = 0,
    INIT_STAGE_BOARD,
    INIT_STAGE_COMM,
    INIT_STAGE_LOGGER,
    INIT_STAGE_AHRS_NAV,
    INIT_STAGE_ESC_CAL1,
    INIT_STAGE_ESC_CAL2,
    INIT_STAGE_MMS,
    INIT_STAGE_FMS,
    INIT_STAGE_END,
};

typedef struct {
    Param_int8  safety_enable;
    Param_int16 safety_option;
    Param_int32 ignore_safety_channels;
    Param_int8  board_type;
    Param_int8  io_enable;
} board_state;

typedef struct {
    board_state state;
    bool _in_error_loop;
    enum init_stage vehicle_init_stage;
    px4_board_type px4_configured_board;

    // we start soft_armed false, so that actuators don't send any
    // values until the vehicle code has fully started
    bool soft_armed;
    uint32_t last_armed_change_ms;
} board_config;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void brd_init();
void brd_set_vehicle_init_stage(enum init_stage stage);
enum init_stage  brd_get_vehicle_init_stage();
void brd_set_vehicle_init_finish();

bool brd_is_vehicle_init_finish();
bool brd_is_vehicle_esc_calibrate();

px4_board_type brd_get_board_type();

void brd_board_setup(board_config *brd);
void brd_config_error(const char *fmt, ...);
uint8_t brd_io_enabled(void);
uint16_t brd_get_safety_button_options(void);
uint16_t brd_get_safety_mask(void);
bool brd_safety_button_handle_pressed(uint8_t press_count);
enum safety_state brd_safety_switch_state();

void brd_set_soft_armed(const bool b);
bool brd_get_soft_armed();

void brd_set_default_safety_ignore_mask(uint16_t mask);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



