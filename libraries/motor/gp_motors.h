
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_motors.h
  * @author     baiyang
  * @date       2021-8-8
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include <srv_channel/srv_channel.h>
#include <common/gp_math/gp_mathlib.h>
/*-----------------------------------macro------------------------------------*/
// offsets for motors in motor_out and _motor_filtered arrays
#define GP_MOTORS_MOT_1 0U
#define GP_MOTORS_MOT_2 1U
#define GP_MOTORS_MOT_3 2U
#define GP_MOTORS_MOT_4 3U
#define GP_MOTORS_MOT_5 4U
#define GP_MOTORS_MOT_6 5U
#define GP_MOTORS_MOT_7 6U
#define GP_MOTORS_MOT_8 7U
#define GP_MOTORS_MOT_9 8U
#define GP_MOTORS_MOT_10 9U
#define GP_MOTORS_MOT_11 10U
#define GP_MOTORS_MOT_12 11U

#define GP_MOTORS_MAX_NUM_MOTORS 12

// motor update rate
#define GP_MOTORS_SPEED_DEFAULT     400 // default output rate to the motors

/*----------------------------------typedef-----------------------------------*/
typedef enum {
    MOTOR_FRAME_UNDEFINED = 0,
    MOTOR_FRAME_QUAD = 1,
    MOTOR_FRAME_HEXA = 2,
    MOTOR_FRAME_OCTA = 3,
    MOTOR_FRAME_OCTAQUAD = 4,
    MOTOR_FRAME_Y6 = 5,
    MOTOR_FRAME_HELI = 6,
    MOTOR_FRAME_TRI = 7,
    MOTOR_FRAME_SINGLE = 8,
    MOTOR_FRAME_COAX = 9,
    MOTOR_FRAME_TAILSITTER = 10,
    MOTOR_FRAME_HELI_DUAL = 11,
    MOTOR_FRAME_DODECAHEXA = 12,
    MOTOR_FRAME_HELI_QUAD = 13,
    MOTOR_FRAME_DECA = 14,
} motor_frame_class;

typedef enum {
    MOTOR_FRAME_TYPE_PLUS = 0,
    MOTOR_FRAME_TYPE_X = 1,
    MOTOR_FRAME_TYPE_V = 2,
    MOTOR_FRAME_TYPE_H = 3,
    MOTOR_FRAME_TYPE_VTAIL = 4,
    MOTOR_FRAME_TYPE_ATAIL = 5,
    MOTOR_FRAME_TYPE_PLUSREV = 6, // plus with reversed motor direction
    MOTOR_FRAME_TYPE_Y6B = 10,
    MOTOR_FRAME_TYPE_Y6F = 11, // for FireFlyY6
    MOTOR_FRAME_TYPE_BF_X = 12, // X frame, betaflight ordering
    MOTOR_FRAME_TYPE_DJI_X = 13, // X frame, DJI ordering
    MOTOR_FRAME_TYPE_CW_X = 14, // X frame, clockwise ordering
    MOTOR_FRAME_TYPE_I = 15, // (sideways H) octo only
    MOTOR_FRAME_TYPE_NYT_PLUS = 16, // plus frame, no differential torque for yaw
    MOTOR_FRAME_TYPE_NYT_X = 17, // X frame, no differential torque for yaw
    MOTOR_FRAME_TYPE_BF_X_REV = 18, // X frame, betaflight ordering, reversed motors
} motor_frame_type;

// desired spool states
typedef enum {
    MOTOR_DESIRED_SHUT_DOWN = 0,              // all motors should move to stop
    MOTOR_DESIRED_GROUND_IDLE = 1,            // all motors should move to ground idle
    MOTOR_DESIRED_THROTTLE_UNLIMITED = 2,     // motors should move to being a state where throttle is unconstrained (e.g. by start up procedure)
} DesiredSpoolState;

// spool states
typedef enum {
    MOTOR_SHUT_DOWN = 0,              // all motors stop
    MOTOR_GROUND_IDLE = 1,            // all motors at ground idle
    MOTOR_THROTTLE_UNLIMITED = 2,     // throttle is no longer constrained by start up procedure
    MOTOR_SPOOLING_UP = 3,            // increasing maximum throttle while stabilizing
    MOTOR_SPOOLING_DOWN = 4,          // decreasing maximum throttle while stabilizing
} SpoolState;

// structure for holding motor limit flags
typedef struct {
    uint8_t roll            : 1; // we have reached roll or pitch limit
    uint8_t pitch           : 1; // we have reached roll or pitch limit
    uint8_t yaw             : 1; // we have reached yaw limit
    uint8_t throttle_lower  : 1; // we have reached throttle's lower limit
    uint8_t throttle_upper  : 1; // we have reached throttle's upper limit
} Motors_limit;

typedef enum {
    PWM_TYPE_NORMAL     = 0,
    PWM_TYPE_ONESHOT    = 1,
    PWM_TYPE_ONESHOT125 = 2,
    PWM_TYPE_BRUSHED    = 3,
    PWM_TYPE_DSHOT150   = 4,
    PWM_TYPE_DSHOT300   = 5,
    PWM_TYPE_DSHOT600   = 6,
    PWM_TYPE_DSHOT1200  = 7,
    PWM_TYPE_PWM_RANGE  = 8
} motors_pwm_type;

typedef struct
{
    //参数
    int8_t _pwm_type;            // PWM output type

    Motors_limit limit;

    bool _armed;             // 0 if disarmed, 1 if armed
    bool _interlock;         // 1 if the motor interlock is enabled (i.e. motors run), 0 if disabled (motors don't run)
    bool _initialised_ok;    // 1 if initialisation was successful

    // internal variables
    uint16_t            _loop_rate;                 // rate in Hz at which output() function is called (normally 400hz)
    uint16_t            _speed_hz;                  // speed in hz to send updates to motors
    float               _roll_in;                   // desired roll control from attitude controllers, -1 ~ +1
    float               _roll_in_ff;                // desired roll feed forward control from attitude controllers, -1 ~ +1
    float               _pitch_in;                  // desired pitch control from attitude controller, -1 ~ +1
    float               _pitch_in_ff;               // desired pitch feed forward control from attitude controller, -1 ~ +1
    float               _yaw_in;                    // desired yaw control from attitude controller, -1 ~ +1
    float               _yaw_in_ff;                 // desired yaw feed forward control from attitude controller, -1 ~ +1
    float               _throttle_in;               // last throttle input from set_throttle caller
    float               _throttle_out;              // throttle after mixing is complete
    float               _forward_in;                // last forward input from set_forward caller
    float               _lateral_in;                // last lateral input from set_lateral caller
    float               _throttle_avg_max;          // last throttle input from set_throttle_avg_max
    LowPassFilt         _throttle_filter;           // throttle input filter
    DesiredSpoolState   _spool_desired;             // desired spool state
    SpoolState          _spool_state;               // current spool mode

    // air pressure compensation variables
    float               _air_density_ratio;     // air density / sea level density - decreases in altitude

    // mask of what channels need fast output
    uint16_t            _motor_fast_mask;

    // mask of what channels need to use SERVOn_MIN/MAX for output mapping
    uint16_t            _motor_pwm_range_mask;

    // pass through variables
    float _roll_radio_passthrough;     // roll input from pilot in -1 ~ +1 range.  used for setup and providing servo feedback while landed
    float _pitch_radio_passthrough;    // pitch input from pilot in -1 ~ +1 range.  used for setup and providing servo feedback while landed
    float _throttle_radio_passthrough; // throttle/collective input from pilot in 0 ~ 1 range.  used for setup and providing servo feedback while landed
    float _yaw_radio_passthrough;      // yaw input from pilot in -1 ~ +1 range.  used for setup and providing servo feedback while landed

    // motor failure handling
    bool                _thrust_boost;          // true if thrust boost is enabled to handle motor failure
    bool                _thrust_balanced;       // true when output thrust is well balanced
    float               _thrust_boost_ratio;    // choice between highest and second highest motor output for output mixing (0 ~ 1). Zero is normal operation

    // 输出的pwm
    uint16_t            _motor_output_pwm[GP_MOTORS_MAX_NUM_MOTORS];

    // 电机数量
    uint8_t             _motor_quantity;
}Motors_HandleTypeDef;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
// Constructor
void          Motors(Motors_HandleTypeDef *pMotors, uint16_t loop_rate, uint16_t speed_hz);

// check initialisation succeeded
bool          Motors_initialised_ok(Motors_HandleTypeDef *pMotors);
void          Motors_set_initialised_ok(Motors_HandleTypeDef *pMotors, bool val);

// arm, disarm or check status status of motors
static inline bool Motors_get_armed(Motors_HandleTypeDef *pMotors) { return pMotors->_armed; }

static inline void Motors_set_armed(Motors_HandleTypeDef *pMotors, bool arm) {
    if (pMotors->_armed != arm) {
        pMotors->_armed = arm;

        if (!arm) {
            //MotorsMC_save_params_on_disarm(((MotorsMC_HandleTypeDef *))pMotors);
        }
    }
};

void          Motors_set_desired_spool_state(Motors_HandleTypeDef *pMotors, DesiredSpoolState spool);

DesiredSpoolState  Motors_get_desired_spool_state(Motors_HandleTypeDef *pMotors);

// get_spool_state - get current spool state
SpoolState    Motors_get_spool_state(Motors_HandleTypeDef *pMotors);

// set_density_ratio - sets air density as a proportion of sea level density
void          Motors_set_air_density_ratio(Motors_HandleTypeDef *pMotors, float ratio);

// set limit flag for pitch, roll and yaw
void          Motors_set_limit_flag_pitch_roll_yaw(Motors_HandleTypeDef *pMotors, bool flag);

//
// virtual functions that should be implemented by child classes
//

// set update rate to motors - a value in hertz
void          Motors_set_update_rate(Motors_HandleTypeDef *pMotors, uint16_t speed_hz );

// pilot input in the -1 ~ +1 range for roll, pitch and yaw. 0~1 range for throttle
void          Motors_set_radio_passthrough(Motors_HandleTypeDef *pMotors, float roll_input, float pitch_input, float throttle_input, float yaw_input);

// set loop rate. Used to support loop rate as a parameter
void          Motors_set_loop_rate(Motors_HandleTypeDef *pMotors, uint16_t loop_rate);

// return the roll factor of any motor, this is used for tilt rotors and tail sitters
// using copter motors for forward flight
float         Motors_get_roll_factor(Motors_HandleTypeDef *pMotors, uint8_t i);

// This function required for tradheli. Tradheli initializes targets when going from unarmed to armed state.
// This function is overriden in motors_heli class.   Always true for multicopters.
bool          Motors_init_targets_on_arming(Motors_HandleTypeDef *pMotors);

motors_pwm_type Motors_get_pwm_type(Motors_HandleTypeDef *pMotors);

// output functions that should be overloaded by child classes
void          Motors_rc_write(Motors_HandleTypeDef *pMotors, uint8_t chan, uint16_t pwm);

// add a motor to the motor map
void          Motors_add_motor_num(Motors_HandleTypeDef *pMotors, int8_t motor_num);

/*
  map an internal motor mask to real motor mask, accounting for
  SERVOn_FUNCTION mappings, and allowing for multiple outputs per
  motor number
 */
uint32_t      Motors_motor_mask_to_srv_channel_mask(uint32_t mask);

/*
  set frequency of a set of channels
 */
void Motors_rc_set_freq(Motors_HandleTypeDef *pMotors,uint32_t mask, uint16_t freq_hz);

// set motor interlock status
static inline void Motors_set_interlock(Motors_HandleTypeDef *pMotors, bool set)
{
    pMotors->_interlock = set;
}

// get motor interlock status.  true means motors run, false motors don't run
static inline bool Motors_get_interlock(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_interlock;
}

// set_roll, set_pitch, set_yaw, set_throttle
static inline void Motors_set_roll(Motors_HandleTypeDef *pMotors, float roll_in)
{
    pMotors->_roll_in = roll_in;
};        // range -1 ~ +1

static inline void Motors_set_roll_ff(Motors_HandleTypeDef *pMotors, float roll_in)
{
    pMotors->_roll_in_ff = roll_in;
};    // range -1 ~ +1

static inline void Motors_set_pitch(Motors_HandleTypeDef *pMotors, float pitch_in)
{
    pMotors->_pitch_in = pitch_in;
};    // range -1 ~ +1

static inline void Motors_set_pitch_ff(Motors_HandleTypeDef *pMotors, float pitch_in)
{
    pMotors->_pitch_in_ff = pitch_in;
};  // range -1 ~ +1

static inline void Motors_set_yaw(Motors_HandleTypeDef *pMotors, float yaw_in)
{
    pMotors->_yaw_in = yaw_in;
};            // range -1 ~ +1

static inline void Motors_set_yaw_ff(Motors_HandleTypeDef *pMotors, float yaw_in)
{
    pMotors->_yaw_in_ff = yaw_in;
};      // range -1 ~ +1

static inline void Motors_set_throttle(Motors_HandleTypeDef *pMotors, float throttle_in)
{
    pMotors->_throttle_in = throttle_in;
};   // range 0 ~ 1

static inline void Motors_set_throttle_avg_max(Motors_HandleTypeDef *pMotors, float throttle_avg_max)
{
    pMotors->_throttle_avg_max = math_constrain_float(throttle_avg_max, 0.0f, 1.0f);
};   // range 0 ~ 1

static inline void Motors_set_throttle_filter_cutoff(Motors_HandleTypeDef *pMotors, float filt_hz)
{
    lpf_set_cutoff1(&pMotors->_throttle_filter,filt_hz);
}

static inline void Motors_set_forward(Motors_HandleTypeDef *pMotors, float forward_in)
{
    pMotors->_forward_in = forward_in;
}; // range -1 ~ +1

static inline void Motors_set_lateral(Motors_HandleTypeDef *pMotors, float lateral_in)
{
    pMotors->_lateral_in = lateral_in;
};     // range -1 ~ +1

// accessors for roll, pitch, yaw and throttle inputs to motors
static inline float Motors_get_roll(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_roll_in;
}

static inline float Motors_get_pitch(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_pitch_in;
}

static inline float Motors_get_yaw(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_yaw_in;
}

static inline float Motors_get_throttle_out(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_throttle_out;
}

static inline float Motors_get_throttle(Motors_HandleTypeDef *pMotors)
{
    return math_constrain_float(lpf_get_output(&pMotors->_throttle_filter), 0.0f, 1.0f);
}

static inline float Motors_get_throttle_bidirectional(Motors_HandleTypeDef *pMotors)
{
    return math_constrain_float(2 * (lpf_get_output(&pMotors->_throttle_filter) - 0.5f), -1.0f, 1.0f);
}

static inline float Motors_get_forward(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_forward_in;
}

static inline float Motors_get_lateral(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_lateral_in;
}

//float         Motors_get_throttle_hover(Motors_HandleTypeDef *pMotors);  //其他文件实现

// motor failure handling
static inline void Motors_set_thrust_boost(Motors_HandleTypeDef *pMotors, bool enable)
{
    pMotors->_thrust_boost = enable;
}

static inline bool Motors_get_thrust_boost(Motors_HandleTypeDef *pMotors)
{
    return pMotors->_thrust_boost;
}

// returns true is pwm type is normal
static inline bool Motors_is_normal_pwm_type(Motors_HandleTypeDef *pMotors) {
    return (pMotors->_pwm_type == PWM_TYPE_NORMAL) || (pMotors->_pwm_type == PWM_TYPE_PWM_RANGE);
}

// returns true is pwm type is brushed
static inline bool Motors_is_brushed_pwm_type(Motors_HandleTypeDef *pMotors) { return pMotors->_pwm_type == PWM_TYPE_BRUSHED; }

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



