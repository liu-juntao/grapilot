
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_gps_param.c
  * @author     baiyang
  * @date       2022-2-20
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "sensor_gps.h"
#include <parameter/param.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void sensor_gps_assign_param()
{
    param_link_variable(PARAM_ID(GPS, GPS_TYPE), &gps._type[0]);
    param_link_variable(PARAM_ID(GPS, GPS_TYPE2), &gps._type[1]);
    param_link_variable(PARAM_ID(GPS, GPS_NAVFILTER), &gps._navfilter);
    param_link_variable(PARAM_ID(GPS, GPS_AUTO_SWITCH), &gps._auto_switch);
    param_link_variable(PARAM_ID(GPS, GPS_MIN_DGPS), &gps._min_dgps);
    param_link_variable(PARAM_ID(GPS, GPS_SBAS_MODE), &gps._sbas_mode);
    param_link_variable(PARAM_ID(GPS, GPS_MIN_ELEV), &gps._min_elevation);
    param_link_variable(PARAM_ID(GPS, GPS_INJECT_TO), &gps._inject_to);
    param_link_variable(PARAM_ID(GPS, GPS_SBP_LOGMASK), &gps._sbp_logmask);
    param_link_variable(PARAM_ID(GPS, GPS_RAW_DATA), &gps._raw_data);
    param_link_variable(PARAM_ID(GPS, GPS_GNSS_MODE), &gps._gnss_mode[0]);
    param_link_variable(PARAM_ID(GPS, GPS_SAVE_CFG), &gps._save_config);
    param_link_variable(PARAM_ID(GPS, GPS_GNSS_MODE2), &gps._gnss_mode[1]);
    param_link_variable(PARAM_ID(GPS, GPS_AUTO_CONFIG), &gps._auto_config);
    param_link_variable(PARAM_ID(GPS, GPS_RATE_MS), &gps._rate_ms[0]);
    param_link_variable(PARAM_ID(GPS, GPS_RATE_MS2), &gps._rate_ms[1]);
    param_link_variable(PARAM_ID(GPS, GPS_POS1_X), &gps._antenna_offset[0].x);
    param_link_variable(PARAM_ID(GPS, GPS_POS1_Y), &gps._antenna_offset[0].y);
    param_link_variable(PARAM_ID(GPS, GPS_POS1_Z), &gps._antenna_offset[0].z);
    param_link_variable(PARAM_ID(GPS, GPS_POS2_X), &gps._antenna_offset[1].x);
    param_link_variable(PARAM_ID(GPS, GPS_POS2_Y), &gps._antenna_offset[1].y);
    param_link_variable(PARAM_ID(GPS, GPS_POS2_Z), &gps._antenna_offset[1].z);
    param_link_variable(PARAM_ID(GPS, GPS_DELAY_MS), &gps._delay_ms[0]);
    param_link_variable(PARAM_ID(GPS, GPS_DELAY_MS2), &gps._delay_ms[1]);
    param_link_variable(PARAM_ID(GPS, GPS_BLEND_MASK), &gps._blend_mask);
    param_link_variable(PARAM_ID(GPS, GPS_BLEND_TC), &gps._blend_tc);
    param_link_variable(PARAM_ID(GPS, GPS_DRV_OPTIONS), &gps._driver_options);
    param_link_variable(PARAM_ID(GPS, GPS_COM_PORT), &gps._port[0]);
    param_link_variable(PARAM_ID(GPS, GPS_COM_PORT2), &gps._port[1]);
    param_link_variable(PARAM_ID(GPS, GPS_PRIMARY), &gps._primary);
}

/*------------------------------------test------------------------------------*/


