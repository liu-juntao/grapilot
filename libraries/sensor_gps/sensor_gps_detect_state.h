
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_gps_detect_state.h
  * @author     baiyang
  * @date       2022-2-16
  ******************************************************************************
  */

/*
  GPS detection state structures. These need to be in a separate
  header to prevent a circular dependency between AP_GPS and the
  backend drivers.

  These structures are allocated as a single block in AP_GPS during
  driver detection, then freed once the detection is finished. Each
  GPS driver needs to implement a static _detect() function which uses
  this state information to detect if the attached GPS is of the
  specific type that it handles.
 */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
struct NMEA_detect_state {
    uint8_t step;
    uint8_t ck;
};

struct SIRF_detect_state {
    uint16_t checksum;
    uint8_t step, payload_length, payload_counter;
};

struct UBLOX_detect_state {
    uint8_t payload_length, payload_counter;
    uint8_t step;
    uint8_t ck_a, ck_b;
};

struct ERB_detect_state {
    uint8_t payload_length, payload_counter;
    uint8_t step;
    uint8_t ck_a, ck_b;
};

enum detect_state_enum {
    WAITING = 0,
    GET_TYPE = 1,
    GET_SENDER = 2,
    GET_LEN = 3,
    GET_MSG = 4,
    GET_CRC = 5
};

struct SBP_detect_state {
    enum detect_state_enum state:8;
    uint16_t msg_type;
    uint8_t n_read;
    uint8_t msg_len;
    uint16_t crc_so_far;
    uint16_t crc;
    uint8_t heartbeat_buff[4];
};

struct SBP2_detect_state {
    enum detect_state_enum state:8;
    uint16_t msg_type;
    uint8_t n_read;
    uint8_t msg_len;
    uint16_t crc_so_far;
    uint16_t crc;
    uint8_t heartbeat_buff[4];
};

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



