
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       rc.h
  * @author     baiyang
  * @date       2021-8-8
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#include <rtthread.h>
/*-----------------------------------macro------------------------------------*/
#ifndef RC_INPUT_MAX_CHANNELS
#define RC_INPUT_MAX_CHANNELS 18
#endif

#define RC_MASK_1 0x0001
#define RC_MASK_2 0x0002
#define RC_MASK_3 0x0004
#define RC_MASK_4 0x0008
#define RC_MASK_5 0x0010
#define RC_MASK_6 0x0020
#define RC_MASK_7 0x0040
#define RC_MASK_8 0x0080
#define RC_MASK_9 0x0100
#define RC_MASK_10 0x0200
#define RC_MASK_11 0x0400
#define RC_MASK_12 0x0800
#define RC_MASK_13 0x1000
#define RC_MASK_14 0x2000
#define RC_MASK_15 0x4000
#define RC_MASK_16 0x8000
#define RC_MASK_17 0x10000
#define RC_MASK_18 0x20000

#define RC_MASK_1_2 0x0003
#define RC_MASK_1_4 0x000F
#define RC_MASK_1_6 0x003F
#define RC_MASK_1_8 0x00FF
#define RC_MASK_1_10 0x03FF
#define RC_MASK_1_12 0x0FFF
#define RC_MASK_1_14 0x3FFF
#define RC_MASK_ALL 0xFFFF

/*----------------------------------typedef-----------------------------------*/
/** @ 
  * @brief  
  */
typedef struct {
    bool _init;

    uint16_t _rc_values[RC_INPUT_MAX_CHANNELS];

    uint64_t _last_read;
    uint8_t _num_channels;

    struct rt_mutex rcin_mutex;

    int16_t _rssi;
    int16_t _rx_link_quality;

    uint64_t _rcin_timestamp_last_signal;
    const char *last_protocol;
    bool pulse_input_enabled;

#if HAL_WITH_IO_MCU
    uint64_t last_iomcu_us;
#endif
} rc_hal_mgr;

/*----------------------------------variable----------------------------------*/
extern rc_hal_mgr rc_hal;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void rc_hal_ctor();
void rc_hal_init();

/*
  enable or disable pulse input for RC input. This is used to reduce
  load when we are decoding R/C via a UART
*/
void rc_hal_pulse_input_enable(bool enable);

bool rc_hal_new_input();

uint16_t rc_hal_read(uint8_t channel);
uint8_t rc_hal_read2(uint16_t* periods, uint8_t len);

uint8_t rc_hal_num_channels();

/*
  start a bind operation, if supported
 */
bool rc_hal_rc_bind(int dsmMode);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



