
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       rc_interface.c
  * @author     baiyang
  * @date       2021-8-8
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <stdint.h>

#include "rc_channel.h"
#include <common/time/gp_time.h>
#include <parameter/param.h>

#include <fms.h>
#include <mavproxy/mavproxy.h>
#include <common/gp_math/gp_mathlib.h>
#include <uITC/uITC_msg.h>
/*-----------------------------------macro------------------------------------*/
#define SWITCH_DEBOUNCE_TIME_MS  200
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
static void RCs_publish_vehicle_rc();
/*----------------------------------variable----------------------------------*/
RCs_HandleTypeDef Rcs;
RCs_HandleTypeDef *pRcs;

static uitc_actuator_rc actuator_rc;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       读取遥控器通道值
  * @param[in]   pRc  
  * @param[out]  
  * @retval      
  * @note        
  */
bool RC_update(RC_HandleTypeDef *pRc, int16_t ChanVal)
{
    RCs_HandleTypeDef *pRCs;
    pRCs = rc_container_of(pRc, RCs_HandleTypeDef, channel[pRc->ch_in]);

    if (RC_has_override(pRc) && !RCs_ignore_receiver()) {
        pRc->radio_in = pRc->override_value;
    } else if (RCs_has_had_rc_receiver() && !RCs_ignore_receiver()) {
        pRc->radio_in = ChanVal;
    } else {
        return false;
    }

    if (pRc->type_in == RC_CHANNEL_TYPE_RANGE) {
        pRc->control_in = RC_pwm_to_range(pRc);
    } else {
        //RC_CHANNEL_TYPE_ANGLE
        pRc->control_in = RC_pwm_to_angle(pRc);
    }
    
    return true;
}

bool RC_has_override(RC_HandleTypeDef *pRc)
{
    RCs_HandleTypeDef *pRCs;
    pRCs = rc_container_of(pRc, RCs_HandleTypeDef, channel[pRc->ch_in]);
    
    if (pRc->override_value == 0) {
        return false;
    }

    uint32_t override_timeout_ms;
    if (!RCs_get_override_timeout_ms(&override_timeout_ms)) {
        // timeouts are disabled
        return true;
    }

    if (override_timeout_ms == 0) {
        // overrides are explicitly disabled by a zero value
        return false;
    }

    return (time_millis() - pRc->last_override_time < override_timeout_ms);
}

/**
  * @brief       
  * @param[in]   pRc  
  * @param[in]   v  
  * @param[in]   timestamp_ms  
  * @param[out]  
  * @retval      
  * @note        
  */
void RC_set_override(RC_HandleTypeDef *pRc, const uint16_t v, const uint32_t timestamp_ms)
{
    RCs_HandleTypeDef *pRCs;
    pRCs = rc_container_of(pRc, RCs_HandleTypeDef, channel[pRc->ch_in]);

    if (!pRCs->gcs_overrides_enabled) {
        return;
    }

    pRc->last_override_time = timestamp_ms != 0 ? timestamp_ms : time_millis();
    pRc->override_value = v;
    pRCs->has_new_overrides = true;
}

// read_3pos_switch
bool RC_read_3pos_switch(RC_HandleTypeDef *pRc, RC_AuxSwitchPos *ret)
{
    RCs_HandleTypeDef *pRCs;
    pRCs = rc_container_of(pRc, RCs_HandleTypeDef, channel[pRc->ch_in]);

    const uint16_t in = pRc->radio_in;
    if (in <= 900 || in >= 2200) {
        return false;
    }
    
    // switch is reversed if 'reversed' option set on channel and switches reverse is allowed by RC_OPTIONS
    bool switch_reversed = pRc->_reversed && RCs_switch_reverse_allowed();
    
    if (in < AUX_PWM_TRIGGER_LOW) {
        *ret = switch_reversed ? RC_HIGH : RC_LOW;
    } else if (in > AUX_PWM_TRIGGER_HIGH) {
        *ret = switch_reversed ? RC_LOW : RC_HIGH;
    } else {
        *ret = RC_MIDDLE;
    }
    return true;
}

// 
RC_AuxSwitchPos  RC_get_aux_switch_pos(RC_HandleTypeDef *pRc)
{
    RC_AuxSwitchPos position = RC_LOW;
    (void)RC_read_3pos_switch(pRc, &position);

    return position;
}

// 
bool RC_debounce_completed(RC_HandleTypeDef *pRc, int8_t position)
{
    // switch change not detected
    if (pRc->switch_state.current_position == position) {
        // reset debouncing
        pRc->switch_state.debounce_position = position;
    } else {
        // switch change detected
        const uint32_t tnow_ms = time_millis();

        // position not established yet
        if (pRc->switch_state.debounce_position != position) {
            pRc->switch_state.debounce_position = position;
            pRc->switch_state.last_edge_time_ms = tnow_ms;
        } else if (tnow_ms - pRc->switch_state.last_edge_time_ms >= SWITCH_DEBOUNCE_TIME_MS) {
            // position estabilished; debounce completed
            pRc->switch_state.current_position = position;
            return true;
        }
    }

    return false;
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void RCs_init()
{
    pRcs = &Rcs;

    pRcs->gcs_overrides_enabled = true;

    RCs_AssignParam();

    pRcs->_override_timeout = 3.0f;
    pRcs->_options          = RC_ARMING_CHECK_THROTTLE;
    // @Bitmask: 0:All,1:PPM,2:IBUS,3:SBUS,4:SBUS_NI,5:DSM,6:SUMD,7:SRXL,8:SRXL2,9:CRSF,10:ST24,11:FPORT,12:FPORT2,13:FastSBUS
    pRcs->_protocols        = 1;

    // setup ch_in on channels
    for (uint8_t i=0; i<RC_INPUT_MAX_CHANNELS; i++) {
        pRcs->channel[i].ch_in = i;
        RC_init(&(pRcs->channel[i]));
    }

    RCs_init_aux_all();
}

/**
  * @brief       
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
bool RCs_read_input()
{
    if (rc_hal_new_input()) {
        pRcs->has_had_rc_receiver = true;
    } else if (!pRcs->has_new_overrides) {
        return false;
    }

    pRcs->has_new_overrides = false;

    pRcs->last_update_ms = time_millis();

    bool success = false;
    for (uint8_t i=0; i<RC_INPUT_MAX_CHANNELS; i++) {
        success |= RC_update(&(pRcs->channel[i]), (int16_t)rc_hal_read(pRcs->channel[i].ch_in));
    }

    RCs_publish_vehicle_rc();

    return success;
}

/**
  * @brief       更新参数
  * @param[in]   pRcs  
  * @param[out]  
  * @retval      
  * @note        
  */
void RCs_AssignParam()
{
    RC_HandleTypeDef *pRC = pRcs->channel;

    param_link_variable(PARAM_ID(RC, RC1_MIN), &pRC[0]._radio_min);
    param_link_variable(PARAM_ID(RC, RC1_MAX), &pRC[0]._radio_max);
    param_link_variable(PARAM_ID(RC, RC1_TRIM), &pRC[0]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC1_DZ), &pRC[0]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC1_OPTION), &pRC[0]._option);

    param_link_variable(PARAM_ID(RC, RC2_MIN), &pRC[1]._radio_min);
    param_link_variable(PARAM_ID(RC, RC2_MAX), &pRC[1]._radio_max);
    param_link_variable(PARAM_ID(RC, RC2_TRIM), &pRC[1]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC2_DZ), &pRC[1]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC2_OPTION), &pRC[1]._option);

    param_link_variable(PARAM_ID(RC, RC3_MIN), &pRC[2]._radio_min);
    param_link_variable(PARAM_ID(RC, RC3_MAX), &pRC[2]._radio_max);
    param_link_variable(PARAM_ID(RC, RC3_TRIM), &pRC[2]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC3_DZ), &pRC[2]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC3_OPTION), &pRC[2]._option);

    param_link_variable(PARAM_ID(RC, RC4_MIN), &pRC[3]._radio_min);
    param_link_variable(PARAM_ID(RC, RC4_MAX), &pRC[3]._radio_max);
    param_link_variable(PARAM_ID(RC, RC4_TRIM), &pRC[3]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC4_DZ), &pRC[3]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC4_OPTION), &pRC[3]._option);

    param_link_variable(PARAM_ID(RC, RC5_MIN), &pRC[4]._radio_min);
    param_link_variable(PARAM_ID(RC, RC5_MAX), &pRC[4]._radio_max);
    param_link_variable(PARAM_ID(RC, RC5_TRIM), &pRC[4]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC5_DZ), &pRC[4]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC5_OPTION), &pRC[4]._option);

    param_link_variable(PARAM_ID(RC, RC6_MIN), &pRC[5]._radio_min);
    param_link_variable(PARAM_ID(RC, RC6_MAX), &pRC[5]._radio_max);
    param_link_variable(PARAM_ID(RC, RC6_TRIM), &pRC[5]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC6_DZ), &pRC[5]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC6_OPTION), &pRC[5]._option);

    param_link_variable(PARAM_ID(RC, RC7_MIN), &pRC[6]._radio_min);
    param_link_variable(PARAM_ID(RC, RC7_MAX), &pRC[6]._radio_max);
    param_link_variable(PARAM_ID(RC, RC7_TRIM), &pRC[6]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC7_DZ), &pRC[6]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC7_OPTION), &pRC[6]._option);

    param_link_variable(PARAM_ID(RC, RC8_MIN), &pRC[7]._radio_min);
    param_link_variable(PARAM_ID(RC, RC8_MAX), &pRC[7]._radio_max);
    param_link_variable(PARAM_ID(RC, RC8_TRIM), &pRC[7]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC8_DZ), &pRC[7]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC8_OPTION), &pRC[7]._option);

    param_link_variable(PARAM_ID(RC, RC9_MIN), &pRC[8]._radio_min);
    param_link_variable(PARAM_ID(RC, RC9_MAX), &pRC[8]._radio_max);
    param_link_variable(PARAM_ID(RC, RC9_TRIM), &pRC[8]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC9_DZ), &pRC[8]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC9_OPTION), &pRC[8]._option);

    param_link_variable(PARAM_ID(RC, RC10_MIN), &pRC[9]._radio_min);
    param_link_variable(PARAM_ID(RC, RC10_MAX), &pRC[9]._radio_max);
    param_link_variable(PARAM_ID(RC, RC10_TRIM), &pRC[9]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC10_DZ), &pRC[9]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC10_OPTION), &pRC[9]._option);

    param_link_variable(PARAM_ID(RC, RC11_MIN), &pRC[10]._radio_min);
    param_link_variable(PARAM_ID(RC, RC11_MAX), &pRC[10]._radio_max);
    param_link_variable(PARAM_ID(RC, RC11_TRIM), &pRC[10]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC11_DZ), &pRC[10]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC11_OPTION), &pRC[10]._option);

    param_link_variable(PARAM_ID(RC, RC12_MIN), &pRC[11]._radio_min);
    param_link_variable(PARAM_ID(RC, RC12_MAX), &pRC[11]._radio_max);
    param_link_variable(PARAM_ID(RC, RC12_TRIM), &pRC[11]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC12_DZ), &pRC[11]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC12_OPTION), &pRC[11]._option);

    param_link_variable(PARAM_ID(RC, RC13_MIN), &pRC[12]._radio_min);
    param_link_variable(PARAM_ID(RC, RC13_MAX), &pRC[12]._radio_max);
    param_link_variable(PARAM_ID(RC, RC13_TRIM), &pRC[12]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC13_DZ), &pRC[12]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC13_OPTION), &pRC[12]._option);

    param_link_variable(PARAM_ID(RC, RC14_MIN), &pRC[13]._radio_min);
    param_link_variable(PARAM_ID(RC, RC14_MAX), &pRC[13]._radio_max);
    param_link_variable(PARAM_ID(RC, RC14_TRIM), &pRC[13]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC14_DZ), &pRC[13]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC14_OPTION), &pRC[13]._option);

    param_link_variable(PARAM_ID(RC, RC15_MIN), &pRC[14]._radio_min);
    param_link_variable(PARAM_ID(RC, RC15_MAX), &pRC[14]._radio_max);
    param_link_variable(PARAM_ID(RC, RC15_TRIM), &pRC[14]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC15_DZ), &pRC[14]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC15_OPTION), &pRC[14]._option);

    param_link_variable(PARAM_ID(RC, RC16_MIN), &pRC[15]._radio_min);
    param_link_variable(PARAM_ID(RC, RC16_MAX), &pRC[15]._radio_max);
    param_link_variable(PARAM_ID(RC, RC16_TRIM), &pRC[15]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC16_DZ), &pRC[15]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC16_OPTION), &pRC[15]._option);

    param_link_variable(PARAM_ID(RC, RC17_MIN), &pRC[16]._radio_min);
    param_link_variable(PARAM_ID(RC, RC17_MAX), &pRC[16]._radio_max);
    param_link_variable(PARAM_ID(RC, RC17_TRIM), &pRC[16]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC17_DZ), &pRC[16]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC17_OPTION), &pRC[16]._option);

    param_link_variable(PARAM_ID(RC, RC18_MIN), &pRC[17]._radio_min);
    param_link_variable(PARAM_ID(RC, RC18_MAX), &pRC[17]._radio_max);
    param_link_variable(PARAM_ID(RC, RC18_TRIM), &pRC[17]._radio_trim);
    param_link_variable(PARAM_ID(RC, RC18_DZ), &pRC[17]._dead_zone);
    param_link_variable(PARAM_ID(RC, RC18_OPTION), &pRC[17]._option);
}

/**
  * @brief       返回切换飞行模式的通道
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
uint8_t RCs_flight_mode_channel_number()
{
    return PARAM_GET_UINT8(FLIGHTMODE, FLTMODE_CH);
}

//
bool RCs_has_valid_input()
{
    if (fms.failsafe.radio) {
        return false;
    }
    if (fms.failsafe.radio_counter != 0) {
        return false;
    }
    return true;
}

uint8_t RCs_get_valid_channel_count(void)
{
    return MIN(RC_INPUT_MAX_CHANNELS, rc_hal_num_channels());
}

static void RCs_publish_vehicle_rc()
{
    uint8_t channel_count = 0;

    for (uint8_t i=0; i<RC_INPUT_MAX_CHANNELS; i++) {
        if (RC_has_override(&(pRcs->channel[i])) && !RCs_ignore_receiver()) {
            actuator_rc.channels[i] = pRcs->channel[i].radio_in;
            channel_count ++;
        } else if (RCs_has_had_rc_receiver() && !RCs_ignore_receiver()) {
            actuator_rc.channels[i]  = pRcs->channel[i].radio_in;
        }
    }

    if (RCs_has_active_overrides() && !RCs_ignore_receiver()) {
        actuator_rc.timestamp_us  = (uint64_t)pRcs->last_update_ms * 1000;
        actuator_rc.rssi          = 50;
        actuator_rc.rc_protocol   = "gcs";
        actuator_rc.channel_count = channel_count;
    } else if (RCs_has_had_rc_receiver() && !RCs_ignore_receiver()) {
        actuator_rc.timestamp_us = rc_hal._rcin_timestamp_last_signal;
        actuator_rc.rssi         = rc_hal._rssi;
        actuator_rc.rc_protocol  = rc_hal.last_protocol;
        actuator_rc.channel_count = MIN(RC_INPUT_MAX_CHANNELS, rc_hal._num_channels);
    }

    itc_publish(ITC_ID(vehicle_rc), &actuator_rc);
}

/*------------------------------------test------------------------------------*/


