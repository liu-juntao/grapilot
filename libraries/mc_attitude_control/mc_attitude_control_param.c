
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       mc_attitude_control_param.c
  * @author     baiyang
  * @date       2021-10-3
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "mc_attitude_control_multi.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       更新姿态控制器相关参数
  * @param[in]   pAttCtrlMulti  
  * @param[out]  
  * @retval      
  * @note        
  */
void attctrl_assign_param(Attitude_ctrl * att_ctrl)
{
    AttitudeMulti_ctrl* att_ctrl_multi = (AttitudeMulti_ctrl*)att_ctrl;

    //横滚角控制P控制器参数赋值
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ANG_RLL_P), &att_ctrl->_p_angle_roll._kp);

    //俯仰角控制P控制器参数赋值
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ANG_PIT_P), &att_ctrl->_p_angle_pitch._kp);

    //偏航角控制P控制器参数赋值
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ANG_YAW_P), &att_ctrl->_p_angle_yaw._kp);

    //横滚角速率控制PID控制器参数赋值
    PID_ctrl* pCtrlRPID = attctrl_get_rate_roll_pid(att_ctrl);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_P), &pCtrlRPID->_kp);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_I), &pCtrlRPID->_ki);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_D), &pCtrlRPID->_kd);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_IMAX), &pCtrlRPID->_kimax);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_FLTT), &pCtrlRPID->_filt_t_hz);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_FLTE), &pCtrlRPID->_filt_e_hz);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_RLL_FLTD), &pCtrlRPID->_filt_d_hz);

    //俯仰角速率控制PID控制器参数赋值
    PID_ctrl* pCtrlPPID = attctrl_get_rate_pitch_pid(att_ctrl);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_P), &pCtrlPPID->_kp);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_I), &pCtrlPPID->_ki);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_D), &pCtrlRPID->_kd);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_IMAX), &pCtrlPPID->_kimax);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_FLTT), &pCtrlPPID->_filt_t_hz);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_FLTE), &pCtrlPPID->_filt_e_hz);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_PIT_FLTD), &pCtrlPPID->_filt_d_hz);

    //偏航角速率控制PID控制器参数赋值
    PID_ctrl* pCtrlYPID = attctrl_get_rate_yaw_pid(att_ctrl);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_P), &pCtrlYPID->_kp);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_I), &pCtrlYPID->_ki);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_D), &pCtrlYPID->_kd);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_IMAX), &pCtrlYPID->_kimax);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_FLTT), &pCtrlYPID->_filt_t_hz);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_FLTE), &pCtrlYPID->_filt_e_hz);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RAT_YAW_FLTD), &pCtrlYPID->_filt_d_hz);

    //设置油门和姿态控制优先级参数
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_THR_MIX_MIN), &att_ctrl_multi->_thr_mix_min);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_THR_MIX_MAN), &att_ctrl_multi->_thr_mix_man);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_THR_MIX_MAX), &att_ctrl_multi->_thr_mix_max);

    // 设置姿态角加速度极限值
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ACCEL_R_MAX), &att_ctrl->_accel_roll_max);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ACCEL_P_MAX), &att_ctrl->_accel_pitch_max);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ACCEL_Y_MAX), &att_ctrl->_accel_yaw_max);

    // 设置姿态角速度限制值参数
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RATE_R_MAX), &att_ctrl->_ang_vel_roll_max);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RATE_P_MAX), &att_ctrl->_ang_vel_pitch_max);
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RATE_Y_MAX), &att_ctrl->_ang_vel_yaw_max);

    // 设置角速度前馈参数
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_RATE_FF_EN), &att_ctrl->_rate_bf_ff_enabled);

    // 设置角度补偿油门使能参数
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ANGLE_BOOST), &att_ctrl->_angle_boost_enabled);

    // 设置速率控制器输入平滑时间常数参数
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_INPUT_TC), &att_ctrl->_input_tc);

    // 设置速率控制器输入平滑时间常数参数
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_ANG_LIM_TC), &att_ctrl->_angle_limit_tc);

    //可以在Loiter，RTL，自动飞行模式下更新偏航目标的最大速率
    param_link_variable(PARAM_ID(ATT_CTRL, ATC_SLEW_YAW), &att_ctrl->_slew_yaw);

    // 最大角度限制，度*100
    // att_ctrl->_angle_max不会实时更新
    att_ctrl->_angle_max = PARAM_GET_INT16(VEHICLE, ANGLE_MAX);
}

/*------------------------------------test------------------------------------*/


