
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       dev_mgr_i2c.h
  * @author     baiyang
  * @date       2021-11-28
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "dev_mgr.h"
/*-----------------------------------macro------------------------------------*/
/*
  convenient macros for iterating over I2C bus numbers
 */
#define FOREACH_I2C_MASK(i,mask) for (uint32_t _bmask=mask, i=0; i<32; i++) if ((1U<<i)&_bmask)
#define FOREACH_I2C_EXTERNAL(i) FOREACH_I2C_MASK(i,devmgr_get_bus_mask_external())
#define FOREACH_I2C_INTERNAL(i) FOREACH_I2C_MASK(i,devmgr_get_bus_mask_internal())
#define FOREACH_I2C(i) FOREACH_I2C_MASK(i,devmgr_get_bus_mask())
/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void devmgr_i2c_config(gp_device_t dev, uint8_t bus, uint8_t address);

uint32_t devmgr_get_bus_mask(void);
uint32_t devmgr_get_bus_mask_internal(void);
uint32_t devmgr_get_bus_mask_external(void);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



