
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       dev_mgr_i2c.c
  * @author     baiyang
  * @date       2021-11-28
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <stdio.h>
#include <string.h>

#include "dev_mgr_i2c.h"

#include <rtthread.h>
#include <rtdevice.h>

#include <common/gp_math/gp_mathlib.h>
/*-----------------------------------macro------------------------------------*/
#ifndef HAL_I2C_BUS_BASE
#define HAL_I2C_BUS_BASE 0
#endif
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
static bool _read_registers(gp_device_t dev, uint8_t first_reg, uint8_t *recv, uint32_t recv_len);
static bool _write_register(gp_device_t dev, uint8_t reg, uint8_t val, bool checked);
static bool _transfer(gp_device_t dev, const uint8_t *send, uint32_t send_len, uint8_t *recv, uint32_t recv_len);
static bool _take_bus(gp_device_t dev);
static bool _release_bus(gp_device_t dev);
/*----------------------------------variable----------------------------------*/
static struct device_bus i2c_bus[4];    //

static struct devmgr_device_ops devmgr_i2c_ops = {.read_registers = _read_registers,
                                                  .write_register = _write_register,
                                                  .transfer       = _transfer,
                                                  .take_bus       = _take_bus,
                                                  .release_bus    = _release_bus};
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]   dev  
  * @param[out]  
  * @retval      
  * @note        
  */
void devmgr_i2c_config(gp_device_t dev, uint8_t bus, uint8_t address)
{
    RT_ASSERT(dev != RT_NULL);

    if (bus > ARRAY_SIZE(i2c_bus)) {
        return;
    }

    dev->ops = &devmgr_i2c_ops;

    dev->bus = &(i2c_bus[bus]);
    dev->bus->bus_id = bus + 1;

    dev->d.devid_s.bus_type = BUS_TYPE_I2C;
    dev->d.devid_s.bus = dev->bus->bus_id;

    dev->_retries = 2;
    dev->_address = address;
    dev->_use_smbus  = false;
    dev->_timeout_ms = 4;
}

/*
  get mask of bus numbers for all configured I2C buses
*/
uint32_t devmgr_get_bus_mask(void)
{
    return ((1U << ARRAY_SIZE(i2c_bus)) - 1) << HAL_I2C_BUS_BASE;
}

/*
  get mask of bus numbers for all configured internal I2C buses
*/
uint32_t devmgr_get_bus_mask_internal(void)
{
    // assume first bus is internal
    return devmgr_get_bus_mask() & HAL_I2C_INTERNAL_MASK;
}

/*
  get mask of bus numbers for all configured external I2C buses
*/
uint32_t devmgr_get_bus_mask_external(void)
{
    // assume first bus is internal
    return devmgr_get_bus_mask() & ~HAL_I2C_INTERNAL_MASK;
}

/**
 * Wrapper function over #transfer() to read recv_len registers, starting
 * by first_reg, into the array pointed by recv. The read flag passed to
 * #set_read_flag(uint8_t) is ORed with first_reg before performing the
 * transfer.
 *
 * Return: true on a successful transfer, false on failure.
 */
static bool _read_registers(gp_device_t dev, uint8_t first_reg, uint8_t *recv, uint32_t recv_len)
{
    rt_size_t len = 0;

    for(uint8_t i=0 ; i <= dev->_retries; i++) {
        len = rt_device_write(dev->dev, dev->_address, (void*)&first_reg, 1);

        if(len != 1) {
            continue;
        }

        len = rt_device_read(dev->dev, dev->_address, (void*)recv, recv_len);
        if (len == recv_len) {
            return true;
        }
    }

    return false;
}

/**
 * Wrapper function over #transfer() to write a byte to the register reg.
 * The transfer is done by sending reg and val in that order.
 *
 * Return: true on a successful transfer, false on failure.
 */
static bool _write_register(gp_device_t dev, uint8_t reg, uint8_t val, bool checked)
{
    rt_size_t len = 0;
    uint8_t buf[2] = { reg, val };

    if (checked) {
        devmgr_set_checked_register(dev, reg, val);
    }

    for(uint8_t i=0 ; i <= dev->_retries; i++) {
        len = rt_device_write(dev->dev, dev->_address, (void*)buf, sizeof(buf));

        if (len == sizeof(buf)) {
            return true;
        }
    }

    return false;
}

/**
  * @brief       
  * @param[in]   dev  
  * @param[in]   send  
  * @param[in]   send_len  
  * @param[in]   recv  
  * @param[in]   recv_len  
  * @param[out]  
  * @retval      
  * @note        
  */
static bool _transfer(gp_device_t dev, const uint8_t *send, uint32_t send_len, uint8_t *recv, uint32_t recv_len)
{
    rt_size_t len = 0;

    if (!recv && send) {
        for(uint8_t i=0 ; i <= dev->_retries; i++) {
            len = rt_device_write(dev->dev, dev->_address, (void*)send, send_len);

            if (len == send_len) {
                return true;
            }
        }
    }

    return false;
}

/**
  * @brief       
  * @param[in]   dev  
  * @param[out]  
  * @retval      
  * @note        
  */
static bool _take_bus(gp_device_t dev)
{
    struct rt_i2c_bus_device *bus = (struct rt_i2c_bus_device *)dev->dev->user_data;
    rt_err_t res = rt_mutex_take(&bus->lock, RT_WAITING_FOREVER);

    return res == RT_EOK;
}

/**
  * @brief       
  * @param[in]   dev  
  * @param[out]  
  * @retval      
  * @note        
  */
static bool _release_bus(gp_device_t dev)
{
    struct rt_i2c_bus_device *bus = (struct rt_i2c_bus_device *)dev->dev->user_data;
    rt_err_t res = rt_mutex_release(&bus->lock);

    return res == RT_EOK;
}

/*------------------------------------test------------------------------------*/


