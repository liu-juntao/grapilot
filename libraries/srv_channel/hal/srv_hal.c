
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       srv_hal.c
  * @author     baiyang
  * @date       2021-12-27
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "srv_hal.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
static rt_err_t hal_srv_init(struct rt_device* dev)
{
    RT_ASSERT(dev != RT_NULL);

    srv_dev_t srvt = (srv_dev_t)dev;

    /* apply default configuration */
    if (srvt->ops->dev_init) {
        return srvt->ops->dev_init(srvt);
    }

    return RT_EOK;
}

static rt_err_t hal_srv_control(struct rt_device* dev, int cmd, void* args)
{
    RT_ASSERT(dev != RT_NULL);

    srv_dev_t srvt = (srv_dev_t)dev;

    switch (cmd) {
    default:
        if (srvt->ops->dev_control) {
            return srvt->ops->dev_control(srvt, cmd, args);
        }
    }

    return RT_EOK;
}

static rt_size_t hal_srv_read(struct rt_device* dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    RT_ASSERT(dev != RT_NULL);

    srv_dev_t srvt = (srv_dev_t)dev;
    rt_size_t rb = 0;

    if (buffer == NULL || pos == 0) {
        return 0;
    }

    if (srvt->ops->dev_read) {
        rb = srvt->ops->dev_read(srvt, pos, buffer, size);
    }

    return rb;
}

static rt_size_t hal_srv_write(rt_device_t dev, rt_off_t pos, const void* buffer, rt_size_t size)
{
    RT_ASSERT(dev != RT_NULL);

    rt_size_t wb = 0;
    srv_dev_t srvt = (srv_dev_t)dev;
    uint16_t val = *((uint16_t*)buffer);


    if (srvt->ops->dev_write) {
        wb = srvt->ops->dev_write(srvt, (uint8_t)pos, val, size);
    }

    return wb;
}

rt_err_t hal_srv_register(srv_dev_t dev, const char* name, rt_uint32_t flag, void* data)
{
    RT_ASSERT(dev != RT_NULL);

    struct rt_device* device;

    device = &(dev->parent);

    device->type = RT_Device_Class_Miscellaneous;
    device->ref_count = 0;
    device->rx_indicate = RT_NULL;
    device->tx_complete = RT_NULL;

    device->init = hal_srv_init;
    device->open = RT_NULL;
    device->close = RT_NULL;
    device->read = hal_srv_read;
    device->write = hal_srv_write;
    device->control = hal_srv_control;
    device->user_data = data;

    /* register device */
    return rt_device_register(device, name, flag);
}

/*------------------------------------test------------------------------------*/


