
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       srv_hal_iomcu.c
  * @author     baiyang
  * @date       2021-12-27
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "srv_hal.h"

#if HAL_WITH_IO_MCU

#include "srv_hal_iomcu.h"
#include "iomcu/gp_iomcu.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
rt_err_t srv_iomcu_init(srv_dev_t dev);
static rt_err_t srv_iomcu_control(srv_dev_t dev, int cmd, void* arg);
static rt_size_t srv_iomcu_read(srv_dev_t dev, uint8_t chan, rt_uint16_t* chan_val, rt_size_t size);
static rt_size_t srv_iomcu_write(srv_dev_t dev, uint8_t chan, uint16_t pwm, rt_size_t size);
/*----------------------------------variable----------------------------------*/
static struct srv_device iomcu_dev;

static struct srv_ops ops = {.dev_init = srv_iomcu_init,
                             .dev_control = srv_iomcu_control,
                             .dev_read    = srv_iomcu_read,
                             .dev_write   = srv_iomcu_write};
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
rt_err_t srv_iomcu_init(srv_dev_t dev)
{
    rt_err_t result = RT_ERROR;

    iomcu_ctor();

    if (iomcu_init()) {
        result = RT_EOK;
    }

    return result;
}

static rt_err_t srv_iomcu_control(srv_dev_t dev, int cmd, void* arg)
{
    srv_dev_t srvt = (srv_dev_t)dev;

    switch (cmd) {
    case SRV_SET_FREQ:
        iomcu_set_freq(((struct pwm_drv_configure *)arg)->chan_mask, ((struct pwm_drv_configure *)arg)->pwm_freq);
        break;
    case SRV_GET_FREQ:
        ((struct pwm_drv_freq *)arg)->freq = iomcu_get_freq(((struct pwm_drv_freq *)arg)->chan);
        break;
    case SRV_CORK:
        iomcu_cork();
        break;
    case SRV_PUSH:
        iomcu_push();
        break;
    default:
        break;
    }

    return RT_EOK;
}

static rt_size_t srv_iomcu_read(srv_dev_t dev, uint8_t chan, rt_uint16_t* chan_val, rt_size_t size)
{
    *chan_val = iomcu_read_channel(chan);
    return size;
}

static rt_size_t srv_iomcu_write(srv_dev_t dev, uint8_t chan, uint16_t pwm, rt_size_t size)
{
    iomcu_write_channel(chan, pwm);
    return size;
}

void srv_iomcu_ctor()
{
    iomcu_dev.ops = &ops;

    hal_srv_register(&iomcu_dev, "srv_io", RT_DEVICE_OFLAG_RDWR, NULL);
}

/*------------------------------------test------------------------------------*/

#endif

