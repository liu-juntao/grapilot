
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       LowPassFilter2p.h
  * @author     baiyang
  * @date       2021-11-7
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <gp_math/gp_mathlib.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
struct biquad_params{
    float cutoff_freq;
    float sample_freq;
    float a1;
    float a2;
    float b0;
    float b1;
    float b2;
};

typedef struct {
    float _delay_element_1;
    float _delay_element_2;
    bool initialised;
} DigitalBiquadFilterFloat;

typedef struct {
    Vector2f_t _delay_element_1;
    Vector2f_t _delay_element_2;
    bool initialised;
} DigitalBiquadFilterVector2f;

typedef struct {
    Vector3f_t _delay_element_1;
    Vector3f_t _delay_element_2;
    bool initialised;
} DigitalBiquadFilterVector3f;

typedef struct {
    struct biquad_params _params;
    DigitalBiquadFilterFloat _filter;
} LowPassFilter2pFloat;

typedef struct {
    struct biquad_params _params;
    DigitalBiquadFilterVector2f _filter;
} LowPassFilter2pVector2f;

typedef struct {
    struct biquad_params _params;
    DigitalBiquadFilterVector3f _filter;
} LowPassFilter2pVector3f;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void DigitalBiquadFilterFloatCtor(DigitalBiquadFilterFloat *filter);
float DigitalBiquadFilterFloat_apply(DigitalBiquadFilterFloat *filter, float sample, const struct biquad_params *params);
void DigitalBiquadFilterFloat_reset(DigitalBiquadFilterFloat *filter);
void DigitalBiquadFilterFloat_reset2(DigitalBiquadFilterFloat *filter, float value, const struct biquad_params *params);
void DigitalBiquadFilterFloat_compute_params(DigitalBiquadFilterFloat *filter, float sample_freq, float cutoff_freq, struct biquad_params *ret);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



