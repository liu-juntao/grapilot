
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       derivative_filter.h
  * @author     baiyang
  * @date       2021-11-7
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>
/*-----------------------------------macro------------------------------------*/
#define DERIVTIVE_FILTER_SIZE_5    5
#define DERIVTIVE_FILTER_SIZE_6    6
#define DERIVTIVE_FILTER_SIZE_7    7
/*----------------------------------typedef-----------------------------------*/
typedef struct {
    bool            _new_data;
    float           _last_slope;

    // microsecond timestamps for samples. This is needed
    // to cope with non-uniform time spacing of the data
    uint32_t        _timestamps[DERIVTIVE_FILTER_SIZE_5];

    float           samples[DERIVTIVE_FILTER_SIZE_5];       // buffer of samples
    uint8_t         sample_index;               // pointer to the next empty slot in the buffer
} DerivativeFilterFloat_Size5;

typedef struct {
    bool            _new_data;
    float           _last_slope;

    // microsecond timestamps for samples. This is needed
    // to cope with non-uniform time spacing of the data
    uint32_t        _timestamps[DERIVTIVE_FILTER_SIZE_6];

    float           samples[DERIVTIVE_FILTER_SIZE_6];       // buffer of samples
    uint8_t         sample_index;               // pointer to the next empty slot in the buffer
} DerivativeFilterFloat_Size6;

typedef struct {
    bool            _new_data;
    float           _last_slope;

    // microsecond timestamps for samples. This is needed
    // to cope with non-uniform time spacing of the data
    uint32_t        _timestamps[DERIVTIVE_FILTER_SIZE_7];

    float           samples[DERIVTIVE_FILTER_SIZE_7];       // buffer of samples
    uint8_t         sample_index;               // pointer to the next empty slot in the buffer
} DerivativeFilterFloat_Size7;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/// constructor
void derivative_filter5(DerivativeFilterFloat_Size5 *filter);

// update - Add a new raw value to the filter, but don't recalculate
void derivative_filter5_update(DerivativeFilterFloat_Size5 *filter, float sample, uint32_t timestamp);

// apply - take in a new raw sample, and return the filtered results
float derivative_filter5_apply(DerivativeFilterFloat_Size5 *filter, float sample);

// return the derivative value
float derivative_filter5_slope(DerivativeFilterFloat_Size5 *filter);

// reset - clear the filter
void derivative_filter5_reset(DerivativeFilterFloat_Size5 *filter);


/// constructor
void derivative_filter6(DerivativeFilterFloat_Size6 *filter);

// update - Add a new raw value to the filter, but don't recalculate
void derivative_filter6_update(DerivativeFilterFloat_Size6 *filter, float sample, uint32_t timestamp);

// apply - take in a new raw sample, and return the filtered results
float derivative_filter6_apply(DerivativeFilterFloat_Size6 *filter, float sample);

// return the derivative value
float derivative_filter6_slope(DerivativeFilterFloat_Size6 *filter);

// reset - clear the filter
void derivative_filter6_reset(DerivativeFilterFloat_Size6 *filter);


/// constructor
void derivative_filter7(DerivativeFilterFloat_Size7 *filter);

// update - Add a new raw value to the filter, but don't recalculate
void derivative_filter7_update(DerivativeFilterFloat_Size7 *filter, float sample, uint32_t timestamp);

// apply - take in a new raw sample, and return the filtered results
float derivative_filter7_apply(DerivativeFilterFloat_Size7 *filter, float sample);

// return the derivative value
float derivative_filter7_slope(DerivativeFilterFloat_Size7 *filter);

// reset - clear the filter
void derivative_filter7_reset(DerivativeFilterFloat_Size7 *filter);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



