
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       butter.c
  * @author     baiyang
  * @date       2021-7-7
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <math.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

#include "butter.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void butter2_set_cutoff_frequency(Butter2 *butter, float sample_freq, float cutoff_freq)
{
    butter->_cutoff_freq = cutoff_freq;
    if (butter->_cutoff_freq <= 0.0f) {
        // no filtering
        return;
    }
    float fr = sample_freq/butter->_cutoff_freq;
    float ohm = tanf(M_PI/fr);
    float c = 1.0f+2.0f*cosf(M_PI/4.0f)*ohm + ohm*ohm;
    butter->_b0 = ohm*ohm/c;
    butter->_b1 = 2.0f*butter->_b0;
    butter->_b2 = butter->_b0;
    butter->_a1 = 2.0f*(ohm*ohm-1.0f)/c;
    butter->_a2 = (1.0f-2.0f*cosf(M_PI/4.0f)*ohm+ohm*ohm)/c;
    
    butter->_delay_element_1 = butter->_delay_element_2 = 0.0f;
}

float butter2_filter_process(Butter2 *butter, float sample)
{
    if (butter->_cutoff_freq <= 0.0f) {
        // no filtering
        return sample;
    }

    // do the filtering
    float delay_element_0 = sample - butter->_delay_element_1 * butter->_a1 - butter->_delay_element_2 * butter->_a2;

    float output = delay_element_0 * butter->_b0 + butter->_delay_element_1 * butter->_b1 + 
                    butter->_delay_element_2 * butter->_b2;
    
    butter->_delay_element_2 = butter->_delay_element_1;
    butter->_delay_element_1 = delay_element_0;

    // return the value.  Should be no need to check limits
    return output;
}

float butter2_reset(Butter2 *butter, float sample)
{
    float dval = sample / (butter->_b0 + butter->_b1 + butter->_b2);
    butter->_delay_element_1 = dval;
    butter->_delay_element_2 = dval;
    
    return butter2_filter_process(butter, sample);
}

Butter3* butter3_filter_create(float b[4], float a[4])
{
    Butter3* butter = (Butter3*)malloc(sizeof(Butter3));
    if(butter == NULL){
        //Console.print("butter create fail, malloc fail\n");
        return NULL;
    }
    for(uint8_t i = 0 ; i < 4 ; i++){
        butter->B[i] = b[i];
        butter->A[i] = a[i];
        butter->X[i] = butter->Y[i] = 0.0f;
    }

    return butter;
}

float butter3_filter_process(float in, Butter3* butter)
{
    float out;
    butter->X[3] = in;
    /* a(1)*y(n) = b(1)*x(n) + b(2)*x(n-1) + ... + b(nb+1)*x(n-nb)
                         - a(2)*y(n-1) - ... - a(na+1)*y(n-na)  */
    butter->Y[3] = butter->B[0]*butter->X[3] + butter->B[1]*butter->X[2] + butter->B[2]*butter->X[1]
        + butter->B[3]*butter->X[0] - butter->A[1]*butter->Y[2] - butter->A[2]*butter->Y[1] - butter->A[3]*butter->Y[0];

    /* we assume a(1)=1 */
    out = butter->Y[3];

    /* move X and Y */
    butter->X[0] = butter->X[1];
    butter->X[1] = butter->X[2];
    butter->X[2] = butter->X[3];
    butter->Y[0] = butter->Y[1];
    butter->Y[1] = butter->Y[2];
    butter->Y[2] = butter->Y[3];

    return out;
}

/*------------------------------------test------------------------------------*/


