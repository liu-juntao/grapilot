
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       butter.h
  * @author     baiyang
  * @date       2021-7-7
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/

/*-----------------------------------macro------------------------------------*/
//extern const float PI;
#ifndef M_PI
    #define M_PI  3.14159265358979f
#endif
/*----------------------------------typedef-----------------------------------*/
typedef struct{
    float    _cutoff_freq; 
    float    _a1;
    float    _a2;
    float    _b0;
    float    _b1;
    float    _b2;
    float    _delay_element_1;        // buffered sample -1
    float    _delay_element_2;        // buffered sample -2
}Butter2;

typedef struct
{
    float A[4];
    float B[4];
    float X[4];
    float Y[4];
}Butter3;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void butter2_set_cutoff_frequency(Butter2 *butter, float sample_freq, float cutoff_freq);
float butter2_reset(Butter2 *butter, float sample);
float butter2_filter_process(Butter2 *butter, float sample);
Butter3* butter3_filter_create(float b[4], float a[4]);
float butter3_filter_process(float in, Butter3* butter);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



