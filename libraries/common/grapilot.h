
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       grapilot.h
  * @author     baiyang
  * @date       2021-7-26
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/

/*-----------------------------------macro------------------------------------*/
#define PERIOD_MS_1HZ      1000
#define PERIOD_MS_2HZ      500
#define PERIOD_MS_5HZ      200
#define PERIOD_MS_10HZ     100
#define PERIOD_MS_20HZ     50
#define PERIOD_MS_25HZ     40
#define PERIOD_MS_50HZ     20
#define PERIOD_MS_100HZ    10
#define PERIOD_MS_200HZ    5
#define PERIOD_MS_250HZ    4
#define PERIOD_MS_500HZ    2

#define THISFIRMWARE "grapilot V0.1.0-beta"

// the following line is parsed by the autotest scripts
#define FIRMWARE_VERSION 0,1,0

#define FW_MAJOR 0
#define FW_MINOR 1
#define FW_PATCH 0
/*----------------------------------typedef-----------------------------------*/
typedef enum {
    PRIORITY_BOOST = 2,
    PRIORITY_SPI = 5,
    PRIORITY_FMS,
    PRIORITY_AHRS,
    PRIORITY_I2C,
    PRIORITY_CAN,
    PRIORITY_RCOUT,
    PRIORITY_RCIN,
    PRIORITY_IO,
    PRIORITY_UART = 21,
    PRIORITY_MMS,
    PRIORITY_COMM,
    PRIORITY_STORAGE,
    PRIORITY_LOGGER,
    PRIORITY_SCRIPTING,
} priority_base;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



