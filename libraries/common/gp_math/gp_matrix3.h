
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_matrix3.h
  * @author     baiyang
  * @date       2021-11-24
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "gp_math.h"
#include "gp_vector3.h"
/*-----------------------------------macro------------------------------------*/
#ifndef MATRIX_TYPE
#define MATRIX_TYPE        float
#endif
/*----------------------------------typedef-----------------------------------*/
typedef struct {
    // Vectors comprising the rows of the matrix
    Vector3f_t a, b, c;
} matrix3f_t;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void matrix3f_from_euler(matrix3f_t* mat, const float roll, const float pitch, const float yaw);
void matrix3f_zeros(matrix3f_t* mat);
void matrix3f_eye(matrix3f_t* mat);
void matrix3f_add(matrix3f_t* dst, const matrix3f_t* src1, const matrix3f_t* src2);
void matrix3f_sub(matrix3f_t* dst, const matrix3f_t* src1, const matrix3f_t* src2);
void matrix3f_mul(matrix3f_t* dst, const matrix3f_t* src1, const matrix3f_t* src2);
void matrix3f_mul_vec(Vector3f_t* dst, const matrix3f_t* m, const Vector3f_t* v);
void matrix3f_mul_transpose_vec(Vector3f_t* dst, const matrix3f_t* m, const Vector3f_t* v);
void matrix3f_trans(matrix3f_t* dst, const matrix3f_t* src);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



