
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_vector2.h
  * @author     baiyang
  * @date       2021-7-6
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <math.h>
#include <stdbool.h>
#include <float.h>
/*-----------------------------------macro------------------------------------*/
#define VECTOR2F_DEFAULT_VALUE {0.0f, 0.0f}
/*----------------------------------typedef-----------------------------------*/
typedef union
{
    struct 
    {
        float x;
        float y;
    };
    float vec2[2];
}Vector2f_t;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void vec2_set(Vector2f_t* vo, const float x, const float y);
void vec2_zero(Vector2f_t* vo);
float vec2_length(const Vector2f_t* v);
float vec2_length_squared(const Vector2f_t* v);
bool vec2_limit_length(Vector2f_t* v, float max_length);
void vec2_norm(Vector2f_t* vo, const Vector2f_t* vi);
Vector2f_t vec2_normalized(const Vector2f_t* v);
void vec2_add(Vector2f_t* vo, const Vector2f_t* v1, const Vector2f_t* v2);
void vec2_sub(Vector2f_t* vo, const Vector2f_t* v1, const Vector2f_t* v2);
float vec2_cross(const Vector2f_t* v1, const Vector2f_t* v2);
float vec2_dot(const Vector2f_t* v1, const Vector2f_t* v2);
void vec2_mult(Vector2f_t* vo, const Vector2f_t* vi, const float scalar);
bool vec2_is_nan(const Vector2f_t* v);
bool vec2_is_inf(const Vector2f_t* v);
float vec2_get_bearing_cd(const Vector2f_t *origin, const Vector2f_t *destination);

static inline void vec2_add_mult(Vector2f_t* vo, const Vector2f_t* v1, float scalar) {
    vo->x += v1->x * scalar;
    vo->y += v1->y * scalar;
}

static inline bool vec2_is_zero(const Vector2f_t* v) {
    return fabsf(v->x) < FLT_EPSILON && fabsf(v->x) < FLT_EPSILON;
}
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



