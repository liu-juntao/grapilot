
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_vector3.h
  * @author     baiyang
  * @date       2021-7-6
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <math.h>
#include "../gp_rotations.h"
/*-----------------------------------macro------------------------------------*/
#define VECTOR3F_DEFAULT_VALUE {0.0f, 0.0f, 0.0f}
/*----------------------------------typedef-----------------------------------*/
typedef union
{
    struct 
    {
        float x;
        float y;
        float z;
    };
    float vec3[3];
}Vector3f_t;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void vec3_set(Vector3f_t* vo, const float x, const float y, const float z);
void vec3_zero(Vector3f_t* vo);
bool vec3_is_zero(const Vector3f_t* vo);
float vec3_length(const Vector3f_t* v);
float vec3_length_xy(const Vector3f_t* v);
bool vec3_limit_length_xy(Vector3f_t* v, float max_length);
float vec3_length_squared(const Vector3f_t* v);
float vec3_length_squared_xy(const Vector3f_t* v);
void vec3_norm(Vector3f_t* vo, const Vector3f_t* vi);
void vec3_add(Vector3f_t* vo, const Vector3f_t* v1, const Vector3f_t* v2);
void vec3_add2(Vector3f_t* vo, const Vector3f_t* v1, const Vector3f_t* v2, const Vector3f_t* v3);
void vec3_sub(Vector3f_t* vo, const Vector3f_t* v1, const Vector3f_t* v2);
Vector3f_t vec3_sub2(const Vector3f_t* v1, const Vector3f_t* v2);
float vec3_sub_length(const Vector3f_t* v1, const Vector3f_t* v2);
void vec3_cross(Vector3f_t* vo, const Vector3f_t* vl, const Vector3f_t* vr);
float vec3_dot(const Vector3f_t* v1, const Vector3f_t* v2);
void vec3_mult(Vector3f_t* vo, const Vector3f_t* vi, const float scalar);
Vector3f_t vec3_mult2(const Vector3f_t* vi, const float scalar);
void vec3_mult_scalar_vec(Vector3f_t* vo, const Vector3f_t* vi, const Vector3f_t* scalar);
Vector3f_t vec3_mult_add(const Vector3f_t* vi, const float scalar, const Vector3f_t* vi2, const float scalar2);  //
void vec3_hadamard_product(Vector3f_t* vo, const Vector3f_t* vl, const Vector3f_t* vr);
float vec3_get_bearing_cd(const Vector3f_t *origin, const Vector3f_t *destination);
float vec3_angle_xy(const Vector3f_t *v);

// rotate by a standard rotation
void vec3_rotate(Vector3f_t *v, enum RotationEnum rotation);
void vec3_rotate_inverse(Vector3f_t *v, enum RotationEnum rotation);

bool vec3_is_nan(const Vector3f_t* v);
bool vec3_is_inf(const Vector3f_t* v);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



