
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_math.h
  * @author     baiyang
  * @date       2021-7-5
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "math_def.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
float math_asinf(const float val);

float math_sqrtf(float val);
float math_rsqrtf(float number);

float math_constrain_float(float amt, float low, float high);
double math_constrain_double(double amt, double low, double high);

double math_rand_normal(double mean, double stddev);

float math_linear_inter(float low_output, float high_output,
                         float var_value,
                         float var_low, float var_high);

float math_wrap_PI(const float radian);
float math_wrap_2PI(const float radian);

float math_wrap_360(const float angle);
float math_wrap_360_cd(float angle);

float math_wrap_180(float angle);
float math_wrap_180_cd(float angle);

float math_calc_lpf_alpha_dt(float dt, float cutoff_freq);

uint16_t math_get_random16(void);

float math_linear_interpolate(float low_output, float high_output, float var_value, float var_low, float var_high);

/** 
  * @brief       限幅
  * @param[in]   amt  
  * @param[in]   low  
  * @param[in]   high  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline uint32_t math_constrain_uint32(uint32_t amt, uint32_t low, uint32_t high) {
    return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

/** 
  * @brief       限幅
  * @param[in]   amt  
  * @param[in]   low  
  * @param[in]   high  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline int32_t math_constrain_int32(int32_t amt, int32_t low, int32_t high) {
    return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

/** 
  * @brief       限幅
  * @param[in]   amt  
  * @param[in]   low  
  * @param[in]   high  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline uint16_t math_constrain_uint16(uint16_t amt, uint16_t low, uint16_t high) {
    return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

/** 
  * @brief       限幅
  * @param[in]   amt  
  * @param[in]   low  
  * @param[in]   high  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline int16_t math_constrain_int16(int16_t amt, int16_t low, int16_t high) {
    return ((amt)<(low)?(low):((amt)>(high)?(high):(amt)));
}

/*
 * Variadic template for calculating the norm (pythagoras) of a vector of any
 * dimension.
 */
static inline float math_norm(const float first, const float second) { return math_sqrtf(first*first + second*second); }

/** 
  * @brief       判断float型是否为零
  * @param[in]   val  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_flt_zero(float val) { return fabsf(val) < FLT_EPSILON; }

/**
  * @brief       判断两个float型数据是否相等
  * @param[in]   val1  
  * @param[in]   val2  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_flt_equal(float val1, float val2) { return fabsf(val1-val2) < FLT_EPSILON; }

/** 
  * @brief       判断float型变量是否为负
  * @param[in]   val  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_flt_negative(float val) { return val <= (-1.0 * FLT_EPSILON); }

/** 
  * @brief       判断float型变量是否为正
  * @param[in]   val  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_flt_positive(float val) { return val >= FLT_EPSILON; }

/** 
  * @brief       判断double型是否为零
  * @param[in]   val  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_dbl_zero(double val) { return fabs(val) < DBL_EPSILON; }

/** 
  * @brief       判断double型是否为负
  * @param[in]   val  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_dbl_negative(double val) { return val <= (-1.0 * DBL_EPSILON); }

/** 
  * @brief       判断double型是否为正
  * @param[in]   val  
  * @param[out]  
  * @retval      
  * @note        
  */
static inline bool math_dbl_positive(double val) { return val >= DBL_EPSILON; }

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



