
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       notify_toshiba_led_i2c.h
  * @author     baiyang
  * @date       2022-1-17
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#include "notify_rgb_led.h"
#include <device_manager/dev_mgr.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
/**
  * @brief       
  * @param[out]  
  * @retval      
  * @note        
  */
typedef struct {
    struct rgb_led backend_rgb;

    gp_device_t _dev;

    bool _need_update;
    struct {
        uint8_t r, g, b;
    } rgb;
    uint8_t _bus;
} toshiba_led_i2c;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void toshiba_led_i2c_ctor(toshiba_led_i2c* toshiba_led, uint8_t bus);
notify_device_t toshiba_led_i2c_probe(uint8_t bus);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



