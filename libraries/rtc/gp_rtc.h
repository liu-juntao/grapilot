
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_rtc.h
  * @author     baiyang
  * @date       2022-1-25
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <time.h>

#include <stdint.h>
#include <stdbool.h>

#include <rtthread.h>
#include <common/gp_config.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
// ordering is important in source_type; lower-numbered is
// considered a better time source.  These values are documented
// and used in the parameters!
enum source_type {
    SOURCE_GPS = 0,
    SOURCE_MAVLINK_SYSTEM_TIME = 1,
    SOURCE_HW = 2,
    SOURCE_NONE,
};

typedef struct {
    // parameters
    Param_int8 allowed_types;
    Param_int16 tz_min;

    struct rt_mutex _mutex;

    enum source_type rtc_source_type;
    int64_t rtc_shift;
} gp_rtc;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void rtc_ctor();
void rtc_set_utc_usec(uint64_t time_utc_usec, enum source_type type);
bool rtc_get_utc_usec(uint64_t *usec);
bool rtc_get_system_clock_utc(uint8_t *hour, uint8_t *min, uint8_t *sec, uint16_t *ms);
bool rtc_get_local_time(uint8_t *hour, uint8_t *min, uint8_t *sec, uint16_t *ms);
uint32_t rtc_get_time_utc(int32_t hour, int32_t min, int32_t sec, int32_t ms);
time_t rtc_mktime(const struct tm *t);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



