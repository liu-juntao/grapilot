
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       jitter_correction.h
  * @author     baiyang
  * @date       2022-1-29
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
/*
  timestamp jitter correction class
 */
typedef struct {
    bool initialised;
    uint16_t max_lag_ms;
    uint16_t convergence_loops;
    uint16_t min_sample_counter;
    int64_t link_offset_usec;
    int64_t min_sample_us;
} JitterCorrection;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
// constructor
void jitter_correction(JitterCorrection* jc, uint16_t max_lag_ms, uint16_t convergence_loops);

// correct an offboard timestamp to a jitter-free local
// timestamp. See JitterCorrection.cpp for details
uint64_t jitter_correct_offboard_timestamp_usec(JitterCorrection* jc, uint64_t offboard_usec, uint64_t local_usec);

// correct an offboard timestamp to a jitter-free local
// timestamp. See JitterCorrection.cpp for details
uint32_t jitter_correct_offboard_timestamp_msec(JitterCorrection* jc, uint32_t offboard_ms, uint32_t local_ms);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



