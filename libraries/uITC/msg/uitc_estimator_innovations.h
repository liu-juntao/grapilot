
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_estimator_innovations.h
  * @author     baiyang
  * @date       2022-3-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(estimator_innovations);
ITC_DECLARE(estimator_innovation_variances);
ITC_DECLARE(estimator_innovation_test_ratios);
/*----------------------------------typedef-----------------------------------*/
typedef struct {
    uint64_t timestamp;
    uint64_t timestamp_sample;
    float gps_hvel[2];
    float gps_vvel;
    float gps_hpos[2];
    float gps_vpos;
    float ev_hvel[2];
    float ev_vvel;
    float ev_hpos[2];
    float ev_vpos;
    float rng_vpos;
    float baro_vpos;
    float aux_hvel[2];
    float aux_vvel;
    float flow[2];
    float heading;
    float mag_field[3];
    float drag[2];
    float airspeed;
    float beta;
    float hagl;
}uitc_estimator_innovations;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



