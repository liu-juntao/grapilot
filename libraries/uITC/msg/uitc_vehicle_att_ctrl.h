
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_att_ctrl.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_att_ctrl);
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;

    float target_euler_angle[3];    // 目标欧拉角，x:roll，y:pitch，z:yaw
    float target_ang_vel_body[3];   // 目标轴角速率，小角度时，轴角速率约等于三轴角速率
    float throttle_in;              // 输入油门

    // 姿态内环PID目标和误差
    float ctrl_r_pid_target;
    float ctrl_r_pid_error;
    float ctrl_p_pid_target;
    float ctrl_p_pid_error;
    float ctrl_y_pid_target;
    float ctrl_y_pid_error;
} uitc_vehicle_att_ctrl;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



