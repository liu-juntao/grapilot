
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_sensor_gps.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
#include <c_library_v2/ardupilotmega/mavlink.h>
#include <c_library_v2/ardupilotmega/mavlink_msg_autopilot_version_request.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(sensor_gps);
/*----------------------------------typedef-----------------------------------*/
typedef enum  {
    uITC_NO_GPS = GPS_FIX_TYPE_NO_GPS,                     ///< No GPS connected/detected
    uITC_NO_FIX = GPS_FIX_TYPE_NO_FIX,                     ///< Receiving valid GPS messages but no lock
    uITC_GPS_OK_FIX_2D = GPS_FIX_TYPE_2D_FIX,              ///< Receiving valid messages and 2D lock
    uITC_GPS_OK_FIX_3D = GPS_FIX_TYPE_3D_FIX,              ///< Receiving valid messages and 3D lock
    uITC_GPS_OK_FIX_3D_DGPS = GPS_FIX_TYPE_DGPS,           ///< Receiving valid messages and 3D lock with differential improvements
    uITC_GPS_OK_FIX_3D_RTK_FLOAT = GPS_FIX_TYPE_RTK_FLOAT, ///< Receiving valid messages and 3D RTK Float
    uITC_GPS_OK_FIX_3D_RTK_FIXED = GPS_FIX_TYPE_RTK_FIXED, ///< Receiving valid messages and 3D RTK Fixed
} uITC_GPS_status;

typedef struct {
    uint64_t timestamp_us;              /**< 自系统上电以来的时间 */
    uint64_t time_gps_usec;             /**< Timestamp (microseconds in GPS format), this is the timestamp which comes from the gps module   */

    uint8_t fix_type;                   /**< 0-1: no fix, 2: 2D fix, 3: 3D fix. Some applications will not use the value of this field unless it is at least two, so always correctly fill in the fix.   */

    uint8_t num_sats;                   ///< Number of visible satellites

    int32_t lat;                        /**< Latitude in 1E-7 degrees */
    int32_t lon;                        /**< Longitude in 1E-7 degrees */
    int32_t alt_msl;                    /**< Altitude in 1E-3 meters (millimeters) above MSL  */
    uint32_t alt_ellipsoid;

    float s_variance_m_s;               /**< speed accuracy estimate m/s */
    float c_variance_rad;               /**< course accuracy estimate rad */

    uint16_t hdop;                      ///< horizontal dilution of precision in cm
    uint16_t vdop;                      ///< vertical dilution of precision in cm

    float vel_m_s;                      /**< GPS ground speed (m/s) */
    float vel_n_m_s;                    /**< North velocity in m/s */
    float vel_e_m_s;                    /**< East velocity in m/s */
    float vel_d_m_s;                    /**< Down velocity in m/s */
    float cog_rad;                      /**< Course over ground (NOT heading, but direction of movement) in rad, -PI..PI */
    bool  vel_ned_valid;                /**< Flag to indicate if NED speed is valid */

    float horizontal_accuracy;          ///< horizontal RMS accuracy estimate in m
    float vertical_accuracy;            ///< vertical RMS accuracy estimate in m

    float gps_yaw;                      ///< GPS derived yaw information, if available (degrees)
    bool  gps_yaw_configured;           ///< GPS is configured to provide yaw
    float gps_yaw_accuracy;           ///< heading accuracy of the GPS in degrees
    bool  have_gps_yaw;                ///< does GPS give yaw? Set to true only once available.
    bool  have_gps_yaw_accuracy;       ///< does the GPS give a heading accuracy estimate? Set to true only once available
}uitc_sensor_gps;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



