
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_home.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_home);
ITC_DECLARE(vehicle_origin);
/*----------------------------------typedef-----------------------------------*/
/**
  * @brief       通常为解锁点
  * @param[out]  
  * @retval      
  * @note        
  */
typedef struct
{
    uint64_t timestamp_us;

    int32_t    lat;    /* Latitude in degrees 1E7 */
    int32_t    lon;    /* Lonitude in degrees 1E7*/
    float      alt;    /* Altitude in meters (AMSL)*/

    float      x;      // X coordinate in meters
    float      y;      // Y coordinate in meters
    float      z;      // Z coordinate in meters,above_origin

    float      yaw;         //Yaw angle in radians
    bool       valid_alt;   //true when the altitude has been set
    bool       valid_hpos;  //true when the latitude and longitude have been set
    bool       valid_lpos;  //true when the local position (xyz) has been set

    float      mag_decl;     /* magnetic declination, in degree */
    bool       manual_home;  //true when home position was set manually
}uitc_vehicle_home;

/**
  * @brief       通常为GPS初始定位的点，由EKF给出
  * @param[out]  
  * @retval      
  * @note        
  */
typedef struct
{
    uint64_t timestamp_us;

    int32_t    lat;    /* Latitude in degrees 1E7 */
    int32_t    lon;    /* Lonitude in degrees 1E7*/
    float      alt;    /* Altitude in meters (AMSL)*/

    float      x;      // X coordinate in meters
    float      y;      // Y coordinate in meters
    float      z;      // Z coordinate in meters

    float      yaw;         //Yaw angle in radians
    bool       valid_alt;   //true when the altitude has been set
    bool       valid_hpos;  //true when the latitude and longitude have been set
    bool       valid_lpos;  //true when the local position (xyz) has been set

    float      mag_decl;     /* magnetic declination, in degree */
    bool       manual_origin;  //true when origin position was set manually
}uitc_vehicle_origin;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



