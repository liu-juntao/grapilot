
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_estimator_status_flags.h
  * @author     baiyang
  * @date       2022-3-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(estimator_status_flags);
/*----------------------------------typedef-----------------------------------*/
/** @ 
  * @brief  
  */
typedef struct {
	uint64_t timestamp;
	uint64_t timestamp_sample;
	uint32_t control_status_changes;
	uint32_t fault_status_changes;
	uint32_t innovation_fault_status_changes;
	bool cs_tilt_align;
	bool cs_yaw_align;
	bool cs_gps;
	bool cs_opt_flow;
	bool cs_mag_hdg;
	bool cs_mag_3d;
	bool cs_mag_dec;
	bool cs_in_air;
	bool cs_wind;
	bool cs_baro_hgt;
	bool cs_rng_hgt;
	bool cs_gps_hgt;
	bool cs_ev_pos;
	bool cs_ev_yaw;
	bool cs_ev_hgt;
	bool cs_fuse_beta;
	bool cs_mag_field_disturbed;
	bool cs_fixed_wing;
	bool cs_mag_fault;
	bool cs_fuse_aspd;
	bool cs_gnd_effect;
	bool cs_rng_stuck;
	bool cs_gps_yaw;
	bool cs_mag_aligned_in_flight;
	bool cs_ev_vel;
	bool cs_synthetic_mag_z;
	bool cs_vehicle_at_rest;
	bool fs_bad_mag_x;
	bool fs_bad_mag_y;
	bool fs_bad_mag_z;
	bool fs_bad_hdg;
	bool fs_bad_mag_decl;
	bool fs_bad_airspeed;
	bool fs_bad_sideslip;
	bool fs_bad_optflow_x;
	bool fs_bad_optflow_y;
	bool fs_bad_vel_n;
	bool fs_bad_vel_e;
	bool fs_bad_vel_d;
	bool fs_bad_pos_n;
	bool fs_bad_pos_e;
	bool fs_bad_pos_d;
	bool fs_bad_acc_bias;
	bool fs_bad_acc_vertical;
	bool fs_bad_acc_clipping;
	bool reject_hor_vel;
	bool reject_ver_vel;
	bool reject_hor_pos;
	bool reject_ver_pos;
	bool reject_mag_x;
	bool reject_mag_y;
	bool reject_mag_z;
	bool reject_yaw;
	bool reject_airspeed;
	bool reject_sideslip;
	bool reject_hagl;
	bool reject_optflow_x;
	bool reject_optflow_y;
} uitc_estimator_status_flags;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



