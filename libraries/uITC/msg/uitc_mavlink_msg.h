
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_mavlink_msg.h
  * @author     baiyang
  * @date       2021-11-10
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
#include <c_library_v2/ardupilotmega/mavlink.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(mavlink_msg);
/*----------------------------------typedef-----------------------------------*/
/**
  * @brief       mavlink消息
  * @param[out]  
  * @retval      
  * @note        注意，msg_t所指向的地址很有可能会变化，此uitc消息只能在
  *              回调函数中处理。
  */
typedef struct
{
    uint64_t timestamp_us;
    mavlink_message_t *msg_t;
} uitc_mavlink_msg;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



