
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_actuator.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_actuator_controls);
ITC_DECLARE(vehicle_actuator_outputs);
ITC_DECLARE(vehicle_actuator_armed);

#define NUM_ACTUATOR_OUTPUTS 16
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;
    float    control[NUM_ACTUATOR_OUTPUTS];    // 每个电机或舵机的归一化输出0~1
} uitc_actuator_controls;

typedef struct
{
    uint64_t timestamp_us;
    int16_t  output[NUM_ACTUATOR_OUTPUTS];    // 每个电机或舵机的PWM值，单位：微秒
} uitc_actuator_outputs;

typedef struct
{
    uint64_t timestamp_us;

    bool     armed;                   // Set to true if system is armed
    bool     prearmed;                // Set to true if the actuator safety is disabled but motors are not armed
    bool     ready_to_arm;            // Set to true if system is ready to be armed
    bool     lockdown;                // Set to true if actuators are forced to being disabled (due to emergency or HIL)
    bool     manual_lockdown;         // Set to true if manual throttle kill switch is engaged
    bool     force_failsafe;          // Set to true if the actuators are forced to the failsafe position
    bool     in_esc_calibration_mode; // IO/FMU should ignore messages from the actuator controls topics
    bool     soft_stop;               // Set to true if we need to ESCs to remove the idle constraint
} uitc_actuator_armed;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



