
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_hil.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
#include <c_library_v2/ardupilotmega/mavlink.h>
#include <c_library_v2/ardupilotmega/mavlink_msg_autopilot_version_request.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_hil_state);
/*----------------------------------typedef-----------------------------------*/
typedef struct {
    uint64_t time_usec; /*< [us] Timestamp (time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
    float roll; /*< [rad] Roll angle*/
    float pitch; /*< [rad] Pitch angle*/
    float yaw; /*< [rad] Yaw angle*/
    float rollspeed; /*< [rad/s] Body frame roll / phi angular speed*/
    float pitchspeed; /*< [rad/s] Body frame pitch / theta angular speed*/
    float yawspeed; /*< [rad/s] Body frame yaw / psi angular speed*/
    int32_t lat; /*< [degE7] Latitude*/
    int32_t lon; /*< [degE7] Longitude*/
    int32_t alt; /*< [mm] Altitude*/
    int16_t vx; /*< [cm/s] Ground X Speed (Latitude)*/
    int16_t vy; /*< [cm/s] Ground Y Speed (Longitude)*/
    int16_t vz; /*< [cm/s] Ground Z Speed (Altitude)*/
    int16_t xacc; /*< [mG] X acceleration*/
    int16_t yacc; /*< [mG] Y acceleration*/
    int16_t zacc; /*< [mG] Z acceleration*/
} uitc_vehicle_hil_state;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



