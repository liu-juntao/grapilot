
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_digital_airplane.h
  * @author     baiyang
  * @date       2021-7-14
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
#include <common/gp_math/gp_mathlib.h>
#include <common/location/location.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(digital_airplane_state);
/*----------------------------------typedef-----------------------------------*/
//数字飞机相关数据  内部使用
typedef struct
{
    //时间戳
    uint32_t time_ms;

    //飞行状态
    Vector3f_t gyro;
    Quat_t     quat;
    Euler_t    euler;
    Vector3f_t acc_body;
    Vector3f_t velocity_ef;
    Vector3f_t position;
    
    //扩展状态
    Location   location;
    float      prop_rpm[6];
    uint16_t   num_engines;
} uitc_digital_airplane_state;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



