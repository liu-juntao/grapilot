
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_sensor_baro.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(sensor_baro);
ITC_DECLARE(sensor_baro_raw);
/*----------------------------------typedef-----------------------------------*/
typedef struct {
    // what is the primary sensor at the moment?
    uint8_t primary;

    struct {
        uint64_t timestamp_us;

        bool  healthy;              // true if calculated altitude is ok
        float temperature_deg;
        float pressure_pa;
        float ground_pressure_pa;
    } baro_raw[3];
} uitc_sensor_baro_raw;

typedef struct
{
    uint64_t timestamp_us;

    // what is the primary sensor at the moment?
    uint8_t primary;

    float velocity_ms;             // 主传感器计算的速率
    float altitude_m;              // 主传感器计算的绝对气压高
} uitc_sensor_baro;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



