
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_pos_ctrl.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_pos_ctrl_xy);
ITC_DECLARE(vehicle_pos_ctrl_z);
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;

    float target_pos_xy[2];    //单位：cm
    float target_vel_xy[2];    //单位：cm/s
    float target_acc_xy[2];    //单位：cm/s/s
} uitc_vehicle_pos_ctrl_xy;

typedef struct
{
    uint64_t timestamp_us;

    float target_pos_z;    //单位：cm
    float target_vel_z;    //单位：cm/s
    float target_acc_z;    //单位：cm/s/s
} uitc_vehicle_pos_ctrl_z;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



