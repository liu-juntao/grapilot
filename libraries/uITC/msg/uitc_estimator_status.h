
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_estimator_status.h
  * @author     baiyang
  * @date       2022-3-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(estimator_status);

#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_GPS_FIX 0
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MIN_SAT_COUNT 1
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MIN_PDOP 2
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_HORZ_ERR 3
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_VERT_ERR 4
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_SPD_ERR 5
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_HORZ_DRIFT 6
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_VERT_DRIFT 7
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_HORZ_SPD_ERR 8
#define ESTIMATOR_STATUS_GPS_CHECK_FAIL_MAX_VERT_SPD_ERR 9
#define ESTIMATOR_STATUS_CS_TILT_ALIGN 0
#define ESTIMATOR_STATUS_CS_YAW_ALIGN 1
#define ESTIMATOR_STATUS_CS_GPS 2
#define ESTIMATOR_STATUS_CS_OPT_FLOW 3
#define ESTIMATOR_STATUS_CS_MAG_HDG 4
#define ESTIMATOR_STATUS_CS_MAG_3D 5
#define ESTIMATOR_STATUS_CS_MAG_DEC 6
#define ESTIMATOR_STATUS_CS_IN_AIR 7
#define ESTIMATOR_STATUS_CS_WIND 8
#define ESTIMATOR_STATUS_CS_BARO_HGT 9
#define ESTIMATOR_STATUS_CS_RNG_HGT 10
#define ESTIMATOR_STATUS_CS_GPS_HGT 11
#define ESTIMATOR_STATUS_CS_EV_POS 12
#define ESTIMATOR_STATUS_CS_EV_YAW 13
#define ESTIMATOR_STATUS_CS_EV_HGT 14
#define ESTIMATOR_STATUS_CS_BETA 15
#define ESTIMATOR_STATUS_CS_MAG_FIELD 16
#define ESTIMATOR_STATUS_CS_FIXED_WING 17
#define ESTIMATOR_STATUS_CS_MAG_FAULT 18
#define ESTIMATOR_STATUS_CS_ASPD 19
#define ESTIMATOR_STATUS_CS_GND_EFFECT 20
#define ESTIMATOR_STATUS_CS_RNG_STUCK 21
#define ESTIMATOR_STATUS_CS_GPS_YAW 22
#define ESTIMATOR_STATUS_CS_MAG_ALIGNED 23
/*----------------------------------typedef-----------------------------------*/
/** @ 
  * @brief  
  */
typedef struct {
    uint64_t timestamp;
    uint64_t timestamp_sample;
    float vibe[3];
    float output_tracking_error[3];
    uint32_t control_mode_flags;
    uint32_t filter_fault_flags;
    float pos_horiz_accuracy;
    float pos_vert_accuracy;
    float mag_test_ratio;
    float vel_test_ratio;
    float pos_test_ratio;
    float hgt_test_ratio;
    float tas_test_ratio;
    float hagl_test_ratio;
    float beta_test_ratio;
    float time_slip;
    uint32_t accel_device_id;
    uint32_t gyro_device_id;
    uint32_t baro_device_id;
    uint32_t mag_device_id;
    uint16_t gps_check_fail_flags;
    uint16_t innovation_check_flags;
    uint16_t solution_status_flags;
    bool pre_flt_fail_innov_heading;
    bool pre_flt_fail_innov_vel_horiz;
    bool pre_flt_fail_innov_vel_vert;
    bool pre_flt_fail_innov_height;
    bool pre_flt_fail_mag_field_disturbed;
    uint8_t health_flags;
    uint8_t timeout_flags;
}uitc_estimator_status;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



