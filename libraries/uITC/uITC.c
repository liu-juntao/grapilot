
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uITC.c
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <string.h>
#include "uITC.h"
#include <common/console/console.h>

#include <rtthread.h>
/*-----------------------------------macro------------------------------------*/
#define OS_ENTER_CRITICAL        rt_enter_critical()
#define OS_EXIT_CRITICAL         rt_exit_critical()

#define ITC_EVENT_HANDLE            rt_event_t    
#define ITC_SEND_EVENT(event_t)     rt_event_send(event_t, 1)
#define ITC_MALLOC(size)            rt_malloc(size)
#define ITC_FREE(ptr)               rt_free(ptr)
#define ITC_ENTER_CRITICAL          OS_ENTER_CRITICAL
#define ITC_EXIT_CRITICAL           OS_EXIT_CRITICAL
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static char* TAG = "uITC: ";
static uint32_t uitc_msg_total_size;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]   hub  
  * @param[out]  
  * @retval      
  * @note        
  */
int8_t itc_advertise(itc_hub* hub)
{
    int res = 0;
    
    if(hub->pdata != NULL){
        // already advertised
        return 0;
    }
    
    ITC_ENTER_CRITICAL;
    hub->pdata = ITC_MALLOC(hub->obj_size * 2);
    if(hub->pdata == NULL){
        res = 1;
    } else {
        uitc_msg_total_size += hub->obj_size * 2;
    }
    ITC_EXIT_CRITICAL;
   
    return res;
}

/**
  * @brief       
  * @param[in]   hub  
  * @param[in]   cb  
  * @param[in]   parameter  
  * @param[out]  
  * @retval      
  * @note        
  */
itc_node_t itc_subscribe(itc_hub* hub, void (*cb)(void *parameter))
{
    if (hub == NULL) {
        return NULL;
    }
    
    if(hub->link_num >= ITC_MAX_LINK_NUM){
        console_printf("%s: itc link num is already full!\n",TAG);
        return NULL;
    }

    itc_node_t node = (itc_node_t)ITC_MALLOC(sizeof(itc_node));
    if(node == NULL){
        console_printf("%s: itc create node fail!\n",TAG);
        return NULL;
    }
    node->renewal = 0;
    node->cb = cb;
    node->next = NULL;

    ITC_ENTER_CRITICAL;
    /* no node link yet */
    if(hub->link_tail == NULL){
        hub->link_head = hub->link_tail = node;
    }else{	
        hub->link_tail->next = node;
        hub->link_tail = node;
    }
    hub->link_num++;
    ITC_EXIT_CRITICAL;

    return node;
}

/**
  * @brief       
  * @param[in]   hub  
  * @param[in]   data  
  * @param[out]  
  * @retval      
  * @note        
  */
int8_t itc_publish(itc_hub* hub, const void* data)
{
    if(data == NULL){
        console_printf("%s: null data publish!\n",TAG);
        return -1;
    }

    if(hub->pdata == NULL){
        // hub is not advertised yet
        return -2;
    }

    bool writing = hub->writing;

    // 检测到有发布者在写，就退出
    // 不同线程发布同一消息的情况不应出现
    // 但还是对此情况进行检测
    if (!writing) {
        hub->writing ^= true;
    } else {
        //console_printf("%s: Other publishers are publishing data(-3)!\n",TAG);
        return -3;
    }

    if (writing == hub->writing) {
        hub->writing ^= true;
        //console_printf("%s: Other publishers are publishing data(-4)!\n",TAG);
        return -4;
    }

    if (hub->reading == true && hub->read_idx == hub->write_idx) {
        hub->write_idx ^= 1;
        //console_printf("%s: subscribers are reading data!\n",TAG);
    }

    uint8_t write_idx = hub->write_idx;

    /* copy data to hub */
    rt_memcpy(&(((uint8_t*)hub->pdata)[write_idx*hub->obj_size]), data, hub->obj_size);

    hub->write_idx ^= 1;
    hub->writing = false;

    /* update each node's renewal flag */
    itc_node_t node = hub->link_head;
    while(node != NULL){
        node->renewal = 1;
        node = node->next;
    }
    hub->published = 1;

    /* invoke callback func */
    node = hub->link_head;
    while(node != NULL){
        if(node->cb != NULL){
            node->cb(&(((uint8_t*)hub->pdata)[write_idx*hub->obj_size]));
            //console_printf("%s:[%s-%u] itc_copy read_idx: %u\n", TAG, hub->obj_name, node, write_idx);
        }
        node = node->next;
    }

    return 0;
}

/**
  * @brief       
  * @param[in]   node_t  
  * @param[out]  
  * @retval      
  * @note        
  */
bool itc_update(itc_node_t node_t)
{
    bool renewal = 0;

    if (node_t != NULL) {
        renewal = node_t->renewal;
    }

    return renewal;
}

/**
  * @brief       
  * @param[in]   node_t  
  * @param[in]   update  
  * @param[out]  
  * @retval      
  * @note        
  */
bool itc_check(itc_node_t node_t, bool* update)
{
    if (node_t == NULL)
    {
        console_printf("%s: node is null\n",TAG);
        return 0;
    }

    *update = node_t->renewal;

    return 1;
}

/**
  * @brief       
  * @param[in]   hub  
  * @param[in]   node_t  
  * @param[in]   buffer  
  * @param[out]  
  * @retval      
  * @note        
  */
int8_t itc_copy(itc_hub* hub, itc_node_t node_t, void* buffer)
{
    if(hub->pdata == NULL){
        // not advertised yet
        console_printf("%s: itc_copy from null hub:%s\n", TAG, hub->obj_name);
        return -1;
    }
    if(!hub->published){
        // copy before published
        return -2;
    }

    uint8_t read_idx = hub->read_idx = hub->write_idx ^ 1;

    hub->reading = true;
    rt_memcpy(buffer, &(((uint8_t*)hub->pdata)[read_idx*hub->obj_size]), hub->obj_size);
    node_t->renewal = 0;

    hub->reading = false;

    return 0;
}

/**
  * @brief       
  * @param[in]   hub  
  * @param[in]   buffer  
  * @param[out]  
  * @retval      
  * @note        
  */
int8_t itc_copy_from_hub(itc_hub* hub, void* buffer)
{
    if(hub->pdata == NULL){
        // not advertised yet
        console_printf("%s: copy_from_hub from null hub:%s\n", TAG, hub->obj_name);
        return -1;
    }
    if(!hub->published){
        // copy before published
        return -2;
    }

    uint8_t read_idx = hub->read_idx = hub->write_idx ^1;

    hub->reading = true;
    rt_memcpy(buffer, &(((uint8_t*)hub->pdata)[read_idx*hub->obj_size]), hub->obj_size);
    hub->reading = false;

    return 0;
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
uint32_t itc_get_msg_total_size()
{
    return uitc_msg_total_size;
}

/*------------------------------------test------------------------------------*/


