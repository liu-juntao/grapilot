
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uITC.h
  * @author     baiyang
  * @date       2021-7-13
  * @note       一写多读
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
/*-----------------------------------macro------------------------------------*/

#define ITC_MAX_LINK_NUM        30
#define ITC_ID(_name)               (&__itc_##_name)
#define ITC_DECLARE(_name)          extern itc_hub __itc_##_name
    
#define ITC_DEFINE(_name, _size)            \
    itc_hub __itc_##_name = {                \
        .obj_name = #_name,                 \
        .obj_size = _size,                  \
        .pdata = NULL,                      \
        .link_head = NULL,                  \
        .link_tail = NULL,                  \
        .link_num = 0,                      \
        .published = 0,                     \
        .write_idx = 0,                     \
        .read_idx = 0,                   \
        .writing   = false,                 \
        .reading   = false                  \
    }

/*----------------------------------typedef-----------------------------------*/
typedef struct ItcNode        itc_node;
typedef struct ItcNode*       itc_node_t;
struct ItcNode
{
    volatile uint8_t renewal;
    void (*cb)(void *parameter);
    itc_node_t next;
};

typedef struct ItcHub        itc_hub;
struct ItcHub
{
    const char* obj_name;
    const uint32_t obj_size;
    void* pdata;
    itc_node_t link_head;
    itc_node_t link_tail;
    uint32_t link_num;
    uint8_t published;     // publish flag

    uint8_t write_idx;     // 写检索，用ping-pong buffer实现不需要关中断的数据写入、读取
    uint8_t read_idx;      // 读检索

    volatile bool writing;
    volatile bool reading;
};

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
int8_t itc_advertise(itc_hub* hub);
itc_node_t itc_subscribe(itc_hub* hub, void (*cb)(void *parameter));

int8_t itc_publish(itc_hub* hub, const void* data);

bool itc_update(itc_node_t node_t);
bool itc_check(itc_node_t node_t, bool* update);

int8_t itc_copy(itc_hub* hub, itc_node_t node_t, void* buffer);
int8_t itc_copy_from_hub(itc_hub* hub, void* buffer);

uint32_t itc_get_msg_total_size();
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



