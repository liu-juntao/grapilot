
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_baro_ms5611.h
  * @author     baiyang
  * @date       2021-11-21
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "sensor_baro.h"
#include "sensor_baro_backend.h"

#include <device_manager/dev_mgr.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
enum MS56XX_TYPE {
    BARO_MS5611 = 0,
    BARO_MS5607 = 1,
    BARO_MS5637 = 2,
    BARO_MS5837 = 3
};

typedef struct {
    sensor_baro_backend backend;

    gp_device_t _dev;

    /* Shared values between thread sampling the HW and main thread */
    struct {
        uint32_t s_D1;
        uint32_t s_D2;
        uint8_t d1_count;
        uint8_t d2_count;
    } _accum;

    uint8_t _state;
    uint8_t _instance;

    /* Last compensated values from accumulated sample */
    float _D1, _D2;

    // Internal calibration registers
    struct {
        uint16_t c1, c2, c3, c4, c5, c6;
    } _cal_reg;

    bool _discard_next;

    enum MS56XX_TYPE _ms56xx_type;
} sensor_baro_ms56xx;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void sensor_baro_ms56xx_ctor(sensor_baro_ms56xx *ms56xx, gp_device_t dev,  enum MS56XX_TYPE ms56xx_type);
sensor_baro_backend *sensor_baro_ms56xx_probe(gp_device_t dev, enum MS56XX_TYPE ms56xx_type);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



