
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       sensor_baro_param.c
  * @author     baiyang
  * @date       2021-11-21
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "sensor_baro.h"
#include <parameter/param.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void sensor_baro_assign_param()
{
    sensor_baro * _baro = sensor_baro_get_singleton();

    param_link_variable(PARAM_ID(BARO, BARO1_GND_PRESS), &_baro->sensors[0].ground_pressure);
    param_link_variable(PARAM_ID(BARO, BARO2_GND_PRESS), &_baro->sensors[1].ground_pressure);
    param_link_variable(PARAM_ID(BARO, BARO3_GND_PRESS), &_baro->sensors[2].ground_pressure);

    param_link_variable(PARAM_ID(BARO, BARO1_DEVID), &_baro->sensors[0].bus_id);
    param_link_variable(PARAM_ID(BARO, BARO2_DEVID), &_baro->sensors[1].bus_id);
    param_link_variable(PARAM_ID(BARO, BARO3_DEVID), &_baro->sensors[2].bus_id);

    param_link_variable(PARAM_ID(BARO, BARO_ALT_OFFSET), &_baro->_alt_offset);
    param_link_variable(PARAM_ID(BARO, BARO_PRIMARY), &_baro->_primary_baro);
    param_link_variable(PARAM_ID(BARO, BARO_EXT_BUS), &_baro->_ext_bus);

    param_link_variable(PARAM_ID(BARO, BARO_SPEC_GRAV), &_baro->_specific_gravity);
    param_link_variable(PARAM_ID(BARO, BARO_GND_TEMP), &_baro->_user_ground_temperature);
    param_link_variable(PARAM_ID(BARO, BARO_FLTR_RNG), &_baro->_filter_range);
    param_link_variable(PARAM_ID(BARO, BARO_PROBE_EXT), &_baro->_baro_probe_ext);
}

void sensor_baro_ground_pressure_set_and_save(uint8_t index, float val)
{
    sensor_baro * _baro = sensor_baro_get_singleton();

    if (index == 0) {
        param_set_and_save(PARAM_ID(BARO, BARO1_GND_PRESS), val);
    } else if (index == 1) {
        param_set_and_save(PARAM_ID(BARO, BARO2_GND_PRESS), val);
    } else if (index == 2) {
        param_set_and_save(PARAM_ID(BARO, BARO3_GND_PRESS), val);
    }
}

/*------------------------------------test------------------------------------*/


