
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_serial.h
  * @author     baiyang
  * @date       2021-7-19
  ******************************************************************************
  */

#pragma once

/*----------------------------------include-----------------------------------*/
#include <windows.h>
/*-----------------------------------macro------------------------------------*/
#define FC_DTRDSR       0x01
#define FC_RTSCTS       0x02
#define FC_XONXOFF      0x04
#define ASCII_BEL       0x07
#define ASCII_BS        0x08
#define ASCII_LF        0x0A
#define ASCII_CR        0x0D
#define ASCII_XON       0x11
#define ASCII_XOFF      0x13

/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
class WSerial
{
public:
    WSerial();
    ~WSerial();

    //打开关闭串口
    BOOL Open(int nPort = 2, int nBaud = 9600);
    BOOL OpenSbus(int nPort = 2, int nBaud = 9600);
    BOOL Close(void);

    int ReadData(void*, int);
    int SendData(const char*, int);
    int ReadDataWaiting(void);
    HANDLE GetSerialHandle(void);
    BOOL IsOpened() { return m_bOpened; }
protected:
    BOOL WriteCommByte(unsigned char);
    HANDLE m_hIDComDev;        //串口句柄
    OVERLAPPED m_OverlappedRead, m_OverlappedWrite;
    BOOL m_bOpened;
};
/*------------------------------------test------------------------------------*/


