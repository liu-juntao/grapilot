/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VDD_3V3_SENSORS_EN_Pin GPIO_PIN_3
#define VDD_3V3_SENSORS_EN_GPIO_Port GPIOE
#define MPU_EXT_CS_Pin GPIO_PIN_4
#define MPU_EXT_CS_GPIO_Port GPIOE
#define GYRO_EXT_CS_Pin GPIO_PIN_13
#define GYRO_EXT_CS_GPIO_Port GPIOC
#define BARO_EXT_CS_Pin GPIO_PIN_14
#define BARO_EXT_CS_GPIO_Port GPIOC
#define ACCEL_EXT_CS_Pin GPIO_PIN_15
#define ACCEL_EXT_CS_GPIO_Port GPIOC
#define VBUS_nVALID_Pin GPIO_PIN_0
#define VBUS_nVALID_GPIO_Port GPIOC
#define MAG_CS_Pin GPIO_PIN_1
#define MAG_CS_GPIO_Port GPIOC
#define MPU_CS_Pin GPIO_PIN_2
#define MPU_CS_GPIO_Port GPIOC
#define AUX_POWER_Pin GPIO_PIN_3
#define AUX_POWER_GPIO_Port GPIOC
#define AUX_ADC2_Pin GPIO_PIN_4
#define AUX_ADC2_GPIO_Port GPIOC
#define PRESSURE_SENS_Pin GPIO_PIN_5
#define PRESSURE_SENS_GPIO_Port GPIOC
#define EXTERN_GPIO1_Pin GPIO_PIN_0
#define EXTERN_GPIO1_GPIO_Port GPIOB
#define EXTERN_GPIO2_Pin GPIO_PIN_1
#define EXTERN_GPIO2_GPIO_Port GPIOB
#define VDD_5V_HIPOWER_nOC_Pin GPIO_PIN_10
#define VDD_5V_HIPOWER_nOC_GPIO_Port GPIOE
#define FMU_LED_AMBER_Pin GPIO_PIN_12
#define FMU_LED_AMBER_GPIO_Port GPIOE
#define FRAM_CS_Pin GPIO_PIN_10
#define FRAM_CS_GPIO_Port GPIOD
#define MPU_DRDY_Pin GPIO_PIN_15
#define MPU_DRDY_GPIO_Port GPIOD
#define VDD_5V_PERIPH_EN_Pin GPIO_PIN_8
#define VDD_5V_PERIPH_EN_GPIO_Port GPIOA
#define VBUS_Pin GPIO_PIN_9
#define VBUS_GPIO_Port GPIOA
#define BARO_CS_Pin GPIO_PIN_7
#define BARO_CS_GPIO_Port GPIOD
#define FMU_SW0_Pin GPIO_PIN_3
#define FMU_SW0_GPIO_Port GPIOB
#define VDD_BRICK_nVALID_Pin GPIO_PIN_5
#define VDD_BRICK_nVALID_GPIO_Port GPIOB
#define VDD_BRICK2_nVALID_Pin GPIO_PIN_7
#define VDD_BRICK2_nVALID_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
