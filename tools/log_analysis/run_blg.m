% Author：Shuhao Zhang
% Data  ：2020-11-25
% Introduction
% 解析Blog数据和绘图
% 

clc;
clear all;

global ui_popup;
global ui_popup_ele;
global linebuff;

global flag_load;

global header;
global logmsg;

global color;
color.yans = 'rgbcmyk';
color.cnt  = 0;
%% 

flag_load = false;
fig = figure('units','normalized','position',[.2 .2 .7 .7],'name','解析BLog','numbertitle','off','color',[0.95 0.96 0.97]);
hm = findall(fig , 'type', 'uimenu'); %查找标准菜单
delete(hm);

linebuff.ax = axes('units','normalized','position',[.05 .07 .9 .7]);
grid on;
xlabel('time(ms)');

ui_load  = uicontrol('units','normalized','position',[.05 .9 .1 .05],'fontsize',10,'style','pushbutton','BackgroundColor',[0.95 0.96 0.97],'String','加载日志文件');
ui_status  = uicontrol('units','normalized','position',[.2 .9 .1 .05],'fontsize',10,'style','text','BackgroundColor',[0.95 0.96 0.97],'String','未加载文件');

uipanel(fig, 'position',[.047 .78 .33 .1],'Title', '选择数据','BackgroundColor',[0.95 0.96 0.97], 'TitlePosition', 'centertop');
uicontrol('units','normalized','position',[.05 .805 .07 .04],'style','text','fontsize',10,'BackgroundColor',[0.95 0.96 0.97],'string','Bus：');
uicontrol('units','normalized','position',[.20 .805 .07 .04],'style','text','fontsize',10,'BackgroundColor',[0.95 0.96 0.97],'string','Elem：');
ui_popup = uicontrol('units','normalized','position',[.1 .8 .10 .05],'style','popupmenu','fontsize',10,'BackgroundColor',[0.85 0.85 0.8],'BackgroundColor',[0.95 0.94 0.90], 'String', {'Null'});
ui_popup_ele = uicontrol('units','normalized','position',[.25 .8 .10 .05],'style','popupmenu', 'fontsize',10,'BackgroundColor',[0.85 0.85 0.8],'BackgroundColor',[0.95 0.94 0.90],'String', {'Null'});

uipanel(fig, 'position',[.38 .78 .5 .1],'Title', '添加 && 删除','BackgroundColor',[0.95 0.96 0.97], 'TitlePosition', 'centertop');
ui_add   = uicontrol('units','normalized','position',[.40 .8 .1 .05],'style','pushbutton','fontsize',10,'BackgroundColor',[0.95 0.96 0.97],'String','添加');
ui_addr   = uicontrol('units','normalized','position',[.52 .8 .1 .05],'style','pushbutton','fontsize',10,'BackgroundColor',[0.95 0.96 0.97],'String','右添加');

ui_del   = uicontrol('units','normalized','position',[.64 .8 .1 .05],'style','pushbutton','fontsize',10,'BackgroundColor',[0.95 0.96 0.97],'String','删除');
ui_del_all = uicontrol('units','normalized','position',[.76 .8 .1 .05],'style','pushbutton','fontsize',10,'BackgroundColor',[0.95 0.96 0.97],'String','全部删除');

%按键相关回调函数
ui_load.Callback = @load_cb;
ui_add.Callback  = @add_cb;
ui_addr.Callback = @addr_cb;
ui_popup.Callback = @popup_cb;
ui_del.Callback  = @del_cb;
ui_del_all.Callback = @delall_cb;

%回调函数定义
function load_cb(src, event)
    global ui_popup;
    global ui_popup_ele;
    global header;
    global logmsg;
    global linebuff;
    global flag_load
    
    ui_status  = uicontrol('units','normalized','position',[.2 .9 .1 .05],'fontsize',10,'style','text','BackgroundColor',[0.95 0.96 0.97],'String','未加载文件');
    
    [file, folder] = uigetfile({'*.bin'; '*.log'});
    logfile = strcat(folder, file);
    %%
%     fileID = fopen(logfile, 'r');
%     fileDir = dir(logfile);
%     header.version = fread(fileID, 1, 'uint16=>uint16');
%     header.timestamp = fread(fileID, 1, 'uint32=>uint32');
%     header.max_name_len = fread(fileID, 1, 'uint16=>uint16');
%     header.max_desc_len = fread(fileID, 1, 'uint16=>uint16');
%     header.max_model_info_len = fread(fileID, 1, 'uint16=>uint16');
%     header.description = fread(fileID, header.max_desc_len, 'char=>char');
%     header.model_info = fread(fileID, header.max_model_info_len, 'char=>char');
%     header.num_bus = fread(fileID, 1, 'uint8=>uint8');
%     
%     for n = 1:header.num_bus
%         header.bus(n).name     = fread(fileID, [1,header.max_name_len], 'uint8=>char');
%         header.bus(n).msg_id   = fread(fileID, 1,  'uint8=>uint8');
%         header.bus(n).num_elem = fread(fileID, 1,  'uint8=>uint8');
%         
%         for k = 1:header.bus(n).num_elem
%             header.bus(n).elem_list(k).name   = fread(fileID, [1,header.max_name_len], 'uint8=>char');
%             header.bus(n).elem_list(k).type   = fread(fileID, 1, 'uint16=>uint16');
%             header.bus(n).elem_list(k).number = fread(fileID, 1, 'uint16=>uint16');
%         end
%         MsgCount{header.bus(n).msg_id} = 0;
%         LogMsg{header.bus(n).msg_id} = {};
%         
%         if header.bus(n).msg_id ~= n
%             bus_alligned = 0;
%         end
%     end

    
    %%
%     [LogHeader, LogMsg] = blog_read(logfile);
%     header = LogHeader;
%     logmsg = LogMsg;
    [header,logmsg] = blog_read(logfile);
    for n = 1:header.num_bus
        BusName = strrep(header.bus(n).name, '"', '');
        BusName = BusName(~isspace(BusName));
        cell_bus{1, n} = BusName;
    end
    ui_popup.String = cell_bus;
    for n = 1:header.bus(1).num_elem
        EleName = strrep(header.bus(1).elem_list(n).name, '"', '');
        EleName = EleName(~isspace(EleName));
        cell_elem{1, n} = EleName;
    end
    linebuff.state(header.num_bus, 30) = false;
    linebuff.side(header.num_bus, 30) = 0;
    ui_popup_ele.String = cell_elem;
    
    flag_load = true;
    ui_status  = uicontrol('units','normalized','position',[.2 .9 .1 .05],'fontsize',10,'style','text','BackgroundColor',[0.95 0.96 0.97],'String','加载文件完成');
end


function popup_cb(src, event)
    global ui_popup_ele;
    global header;
    global flag_load
    if(flag_load == false)
        return;
    end
    val = src.Value;
    for n = 1:header.bus(val).num_elem
        EleName = strrep(header.bus(val).elem_list(n).name, '"', '');
        EleName = EleName(~isspace(EleName));
        cell_elem{1, n} = EleName;
    end
    ui_popup_ele.String = cell_elem;  
    ui_popup_ele.Value  = 1;
end

function add_cb(src, event)
  
    global ui_popup_ele; 
    global ui_popup;
    global linebuff;
    global logmsg;
    global header;
    global color;
    global flag_load
    if(flag_load == false)
        return;
    end
    val_bus = ui_popup.Value;
    val_ele = ui_popup_ele.Value;
    
    if linebuff.state(val_bus, val_ele) == true
       return;
    end
    color.cnt = color.cnt + 1;
    
    elem_name = strrep(header.bus(val_bus).elem_list(val_ele).name, '"', '');
    elem_name = elem_name(~isspace(elem_name));
    hold on;
    
    if strcmp(linebuff.ax.YAxisLocation , 'right')
        yyaxis left;
    end
    linebuff.val(val_bus, val_ele) = plot(logmsg{val_bus}{1}, logmsg{val_bus}{val_ele}, ...
                                    strcat(color.yans(mod(color.cnt, 7)), '-'), 'DisplayName', strcat('l-',elem_name), 'LineWidth', 0.75);
                                 %header.bus(val_bus).elem_list(val_ele).name
    linebuff.state(val_bus, val_ele) = true;
    legend('show');
    
end

function addr_cb(src, event)
    global ui_popup_ele; 
    global ui_popup;
    global linebuff;
    global logmsg;
    global header;
    global color;
    global flag_load
    if(flag_load == false)
        return;
    end
    val_bus = ui_popup.Value;
    val_ele = ui_popup_ele.Value;
    
    if linebuff.state(val_bus, val_ele) == true
       return;
    end
    color.cnt = color.cnt + 1;
    if(color.cnt == 7)
        color.cnt = 1;
    end
    linebuff.side(val_bus, val_ele) = 1;
    hold on
    if strcmp(linebuff.ax.YAxisLocation , 'left')
        yyaxis right;
    end
    elem_name = strrep(header.bus(val_bus).elem_list(val_ele).name, '"', '');
    elem_name = elem_name(~isspace(elem_name));
    linebuff.val(val_bus, val_ele) = plot(logmsg{val_bus}{1}, logmsg{val_bus}{val_ele}, ...
                                      strcat(color.yans(mod(color.cnt, 7)), '-'), 'DisplayName', strcat('r-',elem_name));                            
    linebuff.state(val_bus, val_ele) = true;
    legend('show');
    %legend(ah, linebuff.val(val_bus, val_ele), 'Location', 'northeast');
    
end

function del_cb(src, event)
    global ui_popup_ele; 
    global ui_popup;
    global linebuff;
    %global logmsg;
    global header;
    global flag_load
    if(flag_load == false)
        return;
    end
    val_bus = ui_popup.Value;
    val_ele = ui_popup_ele.Value;
    if(linebuff.state(val_bus, val_ele) == true)
        linebuff.state(val_bus, val_ele) = false;
        delete(linebuff.val(val_bus, val_ele));
        linebuff.side(header.num_bus, header.bus(1).num_elem) = 0;
    end
end

function delall_cb(src, event)
    global header;
    global linebuff;
    global color;
    global flag_load
    if(flag_load == false)
        return;
    end
    for n = 1:header.num_bus
        for k = 1:header.bus(n).num_elem
            if(linebuff.state(n, k) == true)
                linebuff.state(n, k) = false;
                delete(linebuff.val(n, k));
                linebuff.side(n, k) = 0;
            end
        end
    end
    color.cnt  = 0;
    %cla
    %cla reset
    legend('off');
end



%%
function [LogHeader, LogMsg] = blog_read(logfile)
    
    %与param_type_t对应
    BLOG_TYPE = ["int8=>int8", "uint8=>uint8", "int16=>int16", "uint16=>uint16",...
    "int32=>int32", "uint32=>uint32", "float=>float", "double=>double"];

    BLOG_BEGIN_MSG1 = hex2dec('92');    %帧头
    BLOG_BEGIN_MSG2 = hex2dec('05');
    
    BLOG_END_MSG = hex2dec('26');       %帧尾
    
    fileID = fopen(logfile, 'r');
    fileDir = dir(logfile);
    
    if fileID == -1
        disp('读取失败');
        return;
    end
    
    %%
    %读取Header
    LogHeader.version = fread(fileID, 1, 'uint16=>uint16');
    LogHeader.timestamp = fread(fileID, 1, 'uint32=>uint32');
    LogHeader.max_name_len = fread(fileID, 1, 'uint16=>uint16');
    LogHeader.max_desc_len = fread(fileID, 1, 'uint16=>uint16');
    LogHeader.max_model_info_len = fread(fileID, 1, 'uint16=>uint16');
    LogHeader.description = fread(fileID, LogHeader.max_desc_len, 'char=>char');
    LogHeader.model_info = fread(fileID, LogHeader.max_model_info_len, 'char=>char');
    
    %%
    %读取bus
    LogHeader.num_bus = fread(fileID, 1, 'uint8=>uint8');
    bus_alligned = 1;
    for n = 1:LogHeader.num_bus
        LogHeader.bus(n).name     = fread(fileID, [1,LogHeader.max_name_len], 'uint8=>char');
        LogHeader.bus(n).msg_id   = fread(fileID, 1,  'uint8=>uint8');
        LogHeader.bus(n).num_elem = fread(fileID, 1,  'uint8=>uint8');
        %LogHeader.bus(n).name    = LogHeader.bus(n).name(~isspace(strrep(LogHeader.bus(n).name, '"', '')));
        
        for k = 1:LogHeader.bus(n).num_elem
            LogHeader.bus(n).elem_list(k).name   = fread(fileID, [1,LogHeader.max_name_len], 'uint8=>char');
            LogHeader.bus(n).elem_list(k).type   = fread(fileID, 1, 'uint16=>uint16');
            LogHeader.bus(n).elem_list(k).number = fread(fileID, 1, 'uint16=>uint16');
            %LogHeader.bus(n).elem_list(k).name   = LogHeader.bus(n).elem_list(k).name(~isspace(strrep(LogHeader.bus(n).elem_list(k).name, '"', '')));
        end
        MsgCount{LogHeader.bus(n).msg_id} = 0;
        LogMsg{LogHeader.bus(n).msg_id} = {};
        
        if LogHeader.bus(n).msg_id ~= n
            bus_alligned = 0;
        end
    end
    
    %%
    %读取parameter
    LogHeader.num_param_group = fread(fileID, 1, 'uint8=>uint8');
    for n = 1:LogHeader.num_param_group
        LogHeader.param_group_list(n).name = fread(fileID, [1 LogHeader.max_name_len], 'uint8=>char');
        LogHeader.param_group_list(n).num_param = fread(fileID, 1, 'uint32=>uint32');
        %去除空格和双引号
        
        LogHeader.param_group_list(n).name = LogHeader.param_group_list(n).name(~isspace(LogHeader.param_group_list(n).name));
        
        for k = 1:LogHeader.param_group_list(n).num_param
            LogHeader.param_group_list(n).param(k).name = fread(fileID, [1 LogHeader.max_name_len], 'uint8=>char');
            LogHeader.param_group_list(n).param(k).type = fread(fileID, 1, 'uint8=>uint8');
            %参照param_type_t枚举型
            index = LogHeader.param_group_list(n).param(k).type+1;
            LogHeader.param_group_list(n).param(k).val = fread(fileID, 1, BLOG_TYPE(index));
            %去除名字字符空格
            LogHeader.param_group_list(n).param(k).name = LogHeader.param_group_list(n).param(k).name(~isspace(LogHeader.param_group_list(n).param(k).name));
        end
    end
    
    %test_cnt = 0;
    %% 读取Log Msg
    while ~feof(fileID) && ftell(fileID)<fileDir.bytes
    %/*                           BLOG MSG Format                                 */
    %/*   ======================================================================= */
    %/*   | BLOG_BEGIN_MSG1 | BLOG_BEGIN_MSG2 | MSG_ID | PAYLOAD | BLOG_END_MSG | */
    %/*   ======================================================================= */
    
        %test_cnt = test_cnt + 1
        begin_state = 0;
        %对齐帧头
        while begin_state < 2
            switch begin_state
                case 0
                    msg_begin = fread(fileID, 1, 'uint8=>uint8');
                    if msg_begin == BLOG_BEGIN_MSG1
                        begin_state = 1;
                    end
                case 1
                    msg_begin = fread(fileID, 1, 'uint8=>uint8');
                    if msg_begin == BLOG_BEGIN_MSG2
                        begin_state = 2;
                    elseif msg_begin == BLOG_BEGIN_MSG1
                        begin_state = 1;
                    else
                        begin_state = 0;
                    end
                otherwise
                    break;            
            end              
        end
        
        msg_id = fread(fileID, 1, 'uint8=>uint8');
        if(isempty(msg_id))
            break;
        end
        if bus_alligned
            index = msg_id;
            if index >  LogHeader.num_bus
                index = -1;
            end
        else
            index = -1;
            for n = 1:LogHeader.num_bus
                if msg_id == LogHeader.bus(n).msg_id
                    index = n;
                end
            end
        end
%         if(test_cnt == 72018)
%             break;
%         end
        if index <= 0
            fprintf('invalid msg id:%d\r\n', msg_id);
            continue;
        end 
        
        %读PAYLOAD
        for k = 1:LogHeader.bus(index).num_elem
            type = LogHeader.bus(index).elem_list(k).type+1;
            len = LogHeader.bus(index).elem_list(k).number;
            
            [elem_val, rb] = fread(fileID, [len, 1], BLOG_TYPE(type));
            if rb < len
                fprintf('%s %s cnt %d read err, delete it\n', LogHeader.bus(index).name, LogHeader.bus(index).elem_list(k).name, MsgCount{msg_id});
                % TODO: handle this error
                break;
            else
                LogMsg{msg_id}{k}(1:len, MsgCount{msg_id}+1) = elem_val;
            end
        end
        
        msg_end = fread(fileID, 1, 'uint8=>uint8');
        if msg_end == BLOG_END_MSG
            MsgCount{msg_id} = MsgCount{msg_id} + 1;
        else
             fprintf('invalid msg end flag:%d, msg id:%d\r\n', msg_end, msg_id);
%              for k = 1:LogHeader.bus(index).num_elem
%                 try
%                     LogMsg{msg_id}{k}(:, MsgCount{msg_id}+1) = [];
%                 catch
%                     continue
%                 end
%              end
        break;
        end  
    end
    fclose(fileID);
    fprintf('解算完成！\n');
    
    %去掉双引号
    
    
   for n = 1:LogHeader.num_bus
       BusName = strrep(LogHeader.bus(n).name, '"', '');
       BusName = BusName(~isspace(BusName));
       fprintf('%s: %d msg recorded\n', BusName, MsgCount{LogHeader.bus(n).msg_id});
   end
    
end
