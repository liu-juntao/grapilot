
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       fms_arming.c
  * @author     baiyang
  * @date       2021-8-19
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "fms.h"

#include <notify/notify.h>
#include <logger/blog.h>
#include <logger/blog_msg.h>
#include <parameter/param.h>
#include <arming/gp_arming.h>
#include <logger/task_logger.h>
#include <sensor_gps/sensor_gps.h>
#include <srv_channel/srv_channel.h>
#include <board_config/borad_config.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
static bool fms_arming_pre_arm_checks(gp_arming_t arming,bool display_failure);
static bool fms_arming_run_pre_arm_checks(gp_arming_t arming, bool display_failure);
static void fms_arming_set_pre_arm_check(gp_arming_t arming, bool b);
static bool fms_arming_mandatory_checks(gp_arming_t arming, bool display_failure);
static bool fms_arming_arm_checks(gp_arming_t arming, const ArmingMethod method);
static bool fms_rc_calibration_checks(gp_arming_t arming, bool display_failure);
static bool fms_arming_arm(gp_arming_t arming, const ArmingMethod method, const bool do_arming_checks);
static bool fms_arming_disarm(gp_arming_t arming, const ArmingMethod method, bool do_disarm_checks);
static bool fms_arming_gps_checks(gp_arming_t arming, bool display_failure);
static bool fms_arming_mandatory_gps_checks(gp_arming_t arming, bool display_failure);
static bool fms_arming_barometer_checks(gp_arming_t arming, bool display_failure);
static bool fms_arming_ins_checks(gp_arming_t arming, bool display_failure);
/*----------------------------------variable----------------------------------*/
static struct gp_arming_ops ops = {.arm = fms_arming_arm,
                                  .disarm = fms_arming_disarm,
                                  .pre_arm_checks = fms_arming_pre_arm_checks,
                                  .arm_checks = fms_arming_arm_checks,
                                  .barometer_checks = fms_arming_barometer_checks,
                                  .ins_checks = fms_arming_ins_checks,
                                  .gps_checks = fms_arming_gps_checks,
                                  .board_voltage_checks = NULL,
                                  .rc_calibration_checks = fms_rc_calibration_checks,
                                  .system_checks = NULL,
                                  .proximity_checks = NULL,
                                  .mandatory_checks = fms_arming_mandatory_checks};
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void fms_arming_ctor(gp_arming_t arming)
{
    arming_ctor(arming, &ops);
}

// check motor setup was successful
static bool motor_checks(gp_arming_t arming, bool display_failure)
{
    // check motors initialised  correctly
    if (!fms.motors->_initialised_ok) {
        arming_check_failed(arming, display_failure, "Check firmware or FRAME_CLASS");
        return false;
    }

    // further checks enabled with parameters
    if (!arming_check_enabled(arming, ARMING_CHECK_PARAMETERS)) {
        return true;
    }

    return true;
}

static bool fms_rc_calibration_checks(gp_arming_t arming, bool display_failure)
{
    fms.ap.pre_arm_rc_check = arming_rc_checks_copter_sub(arming, display_failure)
        & rc_calibration_checks(arming, display_failure);

    return fms.ap.pre_arm_rc_check;
}

// performs pre-arm checks. expects to be called at 1hz.
void fms_arming_update(gp_arming_t arming)
{
    // perform pre-arm checks & display failures every 30 seconds
    static uint8_t pre_arm_display_counter = PREARM_DISPLAY_PERIOD/2;
    pre_arm_display_counter++;
    
    bool display_fail = false;
    if (pre_arm_display_counter >= PREARM_DISPLAY_PERIOD) {
        display_fail = true;
        pre_arm_display_counter = 0;
    }

    arming_pre_arm_checks(arming, display_fail);
}

static bool fms_arming_pre_arm_checks(gp_arming_t arming,bool display_failure)
{
    const bool passed = fms_arming_run_pre_arm_checks(arming, display_failure);
    fms_arming_set_pre_arm_check(arming, passed);
    return passed;
}

// perform pre-arm checks
//  return true if the checks pass successfully
static bool fms_arming_run_pre_arm_checks(gp_arming_t arming, bool display_failure)
{
    // exit immediately if already armed
    if (fms.motors->_armed) {
        return true;
    }

    // check if motor interlock and Emergency Stop aux switches are used
    // at the same time.  This cannot be allowed.
    if (RCs_find_channel_for_option(RC_MOTOR_INTERLOCK) &&
        RCs_find_channel_for_option(RC_MOTOR_ESTOP)){
        arming_check_failed(arming, display_failure, "Interlock/E-Stop Conflict");
        return false;
    }

    // check if motor interlock aux switch is in use
    // if it is, switch needs to be in disabled position to arm
    // otherwise exit immediately.  This check to be repeated,
    // as state can change at any time.
    if (fms.ap.using_interlock && fms.ap.motor_interlock_switch) {
        arming_check_failed(arming, display_failure, "Motor Interlock Enabled");
    }

    // if pre arm checks are disabled run only the mandatory checks
    if (arming->checks_to_perform == 0) {
        return arming_mandatory_checks(arming, display_failure);
    }

#if 0
    return parameter_checks(display_failure)
        & motor_checks(display_failure)
        & pilot_throttle_checks(display_failure)
        & oa_checks(display_failure)
        & gcs_failsafe_check(display_failure)
        & winch_checks(display_failure)
        & alt_checks(display_failure)
        & pre_arm_checks(arming, display_failure);
#endif

    return motor_checks(arming, display_failure)
        & pre_arm_checks(arming, display_failure);
}

static void fms_arming_set_pre_arm_check(gp_arming_t arming, bool b)
{
    fms.ap.pre_arm_check = b;
    notify_flags.pre_arm_check = b;
}

// mandatory checks that will be run if ARMING_CHECK is zero or arming forced
static bool fms_arming_mandatory_checks(gp_arming_t arming, bool display_failure)
{
    // call mandatory gps checks and update notify status because regular gps checks will not run
    //bool result = mandatory_gps_checks(display_failure);
    //notify_flags.pre_arm_gps_check = result;

    // call mandatory alt check
    //if (!alt_checks(display_failure)) {
    //    result = false;
    //}

    //return result;
    return true;
}

// arm_checks - perform final checks before arming
//  always called just before arming.  Return true if ok to arm
//  has side-effect that logging is started
static bool fms_arming_arm_checks(gp_arming_t arming, const ArmingMethod method)
{
#if 0
    // always check if inertial nav has started and is ready
    if (!ahrs.healthy()) {
        arming_check_failed(true, "AHRS not healthy");
        return false;
    }
#endif

    // always check if the current mode allows arming
    if (!mode_allows_arming(fms.flightmode, method)) {
        arming_check_failed(arming, true, "Mode not armable");
        return false;
    }

    // always check motors
    if (!motor_checks(arming, true)) {
        return false;
    }

    // if we are using motor interlock switch and it's enabled, fail to arm
    // skip check in Throw mode which takes control of the motor interlock
    if (fms.ap.using_interlock && fms.ap.motor_interlock_switch) {
        arming_check_failed(arming, true, "Motor Interlock Enabled");
        return false;
    }

    // if we are not using Emergency Stop switch option, force Estop false to ensure motors
    // can run normally
    if (!RCs_find_channel_for_option(RC_MOTOR_ESTOP)){
        srv_channels_set_emergency_stop(false);
        // if we are using motor Estop switch, it must not be in Estop position
    } else if (srv_channels_get_emergency_stop()){
        arming_check_failed(arming, true, "Motor Emergency Stopped");
        return false;
    }

    // succeed if arming checks are disabled
    if (arming->checks_to_perform == 0) {
        return true;
    }


    // check lean angle
    if ((arming->checks_to_perform == ARMING_CHECK_ALL) || (arming->checks_to_perform & ARMING_CHECK_INS)) {
        int16_t lean_angle_cd = degrees(acosf(fms.ahrs->cos_roll*fms.ahrs->cos_pitch))*100.0f;
        if (lean_angle_cd > fms.g.angle_max) {
            arming_check_failed2(arming, ARMING_CHECK_INS, true, "Leaning %d/%d", lean_angle_cd, fms.g.angle_max);
            return false;
        }
    }

    // check throttle
    if ((arming->checks_to_perform == ARMING_CHECK_ALL) || (arming->checks_to_perform & ARMING_CHECK_RC)) {
        const char *rc_item = "Throttle";
        // check throttle is not too low - must be above failsafe throttle
        if (fms.g.failsafe_throttle != FS_THR_DISABLED && fms.channel_throttle->radio_in < fms.g.failsafe_throttle_value) {
            arming_check_failed2(arming, ARMING_CHECK_RC, true, "%s below failsafe", rc_item);
            return false;
        }

        // check throttle is not too high - skips checks if arming from GCS in Guided
        if (!(method == ARMING_CHECK_MAVLINK && (mode_number(fms.flightmode) == GUIDED || mode_number(fms.flightmode) == GUIDED_NOGPS))) {
            // above top of deadband is too always high
            if (fms_get_pilot_desired_climb_rate(fms.channel_throttle->control_in) > 0.0f) {
                arming_check_failed2(arming, ARMING_CHECK_RC, true, "%s too high", rc_item);
                return false;
            }
            // in manual modes throttle must be at zero
            if ((mode_has_manual_throttle(fms.flightmode) || mode_number(fms.flightmode) == DRIFT) && fms.channel_throttle->control_in > 0) {
                arming_check_failed2(arming, ARMING_CHECK_RC, true, "%s too high", rc_item);
                return false;
            }
        }
    }

    // check if safety switch has been pushed
    if (srv_hal_safety_switch_state() == SAFETY_DISARMED) {
        arming_check_failed(arming, true, "Safety Switch");
        return false;
    }

    // superclass method should always be the last thing called; it
    // has side-effects which would need to be cleaned up if one of
    // our arm checks failed
    return arm_checks(arming, method);
}

bool fms_arming_arm(gp_arming_t arming, const ArmingMethod method, const bool do_arming_checks)
{
    static bool in_arm_motors = false;

    // exit immediately if already in this function
    if (in_arm_motors) {
        return false;
    }
    in_arm_motors = true;

    // return true if already armed
    if (fms.motors->_armed) {
        in_arm_motors = false;
        return true;
    }

    if (!arm(arming, method, do_arming_checks)) {
        notify_events.arming_failed = true;
        in_arm_motors = false;
        return false;
    }

    // let logger know that we're armed (it may open logs e.g.)
    //AP::logger().set_vehicle_armed(true);
    if (blog_get_log_mode() == 0 || 
        blog_get_log_mode() == 1) {
        logger_start_blog(NULL);
    }

    // disable cpu failsafe because initialising everything takes a while
    //copter.failsafe_disable();

    // notify that arming will occur (we do this early to give plenty of warning)
    notify_flags.armed = true;

    // call notify update a few times to ensure the message gets out
    for (uint8_t i=0; i<=10; i++) {
        notify_update();
    }

#if CONFIG_HAL_BOARD == HAL_BOARD_SITL_WIN
    mavproxy_send_statustext(MAV_SEVERITY_INFO, "Arming motors");
#endif

    // Remember Orientation
    // --------------------
    //copter.init_simple_bearing();

    fms.initial_armed_bearing = fms.ahrs->yaw_sensor_cd;

#if 0
    if (!ahrs.home_is_set()) {
        // Reset EKF altitude if home hasn't been set yet (we use EKF altitude as substitute for alt above home)
        ahrs.resetHeightDatum();
        AP::logger().Write_Event(LogEvent::EKF_ALT_RESET);

        // we have reset height, so arming height is zero
        copter.arming_altitude_m = 0;
    } else if (!ahrs.home_is_locked()) {
        // Reset home position if it has already been set before (but not locked)
        if (!copter.set_home_to_current_location(false)) {
            // ignore failure
        }

        // remember the height when we armed
        copter.arming_altitude_m = copter.inertial_nav.get_altitude() * 0.01;
    }
    //copter.update_super_simple_bearing(false);
#endif

    // enable output to motors
    MotorsMat_output_min((MotorsMat_HandleTypeDef *)fms.motors);

    // finally actually arm the motors
    MotorsMat_set_armed((MotorsMat_HandleTypeDef *)fms.motors,true);

    // log flight mode in case it was changed while vehicle was disarmed
    blog_write_mode(mode_number(fms.flightmode), fms.control_mode_reason);

    // re-enable failsafe
    //copter.failsafe_enable();

    // perf monitor ignores delay due to arming
    //AP::scheduler().perf_info.ignore_this_loop();

    // flag exiting this function
    in_arm_motors = false;

    // Log time stamp of arming event
    fms.arm_time_ms = time_millis();

    // Start the arming delay
    fms.ap.in_arming_delay = true;

    // assumed armed without a arming, switch. Overridden in switches.cpp
    fms.ap.armed_with_switch = false;

    // 
    fms_actuator_armed_notify(true);

    blog_write_event(ARM, ARMED);

    // return success
    return true;
}

// arming.disarm - disarm motors
bool fms_arming_disarm(gp_arming_t arming, const ArmingMethod method, bool do_disarm_checks)
{
    // return immediately if we are already disarmed
    if (!fms.motors->_armed) {
        return true;
    }

    // do not allow disarm via mavlink if we think we are flying:
    if (do_disarm_checks &&
        method == ARMING_CHECK_MAVLINK &&
        !fms.ap.land_complete) {
        return false;
    }

    if (!disarm(arming, method, do_disarm_checks)) {
        return false;
    }

#if CONFIG_HAL_BOARD == HAL_BOARD_SITL_WIN
    mavproxy_send_statustext(MAV_SEVERITY_INFO, "Disarming motors");
#endif

#if 0
    auto &ahrs = AP::ahrs();

    // save compass offsets learned by the EKF if enabled
    Compass &compass = AP::compass();
    if (ahrs.use_compass() && compass.get_learn_type() == Compass::LEARN_EKF) {
        for(uint8_t i=0; i<COMPASS_MAX_INSTANCES; i++) {
            Vector3f magOffsets;
            if (ahrs.getMagOffsets(i, magOffsets)) {
                compass.set_and_save_offsets(i, magOffsets);
            }
        }
    }
#endif

#if AUTOTUNE_ENABLED == ENABLED
#if 0
    // save auto tuned parameters
    if (copter.flightmode == &copter.mode_autotune) {
        copter.mode_autotune.save_tuning_gains();
    } else {
        copter.mode_autotune.reset();
    }
#endif
#endif

    // we are not in the air
    fms_set_land_complete(true);
    fms_set_land_complete_maybe(true);

    // send disarm command to motors
    MotorsMat_set_armed((MotorsMat_HandleTypeDef *)fms.motors,false);

#if MODE_AUTO_ENABLED == ENABLED
    // reset the mission
    //copter.mode_auto.mission.reset();
#endif

    //AP::logger().set_vehicle_armed(false);

    fms.ap.in_arming_delay = false;

    // 
    fms_actuator_armed_notify(false);

    blog_write_event(ARM, DISARMED);

    if (blog_get_log_mode() == 0) {
        logger_stop_blog();
    }

    return true;
}

// performs pre_arm gps related checks and returns true if passed
static bool fms_arming_gps_checks(gp_arming_t arming, bool display_failure)
{
#if CONFIG_HAL_BOARD != HAL_BOARD_SITL_WIN
    // check if flight mode requires GPS
    bool mode_requires_gps = mode_requires_GPS(fms.flightmode);

    // call parent gps checks
    if (mode_requires_gps) {
        if (!gps_checks(arming, display_failure)) {
            notify_flags.pre_arm_gps_check = false;
            return false;
        }
    }

    // run mandatory gps checks first
    if (!fms_arming_mandatory_gps_checks(arming, display_failure)) {
        notify_flags.pre_arm_gps_check = false;
        return false;
    }

    // return true if GPS is not required
    if (!mode_requires_gps) {
        notify_flags.pre_arm_gps_check = true;
        return true;
    }

    // return true immediately if gps check is disabled
    if (!(arming->checks_to_perform == ARMING_CHECK_ALL || arming->checks_to_perform & ARMING_CHECK_GPS)) {
        notify_flags.pre_arm_gps_check = true;
        return true;
    }

    // warn about hdop separately - to prevent user confusion with no gps lock
    if (sensor_gps_get_hdop() > fms.g.gps_hdop_good) {
        arming_check_failed2(arming, ARMING_CHECK_GPS, display_failure, "High GPS HDOP");
        notify_flags.pre_arm_gps_check = false;
        return false;
    }
#endif

    // if we got here all must be ok
    notify_flags.pre_arm_gps_check = true;
    return true;
}

// performs mandatory gps checks.  returns true if passed
static bool fms_arming_mandatory_gps_checks(gp_arming_t arming, bool display_failure)
{
    // check if flight mode requires GPS
    bool mode_requires_gps = mode_requires_GPS(fms.flightmode);

    if (mode_requires_gps) {
        if (!fms_position_ok()) {
            // vehicle level position estimate checks
            arming_check_failed(arming, display_failure, "Need Position Estimate");
            return false;
        }
    }

    // if we got here all must be ok
    return true;
}

static bool fms_arming_barometer_checks(gp_arming_t arming, bool display_failure)
{
    if (!barometer_checks(arming, display_failure)) {
        return false;
    }

    bool ret = true;

#if 0
    // check Baro
    if ((checks_to_perform == ARMING_CHECK_ALL) || (checks_to_perform & ARMING_CHECK_BARO)) {
        // Check baro & inav alt are within 1m if EKF is operating in an absolute position mode.
        // Do not check if intending to operate in a ground relative height mode as EKF will output a ground relative height
        // that may differ from the baro height due to baro drift.
        nav_filter_status filt_status = copter.inertial_nav.get_filter_status();
        bool using_baro_ref = (!filt_status.flags.pred_horiz_pos_rel && filt_status.flags.pred_horiz_pos_abs);
        if (using_baro_ref) {
            if (fabsf(copter.inertial_nav.get_position_z_up_cm() - copter.baro_alt) > PREARM_MAX_ALT_DISPARITY_CM) {
                check_failed(ARMING_CHECK_BARO, display_failure, "Altitude disparity");
                ret = false;
            }
        }
    }
#endif

    return ret;
}

static bool fms_arming_ins_checks(gp_arming_t arming, bool display_failure)
{
    bool ret = ins_checks(arming, display_failure);

    if ((arming->checks_to_perform == ARMING_CHECK_ALL) || (arming->checks_to_perform & ARMING_CHECK_INS)) {
#if 0
        // get ekf attitude (if bad, it's usually the gyro biases)
        if (!pre_arm_ekf_attitude_check()) {
            check_failed(ARMING_CHECK_INS, display_failure, "EKF attitude is bad");
            ret = false;
        }
#endif
    }

    return ret;
}

/*------------------------------------test------------------------------------*/


