
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       mode_althold.c
  * @author     baiyang
  * @date       2021-9-3
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "mode.h"
#include "fms.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/
static bool althold_init(mode_base_t mode, bool ignore_checks);
static void althold_run(mode_base_t mode);
static ModeNumber althold_mode_number(mode_base_t mode);
static bool althold_requires_GPS(mode_base_t mode);
static bool althold_has_manual_throttle(mode_base_t mode);
static bool althold_allows_arming(mode_base_t mode, ArmingMethod method);
static bool althold_is_autopilot(mode_base_t mode);
static bool althold_has_user_takeoff(mode_base_t mode, bool must_navigate);
static bool althold_allows_autotune(mode_base_t mode);
static bool althold_allows_flip(mode_base_t mode);

static const char *althold_name(mode_base_t mode);
static const char *althold_name4(mode_base_t mode);
/*----------------------------------variable----------------------------------*/
static struct mode_ops mode_althold_ops = {
        .mode_number = althold_mode_number,
        .init        = althold_init,
        .exit        = NULL,
        .run         = althold_run,
        .requires_GPS = althold_requires_GPS,
        .has_manual_throttle = althold_has_manual_throttle,
        .allows_arming = althold_allows_arming,
        .is_autopilot = althold_is_autopilot,
        .has_user_takeoff = althold_has_user_takeoff,
        .in_guided_mode = NULL,
        .logs_attitude = NULL,
        .allows_save_trim = NULL,
        .allows_autotune = althold_allows_autotune,
        .allows_flip = althold_allows_flip,
        .name = althold_name,
        .name4 = althold_name4,
        .is_taking_off = NULL,
        .is_landing = NULL,
        .requires_terrain_failsafe = NULL,
        .get_wp = NULL,
        .wp_bearing = NULL,
        .wp_distance = NULL,
        .crosstrack_error = NULL,
        .output_to_motors = NULL,
        .use_pilot_yaw = NULL,
        .throttle_hover = NULL,
        .do_user_takeoff_start = NULL};
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void mode_althold_ctor(ModeAltHold* mode_althold)
{
    mode_ctor(&mode_althold->mode, &mode_althold_ops);
}

// althold_init - initialise althold controller
static bool althold_init(mode_base_t mode, bool ignore_checks)
{

    // initialise the vertical position controller
    if (!posctrl_is_active_z(fms.pos_control)) {
        posctrl_init_z_controller(fms.pos_control);
    }

    // set vertical speed and acceleration limits
    posctrl_set_max_speed_accel_z(fms.pos_control, -fms_get_pilot_speed_dn(), fms.g.pilot_speed_up, fms.g.pilot_accel_z);
    posctrl_set_correction_speed_accel_z(fms.pos_control, -fms_get_pilot_speed_dn(), fms.g.pilot_speed_up, fms.g.pilot_accel_z);

    return true;
}

// althold_run - runs the althold controller
// should be called at 100hz or more
static void althold_run(mode_base_t mode)
{
    ModeAltHold* mode_althold = (ModeAltHold*)mode;

    // set vertical speed and acceleration limits
    posctrl_set_max_speed_accel_z(fms.pos_control, -fms_get_pilot_speed_dn(), fms.g.pilot_speed_up, fms.g.pilot_accel_z);

    // apply SIMPLE mode transform to pilot inputs
    //update_simple_mode();

    // get pilot desired lean angles
    float target_roll, target_pitch;
    fms_get_pilot_desired_lean_angles(&target_roll, &target_pitch, fms.g.angle_max, attctrl_get_althold_lean_angle_max_cd(fms.attitude_control));

    // get pilot's desired yaw rate
    float target_yaw_rate = fms_get_pilot_desired_yaw_rate(RC_norm_input_dz(fms.channel_yaw));

    // get pilot desired climb rate
    float target_climb_rate = fms_get_pilot_desired_climb_rate(fms.channel_throttle->control_in);
    target_climb_rate = math_constrain_float(target_climb_rate, -fms_get_pilot_speed_dn(), fms.g.pilot_speed_up);

    // Alt Hold State Machine Determination
    AltHoldModeState althold_state = mode_get_alt_hold_state(target_climb_rate);

    // Alt Hold State Machine
    switch (althold_state) {

    case AltHold_MotorStopped:
        attctrl_reset_yaw_target_and_rate(fms.attitude_control, false);
        attctrl_reset_rate_controller_I_terms(fms.attitude_control);
        posctrl_relax_z_controller(fms.pos_control, 0.0f);   // forces throttle output to decay to zero
        break;

    case AltHold_Landed_Ground_Idle:
        attctrl_reset_yaw_target_and_rate(fms.attitude_control, true);
    case AltHold_Landed_Pre_Takeoff:
        attctrl_reset_rate_controller_I_terms_smoothly(fms.attitude_control);
        posctrl_relax_z_controller(fms.pos_control, 0.0f);   // forces throttle output to decay to zero
        break;

    case AltHold_Takeoff:
        // initiate take-off
        if (!mode_althold->mode.takeoff->_running) {
            takeoff_start(mode_althold->mode.takeoff, math_constrain_float(fms.g.pilot_takeoff_alt,0.0f,1000.0f));
        }

        // get avoidance adjusted climb rate
        //target_climb_rate = get_avoidance_adjusted_climbrate(target_climb_rate);

        // set position controller targets adjusted for pilot input
        takeoff_do_pilot_takeoff(mode_althold->mode.takeoff, target_climb_rate);
        break;

    case AltHold_Flying:
        Motors_set_desired_spool_state(fms.motors, MOTOR_DESIRED_THROTTLE_UNLIMITED);

//#if AC_AVOID_ENABLED == ENABLED
        // apply avoidance
        //copter.avoid.adjust_roll_pitch(target_roll, target_pitch, copter.aparm.angle_max);
//#endif

        // adjust climb rate using rangefinder
        //target_climb_rate = copter.surface_tracking.adjust_climb_rate(target_climb_rate);

        // get avoidance adjusted climb rate
        //target_climb_rate = get_avoidance_adjusted_climbrate(target_climb_rate);

        posctrl_set_pos_target_z_from_climb_rate_cm(fms.pos_control, target_climb_rate);
        break;
    }

    // call attitude controller
    attctrl_input_euler_angle_roll_pitch_euler_rate_yaw(fms.attitude_control,target_roll, target_pitch, target_yaw_rate);
    
    // run the vertical position controller and set output throttle
    posctrl_update_z_controller(fms.pos_control);
}

static ModeNumber althold_mode_number(mode_base_t mode) { return ALT_HOLD; }
static bool althold_requires_GPS(mode_base_t mode) { return false; }
static bool althold_has_manual_throttle(mode_base_t mode) { return false; }
static bool althold_allows_arming(mode_base_t mode, ArmingMethod method) { return true; };
static bool althold_is_autopilot(mode_base_t mode) { return false; }
static bool althold_has_user_takeoff(mode_base_t mode, bool must_navigate) { return !must_navigate;}
static bool althold_allows_autotune(mode_base_t mode) { return true; }
static bool althold_allows_flip(mode_base_t mode) { return true; }

static const char *althold_name(mode_base_t mode) { return "ALT_HOLD"; }
static const char *althold_name4(mode_base_t mode) { return "ALTH"; }

/*------------------------------------test------------------------------------*/


