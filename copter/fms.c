
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       fms.c
  * @author     baiyang
  * @date       2021-8-12
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "fms.h"

#include <common/grapilot.h>

#include <rtthread.h>
#include <rtdevice.h>
#include <rthw.h>

#include <notify/notify.h>
#include <board_config/borad_config.h>
/*-----------------------------------macro------------------------------------*/
#define FMS_EVENT_LOOP        (1<<0)
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static char thread_fms_stack[1024*8];
struct rt_thread thread_fms_handle;

static struct rt_timer fms_timer;
static struct rt_event fms_event;

Fms_handle fms;

static AttitudeMulti_ctrl _atc;
static Position_ctrl _psc;
static MotorsMat_HandleTypeDef _motor;
static mc_loiter _loiter_nav;
static mc_wpnav wp_nav;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       飞行管理模块构造函数
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        构造函数中函数的调用顺序不要随意更改
  */
void fms_ctor()
{
    fms_assign_param();

    fms.psc_dt = PERIOD_MS_100HZ * 0.001f;

    fms.prev_control_mode = STABILIZE;
    fms._last_reason = MODE_REASON_UNKNOWN;
    fms.control_mode_reason = MODE_REASON_UNKNOWN;

    fms.flightmode = (mode_base_t)&fms.mode_stabilize;

    fms_arming_ctor(&fms.arming);

    fms_init_rc_in();

    fms.ahrs = get_ahrs_view();
    fms.motors = (Motors_HandleTypeDef *)&_motor;
    fms.attitude_control = (Attitude_ctrl *)&_atc;
    fms.pos_control = &_psc;
    fms.loiter_nav = &_loiter_nav;
    fms.wp_nav = &wp_nav;

    lpf_set_cutoff1_vec3f(&fms.land_accel_ef_filter, 1.0f);

    MotorsMatrix((MotorsMat_HandleTypeDef *)fms.motors,1000/PERIOD_MS_200HZ,400);

    // 电机模块初始化之后
    fms_init_rc_out();

    attctrl_multi_init(fms.attitude_control, fms.motors, PERIOD_MS_200HZ*0.001f);
    attctrl_set_dt(fms.attitude_control, PERIOD_MS_100HZ*0.001f);

    posctrl_ctor(fms.pos_control, fms.ahrs, fms.motors, fms.attitude_control, PERIOD_MS_100HZ*0.001f);
    loiter_ctor(fms.loiter_nav, fms.ahrs, fms.pos_control, fms.attitude_control);
    wpnav_ctor(fms.wp_nav, fms.ahrs, fms.pos_control);

    // 飞行模式构造函数要在赋值飞行模式指针之前调用
    mode_althold_ctor(&fms.mode_althold);
    mode_stabilize_ctor(&fms.mode_stabilize);
    mode_loiter_ctor(&fms.mode_loiter);
    mode_land_ctor(&fms.mode_land);
    mode_rtl_ctor(&fms.mode_rtl);

    fms_gcs_init();

    // 电调校准,放在最后
    fms_esc_calibration_startup_check();
}

/**
  * @brief       1Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_one_hz_loop()
{
    fms_arming_update(&fms.arming);

    if (!fms.motors->_armed) {
        fms_update_using_interlock();

        MotorsMat_set_frame_class_and_type((MotorsMat_HandleTypeDef *)fms.motors,
            (motor_frame_class)fms.frame_class,
            (motor_frame_type)fms.frame_type);

        // set all throttle channel settings
        MotorsMC_set_throttle_range((MotorsMC_HandleTypeDef *)fms.motors,fms.channel_throttle->_radio_min,fms.channel_throttle->_radio_max);
    }

    srv_channels_enable_aux_servos();

    notify_flags.flying = !fms.ap.land_complete;
}

/**
  * @brief       2Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_two_hz_loop()
{
    // check if we've lost contact with the ground station
    fms_failsafe_gcs_check();
}

/**
  * @brief       5Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_fives_hz_loop()
{

}

/**
  * @brief       10Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_ten_hz_loop()
{
    srv_hal_safety_update();
    RCs_read_aux_all();
    fms_lost_vehicle_check();
    fms_arm_motors_check();
    fms_auto_disarm_check();
}

/**
  * @brief       25Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_twenty_five_hz_loop()
{

}

/**
  * @brief       50Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_fifty_hz_loop()
{
    fms_update_throttle_mix();
    fms_update_auto_armed();
    notify_update();
}

/**
  * @brief       100Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_one_hundred_hz_loop()
{
    fms_read_radio();
    RCs_read_mode_switch();

    fms_update_flight_mode();
    fms_update_throttle_hover();
    fms_standby_update();
}

/**
  * @brief       200Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fast_two_hundred_hz_loop()
{
    attctrl_rate_controller_run(fms.attitude_control);
    fms_motors_output();
    fms_update_land_and_crash_detectors();
}

/**
  * @brief       200Hz循环
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        用于更新需要最先更新的数据
  */
static inline void fast_fast_loop()
{
    fms_update_ahrs();
    fms_gcs_update();
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static inline void fms_loop()
{
    uint32_t NowMs = time_millis();

    TIMETAG_CHECK_EXECUTE2(fast,PERIOD_MS_200HZ,NowMs,fast_fast_loop();)

    TIMETAG_CHECK_EXECUTE2(100Hz,PERIOD_MS_100HZ,NowMs,fast_one_hundred_hz_loop();)
    TIMETAG_CHECK_EXECUTE2(200Hz,PERIOD_MS_200HZ,NowMs,fast_two_hundred_hz_loop();)

    TIMETAG_CHECK_EXECUTE2(50Hz,PERIOD_MS_50HZ,NowMs,fast_fifty_hz_loop();)
    TIMETAG_CHECK_EXECUTE2(25Hz,PERIOD_MS_25HZ,NowMs,fast_twenty_five_hz_loop();)
    TIMETAG_CHECK_EXECUTE2(10Hz,PERIOD_MS_10HZ,NowMs,fast_ten_hz_loop();)
    TIMETAG_CHECK_EXECUTE2(5Hz,PERIOD_MS_5HZ,NowMs,fast_fives_hz_loop();)
    TIMETAG_CHECK_EXECUTE2(2Hz,PERIOD_MS_2HZ,NowMs,fast_two_hz_loop();)
    TIMETAG_CHECK_EXECUTE2(1Hz,PERIOD_MS_1HZ,NowMs,fast_one_hz_loop();)
}

/**
  * @brief       定时器回调函数，发送定时器事件
  * @param[in]   parameter  
  * @param[out]  
  * @retval      
  * @note        
  */
static void fms_time_update(void* parameter)
{
    rt_event_send(&fms_event, FMS_EVENT_LOOP);
}

/**
  * @brief       飞行任务线程入口函数
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
static void fms_entry(void *parameter)
{
    rt_err_t res;
    rt_uint32_t recv_set = 0;
    rt_uint32_t wait_set = FMS_EVENT_LOOP;

    /* create event */
    res = rt_event_init(&fms_event, "fms", RT_IPC_FLAG_FIFO);
    
    /* register timer event */
    rt_timer_init(&fms_timer, "fms",
                    fms_time_update,
                    RT_NULL,
                    rt_tick_from_millisecond(1),
                    RT_TIMER_FLAG_PERIODIC | RT_TIMER_FLAG_HARD_TIMER);
    rt_timer_start(&fms_timer);
    
    while(1)
    {
        res = rt_event_recv(&fms_event, wait_set, RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR, 
                                RT_WAITING_FOREVER, &recv_set);
        
        if(res == RT_EOK){
            if(recv_set & FMS_EVENT_LOOP){
                fms_loop();
            }
        }
    }
}

gp_err task_fms_init(void)
{
    rt_err_t res;

    fms_ctor();

    res = rt_thread_init(&thread_fms_handle,
                           "fms",
                           fms_entry,
                           RT_NULL,
                           &thread_fms_stack[0],
                           sizeof(thread_fms_stack),PRIORITY_FMS,5);

    RT_ASSERT(res == RT_EOK);

    if (res == RT_EOK) {
        rt_thread_startup(&thread_fms_handle);
    }

    brd_set_vehicle_init_stage(INIT_STAGE_FMS);

    return res;
}

/*------------------------------------test------------------------------------*/


