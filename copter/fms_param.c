
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       fms_param.c
  * @author     baiyang
  * @date       2021-8-23
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "fms.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void fms_assign_param()
{
    param_link_variable(PARAM_ID(FLIGHTMODE, FLTMODE1), &fms.flight_modes[0]);
    param_link_variable(PARAM_ID(FLIGHTMODE, FLTMODE2), &fms.flight_modes[1]);
    param_link_variable(PARAM_ID(FLIGHTMODE, FLTMODE3), &fms.flight_modes[2]);
    param_link_variable(PARAM_ID(FLIGHTMODE, FLTMODE4), &fms.flight_modes[3]);
    param_link_variable(PARAM_ID(FLIGHTMODE, FLTMODE5), &fms.flight_modes[4]);
    param_link_variable(PARAM_ID(FLIGHTMODE, FLTMODE6), &fms.flight_modes[5]);

    param_link_variable(PARAM_ID(VEHICLE,FRAME_CLASS), &fms.frame_class);
    param_link_variable(PARAM_ID(VEHICLE,FRAME_TYPE), &fms.frame_type);

    param_link_variable(PARAM_ID(VEHICLE,FS_THR_ENABLE), &fms.g.failsafe_throttle);
    param_link_variable(PARAM_ID(VEHICLE,FS_THR_VALUE), &fms.g.failsafe_throttle_value);
    param_link_variable(PARAM_ID(VEHICLE,THR_DZ), &fms.g.throttle_deadzone);

    param_link_variable(PARAM_ID(VEHICLE, PILOT_SPEED_UP), &fms.g.pilot_speed_up);
    param_link_variable(PARAM_ID(VEHICLE, PILOT_SPEED_DN), &fms.g.pilot_speed_dn);

    param_link_variable(PARAM_ID(VEHICLE,PILOT_Y_RATE), &fms.g.pilot_y_rate);
    param_link_variable(PARAM_ID(VEHICLE,ACRO_Y_EXPO), &fms.g.pilot_y_expo);

    param_link_variable(PARAM_ID(VEHICLE,PILOT_ACCEL_Z), &fms.g.pilot_accel_z);
    param_link_variable(PARAM_ID(VEHICLE,DISARM_DELAY), &fms.g.disarm_delay);

    param_link_variable(PARAM_ID(VEHICLE,PILOT_THR_BHV), &fms.g.throttle_behavior);
    param_link_variable(PARAM_ID(VEHICLE,PILOT_THR_FILT), &fms.g.throttle_filt);

    param_link_variable(PARAM_ID(VEHICLE,ANGLE_MAX), &fms.g.angle_max);
    param_link_variable(PARAM_ID(VEHICLE,PILOT_TKOFF_ALT), &fms.g.pilot_takeoff_alt);

    param_link_variable(PARAM_ID(VEHICLE,GPS_HDOP_GOOD), &fms.g.gps_hdop_good);

    param_link_variable(PARAM_ID(VEHICLE,WP_YAW_BEHAVIOR), &fms.g.wp_yaw_behavior);
    param_link_variable(PARAM_ID(VEHICLE,LAND_ALT_LOW), &fms.g.land_alt_low);

    param_link_variable(PARAM_ID(VEHICLE,LAND_SPEED), &fms.g.land_speed);
    param_link_variable(PARAM_ID(VEHICLE,LAND_SPEED_HIGH), &fms.g.land_speed_high);

    param_link_variable(PARAM_ID(VEHICLE,LAND_REPOSITION), &fms.g.land_repositioning);
    param_link_variable(PARAM_ID(VEHICLE,WP_NAVALT_MIN), &fms.g.wp_navalt_min);

    param_link_variable(PARAM_ID(RTL,RTL_ALT), &fms.g.rtl_altitude);
    param_link_variable(PARAM_ID(RTL,RTL_CONE_SLOPE), &fms.g.rtl_cone_slope);
    param_link_variable(PARAM_ID(RTL,RTL_SPEED), &fms.g.rtl_speed_cms);
    param_link_variable(PARAM_ID(RTL,RTL_ALT_FINAL), &fms.g.rtl_alt_final);
    param_link_variable(PARAM_ID(RTL,RTL_CLIMB_MIN), &fms.g.rtl_climb_min);
    param_link_variable(PARAM_ID(RTL,RTL_LOIT_TIME), &fms.g.rtl_loiter_time);
    param_link_variable(PARAM_ID(RTL,RTL_ALT_TYPE), &fms.g.rtl_alt_type);
    param_link_variable(PARAM_ID(RTL,RTL_OPTIONS), &fms.g.rtl_options);

    param_link_variable(PARAM_ID(VEHICLE,FS_OPTIONS), &fms.g.fs_options);
    param_link_variable(PARAM_ID(VEHICLE,FS_GCS_ENABLE), &fms.g.failsafe_gcs);
    param_link_variable(PARAM_ID(VEHICLE,FS_GCS_TIMEOUT), &fms.g.fs_gcs_timeout);
}

/*------------------------------------test------------------------------------*/


